import 'package:fushipment/models/catalog.dart';
import 'package:velocity_x/velocity_x.dart';

import '../models/order_item.dart';
import '../models/product.dart';

class MyStore extends VxStore {
  //user
  List<CartItems> cartItems = [];
  num totalPrice = 0;
  String role = "";
  Map<String, dynamic> userData = {};

  //vendor
  List<OrderItem> orderItems = [];
  List<Product> productItems = [];
}

/*class TotalPrice extends VxMutation<MyStore> {
  num quantity;
  num price;

  TotalPrice(this.quantity, this.price);

  @override
  perform() {
    if (quantity < 1) quantity = 1;
    store?.totalPrice = quantity * price;
  }
}

class RemoveFromCart extends VxMutation<MyStore> {
  final int index;

  RemoveFromCart(this.index);

  @override
  Future perform() async {
    final prefs = await SharedPreferences.getInstance();
    store?.cartItems.removeAt(index);
    List _listItems = [];
    for (var element in store!.cartItems) {
      _listItems.add({
        "id": element.id,
        "name": element.name,
        "price": element.price,
        "image": element.image,
        "quantity": element.quantity,
        "desc": element.desc
      });
    }
    await prefs.setString("cart", json.encode(_listItems));
  }
}

class UpdateCartItems extends VxMutation<MyStore> {
  final int index;
  final int quantity;
  final String desc;

  UpdateCartItems(this.index, this.quantity, this.desc);

  @override
  Future perform() async {
    final prefs = await SharedPreferences.getInstance();
    store?.cartItems[index] = CartItems(
        store!.cartItems[index].id,
        store!.cartItems[index].name,
        store!.cartItems[index].price,
        store!.cartItems[index].image,
        quantity,
        desc);
    List _listItems = [];
    for (var element in store!.cartItems) {
      _listItems.add({
        "id": element.id,
        "name": element.name,
        "price": element.price,
        "image": element.image,
        "quantity": element.quantity,
        "desc": element.desc
      });
    }
    await prefs.setString("cart", json.encode(_listItems));
  }
}

class UpdateOrderItems extends VxMutation<MyStore> {
  final String created_by;
  final List order;

  UpdateOrderItems(this.created_by, this.order);

  @override
  perform() {
    store?.orderItems
        .add(OrderItem(store?.orderItems[0].id ?? 0 + 1, created_by, 0, order));
  }
}

class AddProduct extends VxMutation<MyStore> {
  final Product product;

  AddProduct(this.product);

  @override
  perform() {
    store!.productItems.add(product);
  }
}

class RemoveProduct extends VxMutation<MyStore> {
  final int index;

  RemoveProduct(this.index);

  @override
  perform() {
    store?.productItems.removeAt(index);
  }
}*/
