import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Helpers {
  static String convertToMoneyVietNam(num number, String symbol) {
    var formattedNumber =
        NumberFormat.currency(locale: 'vi_VN', symbol: symbol).format(number);
    return formattedNumber;
  }

  static num convertMoneyVietNamToNumber(
      String formattedString, String symbol) {
    var number = NumberFormat.currency(locale: 'vi_VN', symbol: symbol)
        .parse(formattedString);
    return number;
  }

  static TimeOfDay getTimeFromString(String timeString) {
    DateTime dateTime = DateTime.parse(timeString);
    return TimeOfDay(hour: dateTime.hour, minute: dateTime.minute);
  }

  static String convertTimeOfDayToString(String timeString) {
    TimeOfDay convertStringToTimeOfDay(String timeString) {
      List<String> parts = timeString.split(' ');
      List<int> timeParts =
          parts[0].split(':').map((e) => int.parse(e)).toList();

      int hour = timeParts[0];
      int minute = timeParts[1];

      // Adjust hour based on AM/PM
      if (parts[1] == 'PM' && hour != 12) {
        hour += 12;
      } else if (parts[1] == 'AM' && hour == 12) {
        hour = 0;
      }

      return TimeOfDay(hour: hour, minute: minute);
    }

    TimeOfDay timeOfDay = convertStringToTimeOfDay(timeString);
    DateTime now = DateTime.now();
    DateTime dateTime = DateTime(
        now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
    return dateTime.toIso8601String();
  }
}
