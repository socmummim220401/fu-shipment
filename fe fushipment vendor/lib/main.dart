import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fushipment/pages/hola_page.dart';
import 'package:fushipment/pages/home_page.dart';
import 'package:fushipment/pages/login_page.dart';
import 'package:fushipment/pages/order_page.dart';
import 'package:fushipment/utils/routes.dart';
import 'package:velocity_x/velocity_x.dart';
import 'core/store.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessageBackgroundHandler);
  runApp(VxState(
    store: MyStore(),
    child: const MyApp(),
  ));
}

@pragma('vm:entry-point')
Future<void> _firebaseMessageBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xFFF5F5F3),
      ),
      routes: {
        "/": (context) => HomePage(),
        AppRoutes.loginRoute: (context) => const LoginPage(),
      },
    );
  }
}
