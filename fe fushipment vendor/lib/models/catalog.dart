class CartItems {
  final int id;
  final String name;
  final num price;
  final String image;
  final int quantity;
  final String desc;

  factory CartItems.fromMap(Map<String, dynamic> map) {
    return CartItems(
      map["id"],
      map["name"],
      map["price"],
      map["image"],
      map["quantity"],
      map["desc"],
    );
  }

  toMap() => {
        "id": id,
        "name": name,
        "price": price,
        "image": image,
        "quantity": quantity,
        "desc": desc
      };

  CartItems(
      this.id, this.name, this.price, this.image, this.quantity, this.desc);
}
