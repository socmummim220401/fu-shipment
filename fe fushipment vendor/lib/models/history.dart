class History {
  int? id;
  int? type;
  String? name;
  String? datePayment;
  int? status;
  double? price;

  History(
      {this.id,
        this.type,
        this.name,
        this.datePayment,
        this.status,
        this.price});

  History.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    name = json['name'];
    datePayment = json['datePayment'];
    status = json['status'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type'] = type;
    data['name'] = name;
    data['datePayment'] = datePayment;
    data['status'] = status;
    data['price'] = price;
    return data;
  }
}