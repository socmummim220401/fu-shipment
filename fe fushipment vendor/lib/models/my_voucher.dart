class MyVoucher {
  int? id;
  String? image;
  String? title;
  String? expireDate;
  int? quantity;

  MyVoucher({this.id, this.image, this.title, this.expireDate, this.quantity});

  MyVoucher.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    title = json['title'];
    expireDate = json['expireDate'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['image'] = image;
    data['title'] = title;
    data['expireDate'] = expireDate;
    data['quantity'] = quantity;
    return data;
  }
}