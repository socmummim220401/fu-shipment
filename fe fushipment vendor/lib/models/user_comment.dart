class UserComment {
  int? id;
  String? userName;
  String? publicDate;
  String? role;
  double? userRate;
  String? userImage;
  String? commentDetail;

  UserComment(
      {this.id,
      this.userName,
      this.publicDate,
      this.role,
      this.userRate,
      this.userImage,
      this.commentDetail});

  UserComment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userName = json['userName'];
    publicDate = json['publicDate'];
    role = json['role'];
    userRate = json['userRate'];
    commentDetail = json['commentDetail'];
    userImage = json['userImage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['userName'] = userName;
    data['publicDate'] = publicDate;
    data['role'] = role;
    data['userRate'] = userRate;
    data['commentDetail'] = commentDetail;
    data['userImage'] = userImage;
    return data;
  }
}
