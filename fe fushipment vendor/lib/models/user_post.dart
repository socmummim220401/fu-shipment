class UserPost {
  int? id;
  String? userPostName;
  String? iconImage;
  double? rate;
  List<String>? voucher;
  String? description;
  String? voucherDetail;
  List<String>? productImage;
  String? timeOpen;
  String? timeClose;

  UserPost(
      {this.id,
        this.userPostName,
        this.iconImage,
        this.rate,
        this.voucher,
        this.description,
        this.voucherDetail,
        this.productImage,
        this.timeOpen,
        this.timeClose});

  UserPost.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userPostName = json['userPostName'];
    iconImage = json['iconImage'];
    rate = json['rate'];
    voucher = json['voucher'].cast<String>();
    description = json['description'];
    voucherDetail = json['voucherDetail'];
    productImage = json['productImage'].cast<String>();
    timeOpen = json['timeOpen'];
    timeClose = json['timeClose'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['userPostName'] = userPostName;
    data['iconImage'] = iconImage;
    data['rate'] = rate;
    data['voucher'] = voucher;
    data['description'] = description;
    data['voucherDetail'] = voucherDetail;
    data['productImage'] = productImage;
    data['timeOpen'] = timeOpen;
    data['timeClose'] = timeClose;
    return data;
  }
}