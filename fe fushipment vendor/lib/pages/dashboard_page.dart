import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  List<Map<String, dynamic>> orders = [
    {"title": "Chờ xác nhận", "quantity": 1},
    {"title": "Đang chuẩn bị", "quantity": 0},
    {"title": "Đang giao", "quantity": 0},
    {"title": "Đã giao", "quantity": 0},
    {"title": "Bị hủy", "quantity": 0},
  ];

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
          child: Column(
            children: [
              Container(
                  margin: const EdgeInsets.only(bottom: 12, top: 4),
                  child: const Text(
                    "Đơn hàng hôm nay",
                    style:
                    TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
                  )),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.white,
                    width: 4.0,
                  ),
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                      spreadRadius: 0.1,
                    ),
                  ],
                ),
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                  child: Column(
                    children: orders.asMap().entries.map((entry) {
                      final index = entry.key;
                      final item = entry.value;
                      final isLastItem = index == orders.length - 1;
                      final isFirstItem = index == 0;
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: (Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Text(item["title"]),
                                    const SizedBox(
                                      width: 2,
                                    ),
                                    (isFirstItem || isLastItem)
                                        ? Icon(
                                      Icons.circle,
                                      size: 10,
                                      color: isFirstItem
                                          ? Colors.green
                                          : Colors.red,
                                    )
                                        : Container()
                                  ],
                                ),
                                RichText(
                                  text: TextSpan(
                                    text: '',
                                    style:
                                    const TextStyle(color: Colors.black),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: item["quantity"].toString(),
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold)),
                                      const TextSpan(text: ' ĐH'),
                                    ],
                                  ),
                                )
                              ],
                            )),
                          ),
                          !isLastItem
                              ? const Divider(
                            color: Color(0xffcccccc),
                          )
                              : const SizedBox()
                        ],
                      );
                    }).toList(),
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(bottom: 12, top: 16),
                  child: const Text(
                    "Voucher",
                    style:
                    TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
                  )),
              Container(
                height: 160,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.white,
                    width: 4.0,
                  ),
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                      spreadRadius: 0.1,
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
