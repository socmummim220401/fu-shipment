import 'package:flutter/material.dart';
import 'package:fushipment/pages/login_page.dart';
import 'package:fushipment/values/app_assets.dart';
import 'package:fushipment/values/app_colors.dart';

class HoLaPage extends StatelessWidget {
  const HoLaPage({super.key}) ;
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 5), () {
      Navigator.push(context, MaterialPageRoute(builder: (context)=> const LoginPage()));
    });
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Center(
        child: (Image.asset(
          AppAssets.hoLaPath,
          width: 200,
        )),
      ),
    );
  }
}
