import 'package:flutter/material.dart';
import 'package:fushipment/widgets/layout/default_layout.dart';

class HomePage extends StatefulWidget {
  final int? selectedIndex;

  const HomePage({super.key, this.selectedIndex});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    int selectedIndex = widget.selectedIndex ?? 0;
    return DefaultLayout(
      selectedIndex: selectedIndex,
    );
  }
}
