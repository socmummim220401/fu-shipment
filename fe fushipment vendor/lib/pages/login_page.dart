import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/models/user.dart';
import 'package:fushipment/pages/otp_page.dart';
import 'package:fushipment/pages/product_page.dart';
import 'package:fushipment/services/singleton/http_singleton.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_icons.dart';
import 'package:fushipment/values/app_styles.dart';
import 'package:http/http.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool? checked = true;
  String dropDownValue = "+84";
  late TextEditingController _controller;
  MyHttpWithSingleton httpSingleton = MyHttpWithSingleton();

  Future<void> loginAccount() async {
    String phone = '$dropDownValue${_controller.text}';
    //test +84358111470
    Map<String, String> bodies = {
      "phoneNumber": phone,
    };
    try {
      Response response = await httpSingleton.postRequestWithUnauthorize(
          endpoint: "/login", bodies: bodies);
      if (response.body.isNotEmpty) {
        var responseBody = jsonDecode(response.body);
        if (responseBody != null && responseBody["status"] == "200") {
          if (context.mounted) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => OTPPage(
                          phone: phone,
                        )));
          }
        } else {
          if (context.mounted) {
            final scaffold = ScaffoldMessenger.of(context);
            scaffold.showSnackBar(SnackBar(
              content: Text(responseBody["message"]),
              action: SnackBarAction(
                label: 'Cancel',
                onPressed: scaffold.hideCurrentSnackBar,
              ),
            ));
          }
        }
      } else {
        if (context.mounted) {
          final scaffold = ScaffoldMessenger.of(context);
          scaffold.showSnackBar(SnackBar(
            content: const Text("Server Busy"),
            action: SnackBarAction(
              label: 'Cancel',
              onPressed: scaffold.hideCurrentSnackBar,
            ),
          ));
        }
      }
    } catch (_) {
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    // _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      color: AppColors.primaryColor,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          bottomRight: Radius.circular(8))),
                  height: MediaQuery.of(context).size.height * 0.34,
                  // child: Image.network(
                  //     "https://media.istockphoto.com/id/1202073158/vector/online-shopping-concept.jpg?s=612x612&w=0&k=20&c=6V3ncvtoNhv3klHoMJVTd-Dp_fN4BaUoLulzv75juX0=",
                  //     fit: BoxFit.fill),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Đăng nhập / Đăng ký",
                        style: AppStyles.h5.copyWith(
                            fontWeight: FontWeight.w600, fontSize: 19),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      const Text("Nhập số điện thoại để tiếp tục"),
                      const SizedBox(
                        height: 12,
                      ),
                      Row(
                        children: [
                          SizedBox(
                              width: MediaQuery.of(context).size.width * 0.2,
                              height: 40,
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: AppColors.borderColor, width: 1),
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(4),
                                        bottomLeft: Radius.circular(4))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(AppIcons.flag),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    const Text(
                                      "+84",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 17),
                                    )
                                  ],
                                ),
                              )),
                          const SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: SizedBox(
                              height: 40,
                              child: TextField(
                                controller: _controller,
                                onChanged: (value) {
                                  setState(() {
                                    _controller.text = value;
                                    _controller.selection =
                                        TextSelection.collapsed(
                                            offset: _controller.text.length);
                                  });
                                },
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.borderColor),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(4),
                                          bottomRight: Radius.circular(4))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.borderColor),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(4),
                                          bottomRight: Radius.circular(4))),
                                  // Hide the border
                                  contentPadding: EdgeInsets.all(
                                      12), // Adjust padding as needed
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      _controller.text.isEmpty
                          ? customButton(
                              title: "Tiếp tục",
                              backgroundColor: AppColors.primaryColor,
                              borderColor: AppColors.primaryColor,
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.375,
                                  vertical: 8),
                              context: context,
                              textStyle: const TextStyle(
                                  color: Colors.white, fontSize: 16),
                              onPressed: () async {
                                await loginAccount();
                              })
                          : customButton(
                              title: "Tiếp tục",
                              backgroundColor: AppColors.primaryColor,
                              borderColor: AppColors.primaryColor,
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.375,
                                  vertical: 8),
                              context: context,
                              textStyle: const TextStyle(
                                  color: Colors.white, fontSize: 16),
                              onPressed: () async {
                                await loginAccount();
                              }),
                      const SizedBox(
                        height: 20,
                      ),
                      RichText(
                        text: const TextSpan(
                          text: 'Bằng việc bấm tiếp tục, bạn đồng ý với ',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Điều khoản sử dụng',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                            TextSpan(text: ' và '),
                            TextSpan(
                                text: 'Chính sách riêng tư',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                            TextSpan(text: ' của HOBUY'),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget customButton(
    {BuildContext? context,
    required String title,
    Color? backgroundColor,
    Color? borderColor,
    required VoidCallback onPressed,
    EdgeInsetsGeometry? padding,
    TextStyle? textStyle,
    Widget? icon}) {
  return UnconstrainedBox(
      child: icon != null
          ? ElevatedButton.icon(
              icon: icon,
              onPressed: onPressed,
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                  color: borderColor!,
                )),
                padding: MaterialStateProperty.all(padding),
                shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)))),
                backgroundColor: MaterialStateProperty.all(backgroundColor),
              ),
              label: Text(title, style: textStyle))
          : ElevatedButton(
              onPressed: onPressed,
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                  color: borderColor!,
                )),
                padding: MaterialStateProperty.all(padding),
                shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)))),
                backgroundColor: MaterialStateProperty.all(backgroundColor),
              ),
              child: Text(title, style: textStyle))

      // child: Text(title!, style: textStyle)),
      );
}
