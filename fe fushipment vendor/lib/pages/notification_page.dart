// import 'package:flutter/material.dart';
// import 'package:fushipment/models/notification.dart';
// import 'package:fushipment/widgets/user/notification/notification_item.dart';
//
// class NotificationPage extends StatefulWidget {
//   const NotificationPage({super.key});
//
//   @override
//   State<NotificationPage> createState() => _NotificationPageState();
// }
//
// class _NotificationPageState extends State<NotificationPage> {
//   List<NotificationConstructor>? notifications;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     notifications = [
//       NotificationConstructor(
//           id: 1,
//           image:
//               "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
//           title: "Ưu đãi hot khi mua OPPO online tại FPT Shop",
//           publicDate: "Hết hạn: 30/11/2023",
//           detail:
//               "Giảm đến 1.400.000đ. Trả góp 0%. Tặng nước rửa tay diệt khuẩn phòng covid 19"),
//       NotificationConstructor(
//           id: 1,
//           image:
//               "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
//           title: "Ưu đãi hot khi mua OPPO online tại FPT Shop",
//           publicDate: "Hết hạn: 30/11/2023",
//           detail:
//               "Giảm đến 1.400.000đ. Trả góp 0%. Tặng nước rửa tay diệt khuẩn phòng covid 19"),
//       NotificationConstructor(
//           id: 1,
//           image:
//               "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
//           title: "Ưu đãi hot khi mua OPPO online tại FPT Shop",
//           publicDate: "Hết hạn: 30/11/2023",
//           detail:
//               "Giảm đến 1.400.000đ. Trả góp 0%. Tặng nước rửa tay diệt khuẩn phòng covid 19"),
//       NotificationConstructor(
//           id: 1,
//           image:
//               "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
//           title: "Ưu đãi hot khi mua OPPO online tại FPT Shop",
//           publicDate: "Hết hạn: 30/11/2023",
//           detail:
//               "Giảm đến 1.400.000đ. Trả góp 0%. Tặng nước rửa tay diệt khuẩn phòng covid 19"),
//       NotificationConstructor(
//           id: 1,
//           image:
//               "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
//           title: "Ưu đãi hot khi mua OPPO online tại FPT Shop",
//           publicDate: "Hết hạn: 30/11/2023",
//           detail:
//               "Giảm đến 1.400.000đ. Trả góp 0%. Tặng nước rửa tay diệt khuẩn phòng covid 19"),
//     ];
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.orangeAccent,
//         title: const Text(
//           "Thông báo",
//           style: TextStyle(color: Colors.black, letterSpacing: 1),
//         ),
//         centerTitle: true,
//         iconTheme: const IconThemeData(color: Colors.black),
//         leading: IconButton(
//           icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
//           onPressed: () => Navigator.of(context).pop(),
//         ),
//         actions: const [
//           IconButton(
//               onPressed: null,
//               icon: Icon(
//                 Icons.check,
//                 color: Colors.black,
//               ))
//         ],
//       ),
//       body: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
//         child: ListView(
//           children: notifications!
//               .map((e) => NotificationItem(
//                     notification: e,
//                   ))
//               .toList(),
//         ),
//       ),
//     );
//   }
// }
