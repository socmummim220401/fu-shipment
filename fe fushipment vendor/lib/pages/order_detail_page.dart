import 'package:flutter/material.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';
import 'package:fushipment/widgets/timeline/time_line.dart';

class OrderDetailPage extends StatefulWidget {
  final int? tabSelected;

  const OrderDetailPage({super.key, required, this.tabSelected});

  @override
  State<OrderDetailPage> createState() => _OrderDetailPageState();
}

class _OrderDetailPageState extends State<OrderDetailPage> {
  final List<Map<String, dynamic>> productItems = [{}, {}, {}, {}];
  late int step;

  int handleActionButton(int setStep) {
    setState(() {
      step = setStep;
    });
    return 0;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    step = widget.tabSelected ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    final int tabSelected = widget.tabSelected ?? 0;

    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.sizeOf(context).height * 0.92,
              child: ListView(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 28),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(Icons.arrow_back,
                              color: AppColors.primaryColor, size: 24),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        const Text("Chi tiết đơn hàng",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600)),
                      ],
                    ),
                  ),
                  TimelineWidget(step: tabSelected),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 20),
                    child: const Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Trạng thái",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "Mới",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: AppColors.primaryColor),
                        ),
                      ],
                    ),
                  ),
                  const Text("Địa chỉ giao hàng",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      )),
                  const SizedBox(
                    height: 4,
                  ),
                  const Text("My house 3, Nhà thi đấu Hòa Lạc",
                      style: TextStyle(fontSize: 12)),
                  const SizedBox(
                    height: 4,
                  ),
                  const Text("Số điện thoại",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      )),
                  const SizedBox(
                    height: 4,
                  ),
                  const Text("1234567890", style: TextStyle(fontSize: 12)),
                  const SizedBox(
                    height: 12,
                  ),
                  Container(
                      padding:
                          const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.white,
                          width: 4.0,
                        ),
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 1.0,
                            spreadRadius: 0.1,
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          ...productItems.asMap().entries.map((e) {
                            return productItem();
                          }),
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Tổng (4 sản phẩm)",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                  )),
                              Text("8.000.000",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                  )),
                            ],
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Phí vận chuyển",
                                  style: TextStyle(fontSize: 12)),
                              Text("20.000", style: TextStyle(fontSize: 12)),
                            ],
                          ),
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Giảm giá", style: TextStyle(fontSize: 12)),
                              Text("15.000", style: TextStyle(fontSize: 12)),
                            ],
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Thành tiền",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                  )),
                              Text("8.005.000",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: AppColors.primaryColor)),
                            ],
                          ),
                        ],
                      )),
                  const SizedBox(
                    height: 12,
                  ),
                  const Text("Chi tiết khác",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      )),
                  const SizedBox(
                    height: 4,
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text("Ghi chú", style: TextStyle(fontSize: 12)),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                            textAlign: TextAlign.end,
                            style: TextStyle(fontSize: 12),
                            "mình muốn ăn bánh mì nhiều thịt nhưng ít calo thôi nhé, tại sợ béo. Bánh mì chay thêm nhiều ba chi nha. Thank"),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Mã đơn hàng", style: TextStyle(fontSize: 12)),
                      Text("abCxyz123", style: TextStyle(fontSize: 12)),
                    ],
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Thời gian đặt hàng",
                          style: TextStyle(fontSize: 12)),
                      Text("1:30 28/10/2023", style: TextStyle(fontSize: 12)),
                    ],
                  )
                ],
              ),
            ),
            actionButton(context, step, handleActionButton)
          ],
        ),
      ),
    );
  }
}

Widget actionButton(BuildContext context, int step, void handleActionButton) {
  return step == 0
      ? Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlineAppButton(
              title: "Từ chối",
              onPressed: () {
                modalDialog(context, false, handleActionButton);
              },
              widthButton: MediaQuery.sizeOf(context).width * 0.32,
              heightButton: 32,
            ),
            const SizedBox(
              width: 8,
            ),
            DefaultButton(
                title: "Chấp nhận",
                onPressed: () {
                  modalDialog(context, true, handleActionButton);
                },
                heightButton: 32,
                widthButton: MediaQuery.sizeOf(context).width * 0.32)
          ],
        )
      : step == 1
          ? Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OutlineAppButton(
                  widthButton: MediaQuery.sizeOf(context).width * 0.6,
                  title: "Hủy đơn",
                  onPressed: () {
                    modalDialog(context, false, handleActionButton);
                  },
                  heightButton: 40,
                ),
              ],
            )
          : Container();
}

Widget productItem() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Container(
        margin: const EdgeInsets.only(bottom: 8),
        child: Row(
          children: [
            Image.network(
                'https://cdn.tgdd.vn/2021/08/CookProduct/1-1200x676-42.jpg',
                height: 40,
                width: 40,
                fit: BoxFit.cover),
            const SizedBox(width: 8),
            const Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Bánh mì nhân chi sơ, tính ngang tàn",
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 12)),
                Text("Đơn giá: 20.000", style: TextStyle(fontSize: 10)),
                Text("Số lượng: 100", style: TextStyle(fontSize: 10)),
              ],
            ),
          ],
        ),
      ),
      const Center(
        child: Text("2.000.000", style: TextStyle(fontSize: 12)),
      )
    ],
  );
}

Future modalDialog(BuildContext context, bool status, void handleActionButton) {
  return showDialog(
      context: context,
      builder: (context) => AlertDialog(
            insetPadding: const EdgeInsets.all(4),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 24),
                  child: Text(
                    status
                        ? "Bạn có chắc chắn nhận đơn này không?"
                        : "Bạn có chắc chắn từ chối đơn này không?",
                    style: const TextStyle(fontWeight: FontWeight.w600),
                  ),
                ),
                SizedBox(
                  height: 40,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      OutlineAppButton(
                        title: "Từ chối",
                        onPressed: () {},
                        widthButton: MediaQuery.sizeOf(context).width * 0.35,
                        fontSize: 14,
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      DefaultButton(
                          title: "Chấp nhận",
                          onPressed: () {
                            // handleActionButton(1)
                          },
                          fontSize: 14,
                          widthButton: MediaQuery.sizeOf(context).width * 0.35)
                    ],
                  ),
                )
              ],
            ),
          ));
}
