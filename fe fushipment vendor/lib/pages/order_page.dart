import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';
import 'package:fushipment/widgets/card/order_item.dart';
import 'package:fushipment/widgets/layout/default_layout.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({super.key});

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  late List<String> _tabButton;
  late int tabSelected;

  @override
  void initState() {
    super.initState();
    _tabButton = ["Chờ xác nhận", "Đang chuẩn bị", "Đã giao", "Bị hủy"];
    tabSelected = 0;
  }

  final List<Map<String, dynamic>> orderItems = [{}, {}, {}];

  void _changeTabSelected(index) {
    setState(() {
      tabSelected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: ListView(
        children: [
          Row(
            children: _tabButton.asMap().entries.map((entry) {
              final item = entry.value;
              final key = entry.key;
              final bool selected = key == tabSelected;
              return Expanded(
                  child: SizedBox(
                    height: 28,
                    child: Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: selected
                          ? DefaultButton(
                        onPressed: () => _changeTabSelected(key),
                        title: item,
                      )
                          : OutlineAppButton(
                        title: item,
                        onPressed: () => _changeTabSelected(key),
                      ),
                    ),
                  ));
            }).toList(),
          ),
          Column(
            children: orderItems.asMap().entries.map((entry) {
              final item = entry.value;
              return CardOrderItem(tabSelected: tabSelected,);
            }).toList(),
          )
        ],
      ),
    );
  }
}
