import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/pages/home_page.dart';
import 'package:fushipment/services/category.dart';
import 'package:fushipment/services/product.dart';
import 'package:fushipment/services/singleton/http_singleton.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_icons.dart';
import 'package:fushipment/values/app_styles.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:velocity_x/velocity_x.dart';

class ProductDetail extends StatefulWidget {
  final dynamic productData;

  const ProductDetail({super.key, this.productData});

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  MyHttpWithSingleton httpSingleton = MyHttpWithSingleton();
  late CategoryService _categoryService;
  late ProductService _productService;
  late int productCategory;
  late TextEditingController productName = TextEditingController();
  late TextEditingController productDescription = TextEditingController();
  late TextEditingController productPrice = TextEditingController();
  late bool productStatus;
  late String productImage = "";
  File? image;
  List<dynamic> listCategory = [
    {"categoryId": "-1", "categoryName": ""}
  ];
  final List<Map<String, dynamic>> listStatus = [
    {"id": false, "name": "Không bán"},
    {"id": true, "name": "Mở bán"}
  ];

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;

      final imageTemporary = File(image.path);
      setState(() {
        this.image = imageTemporary;
        productImage = basename(imageTemporary.path);
      });
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<ImageSource?> showImageSource(BuildContext context) async {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<ImageSource>(
          context: context,
          builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.camera);
                      },
                      child: Text("Camera")),
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.gallery);
                      },
                      child: Text("Gallery"))
                ],
              ));
    } else {
      return showModalBottomSheet(
          context: context,
          builder: (context) => Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                    onTap: () {
                      Navigator.of(context).pop(ImageSource.camera);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.image_outlined),
                    title: Text("Gallery"),
                    onTap: () {
                      Navigator.of(context).pop(ImageSource.gallery);
                    },
                  )
                ],
              ));
    }
  }

  Future<void> getListCategory() async {
    List<dynamic> categories = await _categoryService.getListCategory();
    setState(() {
      listCategory = categories;
    });
  }

  Future<void> submitProduct(BuildContext context) async {
    await _productService.submitProduct(
        storeId: 1,
        productData: widget.productData,
        productCategory: productCategory,
        productName: productName,
        productPrice: productPrice,
        productDescription: productDescription,
        productImage: productImage,
        productStatus: productStatus,
        context: context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _categoryService = CategoryService(httpSingleton);
    _productService = ProductService(httpSingleton);
    productCategory = -1;
    productStatus = true;
    productPrice.text = "0";
    initializeData();
  }

  Future<void> initializeData() async {
    await getListCategory();
    if (widget.productData != null) {
      productImage =
          "https://s3-alpha-sig.figma.com/img/0309/c588/4ef76a3cf96cfc11ee955e9d13d6e1e3?Expires=1700438400&Signature=OUbZJf9A-WSgKxgE6ywPxbuX4UX8Ne2MAuBScCjM9UG0Cc4FQGgfDI5kZ-66javxtc5Xxj08WscD0CXUPmiUDFzHGHkrXSYSCv7YLdr0PA01HBpyHaBO~io-12y8TkPUmYdLrxk2~141O86JtYVATvydnkp2q7lVfLFnIvELJQi5prtwG5Rl-05AaJJSqKo6C-6bNrAp7xzd3qGZPxwDkE1poIU3Awk~twTYYWCNDeKFuNS2~ANf5DaA4DezNdUPGi3IPrgc3ns-8sKKrRSlAEstCgETWITSsikHoqxCG1BZ2IQ3t4QVvT0XSMbsamaYqd6TgS~UM-74Q65A1GtV-Q__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4";
      productName.text = widget.productData["productName"];
      productDescription.text = widget.productData["description"];
      productCategory = widget.productData["categoryId"] ?? -1;
      productPrice.text =
          Helpers.convertToMoneyVietNam(widget.productData["price"], "");
      productStatus = widget.productData["isInStock"];
    }
  }

  @override
  Widget build(BuildContext context) {
    String imagePath = image != null ? image!.path : productImage;
    final imageShow = imagePath!.contains('https://')
        ? NetworkImage(imagePath)
        : FileImage(File(imagePath));
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.whiteColor,
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: [
            Container(
              height: MediaQuery.sizeOf(context).height * 0.9,
              child: ListView(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 28),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(Icons.arrow_back,
                              color: AppColors.primaryColor, size: 24),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Text(
                            widget.productData == null
                                ? "Thêm sản phẩm"
                                : "Chi tiết sản phẩm",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600)),
                      ],
                    ),
                  ),
                  imagePath != ""
                      ? Column(
                          children: [
                            Text(
                              "Ảnh sản phẩm",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 15),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            UnconstrainedBox(
                              child: Container(
                                width: 224,
                                height: 224,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12),
                                  child: Ink.image(
                                    image: imageShow as ImageProvider,
                                    fit: BoxFit.cover,
                                    width: 100,
                                    height: 100,
                                    child: InkWell(
                                      onTap: () async {
                                        final source =
                                            await showImageSource(context);
                                        if (source == null) return;
                                        pickImage(source);
                                        // ValueChanged<ImageSource>(source);
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            UnconstrainedBox(
                              child: Container(
                                width: 160,
                                height: 24,
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: OutlineAppButton(
                                      type: "disable",
                                      title: "Xóa",
                                      onPressed: () {
                                        image = null;
                                        imagePath = "";
                                        setState(() {
                                          productImage = "";
                                        });
                                      },
                                    )),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Expanded(
                                        child: OutlineAppButton(
                                      type: "disable",
                                      title: "Chỉnh sửa",
                                      onPressed: () {},
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      : Container(
                          child: Container(
                            alignment: Alignment.bottomLeft,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Ảnh sản phẩm",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15),
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                InkWell(
                                  child: SvgPicture.asset(AppIcons.addImage),
                                  onTap: () async {
                                    final source =
                                        await showImageSource(context);
                                    if (source == null) return;
                                    pickImage(source);
                                    // ValueChanged<ImageSource>(source);
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 6),
                    height: 32,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Text(
                              "Danh mục",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )),
                        Expanded(
                            flex: 2,
                            child: DropdownButtonFormField(
                                style: TextStyle(
                                    fontSize: 13, color: AppColors.blackColor),
                                isExpanded: true,
                                decoration: AppStyles.dropDownOutline,
                                value: productCategory,
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: AppColors.primaryColor,
                                ),
                                items: [
                                  DropdownMenuItem(
                                    value: -1,
                                    child: Text("Chọn danh mục sản phẩm"),
                                  ),
                                  ...listCategory.asMap().entries.map((e) {
                                    final value = e.value ?? {};
                                    final id = value["categoryId"];
                                    final name = value["categoryName"];
                                    return DropdownMenuItem(
                                      value: id,
                                      child: Text(name),
                                    );
                                  }).toList()
                                ],
                                onChanged: (value) {
                                  setState(() {
                                    print(value as int);
                                    productCategory = value as int;
                                  });
                                })),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 6),
                    height: 32,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Text(
                              "Tên món",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )),
                        Expanded(
                            flex: 2,
                            child: TextField(
                                controller: productName,
                                onChanged: (value) {
                                  setState(() {
                                    productName.text = value;
                                  });
                                },
                                textAlignVertical: TextAlignVertical.center,
                                style: TextStyle(
                                  fontSize: 13, // This is not so important
                                ),
                                decoration: AppStyles.outlineInput)),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 6),
                    height: 72,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            flex: 1,
                            child: Text(
                              "Mô tả",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )),
                        Expanded(
                            flex: 2,
                            child: TextField(
                                controller: productDescription,
                                onChanged: (value) {
                                  setState(() {
                                    productDescription.text = value;
                                  });
                                },
                                minLines: 6,
                                maxLines: null,
                                keyboardType: TextInputType.multiline,
                                textAlignVertical: TextAlignVertical.center,
                                style: TextStyle(
                                  fontSize: 13, // This is not so important
                                ),
                                decoration: AppStyles.outlineTextarea)),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 6),
                    height: 32,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Text(
                              "Giá bán",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )),
                        Expanded(
                            flex: 2,
                            child: TextField(
                                controller: productPrice,
                                onChanged: (value) {
                                  setState(() {
                                    var formattedValue = _formatCurrency(value);
                                    productPrice.value = TextEditingValue(
                                      text: formattedValue,
                                      selection: TextSelection.collapsed(
                                          offset: formattedValue.length - 1),
                                    );
                                  });
                                },
                                textAlignVertical: TextAlignVertical.center,
                                style: TextStyle(
                                  fontSize: 13, // This is not so important
                                ),
                                decoration: AppStyles.outlineInput)),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 6),
                    height: 32,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Text(
                              "Trạng thái",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )),
                        Expanded(
                            flex: 2,
                            child: DropdownButtonFormField(
                                style: TextStyle(
                                    fontSize: 13, color: AppColors.blackColor),
                                isExpanded: true,
                                decoration: AppStyles.dropDownOutline,
                                value: productStatus,
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: AppColors.primaryColor,
                                ),
                                items: listStatus.asMap().entries.map((e) {
                                  final value = e.value;
                                  final id = value["id"];
                                  final name = value["name"];
                                  return DropdownMenuItem(
                                    child: Text(name),
                                    value: id,
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    productStatus = value as bool;
                                  });
                                })),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OutlineAppButton(
                  title: "Hủy",
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  type: "disable",
                  widthButton: 120,
                ),
                SizedBox(
                  width: 8,
                ),
                DefaultButton(
                  title: "Lưu",
                  onPressed: () async {
                    await submitProduct(context);
                  },
                  widthButton: 120,
                  type: (productImage != null && productImage != "") &&
                          productCategory != null &&
                          (productName != null && productName != "") &&
                          (productDescription != null &&
                              productDescription != "") &&
                          productPrice != null &&
                          productStatus != null
                      ? "default"
                      : "disable",
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  String _formatCurrency(String value) {
    // Remove non-digit characters, except for "đ"
    var digitsOnly = value.replaceAll(RegExp(r'[^\dđ]'), '');

    // Parse the numeric value
    var numericValue = int.tryParse(digitsOnly) ?? 0;

    // Format the numeric value as Vietnamese currency
    var formattedValue =
        NumberFormat.currency(locale: 'vi_VN', symbol: '').format(numericValue);

    // Check if the last character in the original value was a backspace
    var isBackspace = value.isNotEmpty && value[value.length - 1] == '\b';

    // If backspace, remove the last character from the formatted value
    if (isBackspace && formattedValue.length > 1) {
      formattedValue = formattedValue.substring(0, formattedValue.length - 1);
    }

    return formattedValue;
  }
}
