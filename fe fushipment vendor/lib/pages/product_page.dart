import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fushipment/core/store.dart';
import 'package:fushipment/services/category.dart';
import 'package:fushipment/services/product.dart';
import 'package:fushipment/services/singleton/http_singleton.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_styles.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';
import 'package:fushipment/widgets/card/product_item.dart';
import 'package:http/http.dart';

class ProductPage extends StatefulWidget {
  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  MyHttpWithSingleton httpSingleton = MyHttpWithSingleton();
  ScrollController _scrollController = ScrollController();
  int _currentPage = 1;
  int _currentCategory = -1;
  late ProductService _productService;
  late CategoryService _categoryService;
  late List<String> _tabButton;
  late int tabSelected;
  List<dynamic> listCategory = [
    {"categoryId": "-1", "categoryName": "Danh mục"}
  ];
  late List<dynamic> productItems = [];
  void _changeTabSelected(index) {
    setState(() {
      tabSelected = index;
      productItems = [];
      getListProduct(storeId: 1, isInStock: index == 0 ? true : false);
    });
  }

  @override
  void initState() {
    super.initState();
    _tabButton = ["Đang bán", "Tạm dừng"];
    tabSelected = 0;
    _productService = ProductService(httpSingleton);
    _categoryService = CategoryService(httpSingleton);
    initializeData();
  }

  Future<void> initializeData() async {
    await getListProduct(
        storeId: 1, isInStock: tabSelected == 0 ? true : false);
    await getListCategory();
  }

  Future<void> getListProduct({
    required int storeId,
    int categoryId = 0,
    String productName = "",
    bool isInStock = true,
    int page = 1,
    int pageSize = 2,
  }) async {
    List<dynamic> products = await _productService.getListProduct(
      storeId: storeId,
      categoryId: categoryId,
      productName: productName,
      isInStock: isInStock,
      page: page,
      pageSize: pageSize,
    );
    setState(() {
      productItems = [...productItems, ...products];
    });
  }

  Future<void> getListCategory() async {
    List<dynamic> categories = await _categoryService.getListCategory();
    setState(() {
      listCategory = categories;
    });
  }

  Future<void> submitProduct(BuildContext context, productData) async {
    final Map<String, dynamic> bodies = productData;
    try {
      late Response response;
      bodies["isInStock"] = !productData["isInStock"];
      response = await httpSingleton.putRequestWithAuthorize(
          endpoint: "/product/update_product", bodies: bodies);

      if (response.body.isNotEmpty) {
        final responseBodyDecode = jsonDecode(response.body);
        if (responseBodyDecode != null) {
          if (responseBodyDecode["status"] == "200") {
            if (context.mounted) {
              final scaffold = ScaffoldMessenger.of(context);
              scaffold.showSnackBar(SnackBar(
                content: Text(responseBodyDecode["message"]),
                action: SnackBarAction(
                  label: 'Cancel',
                  onPressed: scaffold.hideCurrentSnackBar,
                ),
              ));
            }
            await getListProduct(
                storeId: 1, isInStock: tabSelected == 0 ? true : false);
          }
        }
      } else {
        if (context.mounted) {
          final scaffold = ScaffoldMessenger.of(context);
          scaffold.showSnackBar(SnackBar(
            content: const Text("Server Busy"),
            action: SnackBarAction(
              label: 'Cancel',
              onPressed: scaffold.hideCurrentSnackBar,
            ),
          ));
        }
      }
    } catch (error) {
      print(error);
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Row(
            children: _tabButton.asMap().entries.map((entry) {
              final item = entry.value;
              final key = entry.key;
              final bool selected = key == tabSelected;
              return Expanded(
                  child: SizedBox(
                height: 28,
                child: Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: selected
                      ? DefaultButton(
                          fontSize: 12,
                          onPressed: () => _changeTabSelected(key),
                          title: item,
                        )
                      : OutlineAppButton(
                          fontSize: 12,
                          title: item,
                          onPressed: () => _changeTabSelected(key),
                        ),
                ),
              ));
            }).toList(),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 32,
            child: Row(
              children: [
                Expanded(
                  child: DropdownButtonFormField(
                      style:
                          TextStyle(fontSize: 13, color: AppColors.blackColor),
                      isExpanded: true,
                      decoration: AppStyles.dropDownOutline,
                      value: _currentCategory,
                      icon: Icon(
                        Icons.arrow_drop_down,
                        color: AppColors.primaryColor,
                      ),
                      items: [
                        DropdownMenuItem(
                          value: -1,
                          child: Text("Danh mục"),
                        ),
                        ...listCategory.asMap().entries.map((e) {
                          final value = e.value ?? {};
                          final id = value["categoryId"];
                          final name = value["categoryName"];
                          return DropdownMenuItem(
                            value: id,
                            child: Text(name),
                          );
                        }).toList()
                      ],
                      onChanged: (value) {
                        setState(() {
                          _currentCategory = value as int;
                          productItems = [];
                        });
                        getListProduct(
                            storeId: 1,
                            isInStock: tabSelected == 0 ? true : false,
                            categoryId: value as int);
                      }),
                  flex: 1,
                ),
                Expanded(
                  child: SizedBox(),
                  flex: 2,
                )
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          NotificationListener(
            onNotification: (notification)  {
              print(_scrollController.position.pixels == _scrollController.position.maxScrollExtent);
              if (notification is ScrollEndNotification) {
                getListProduct( storeId: 1,
                    isInStock: tabSelected == 0 ? true : false,
                    page: _currentPage + 1 ,
                    categoryId: _currentCategory);
                setState(() {
                  _currentPage += 1;
                });
              }
              return true;
            },
            child: Expanded(
              child: ListView(
                controller: _scrollController,
                children: [
                  Column(
                    children: productItems.asMap().entries.map((e) {
                      final item = e.value;
                      return ProductItem(
                        tabSelected: tabSelected,
                        changeStatusProduct: () async {
                          await submitProduct(context, item);
                        },
                        item: item,
                      );
                    }).toList(),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
