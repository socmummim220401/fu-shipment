import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/services/singleton/http_singleton.dart';
import 'package:fushipment/values/app_assets.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_icons.dart';
import 'package:fushipment/values/app_styles.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';
import 'package:http/http.dart';

class StoreDetailPage extends StatefulWidget {
  @override
  State<StoreDetailPage> createState() => _StoreDetailPageState();
}

class _StoreDetailPageState extends State<StoreDetailPage> {
  TextEditingController storeName = TextEditingController();
  TextEditingController storeDescription = TextEditingController();
  TextEditingController storeAddress = TextEditingController();
  TextEditingController storePhone = TextEditingController();
  TextEditingController storeEmail = TextEditingController();
  TextEditingController storeType = TextEditingController();
  TextEditingController storeOpen = TextEditingController();
  TextEditingController storeClose = TextEditingController();
  MyHttpWithSingleton httpSingleton = MyHttpWithSingleton();
  bool readOnly = true;
  late List<Map<String, dynamic>> textField;

  TimeOfDay selectedTime = TimeOfDay.now();

  Future<void> getStoreDetail() async {
    final Map<String, dynamic> bodies = {
      "storeId": 0,
      "userId": 1,
    };
    Response response = await httpSingleton.postRequestWithAuthorize(
        endpoint: "/store/get_store", bodies: bodies);
    if (response.body.isNotEmpty) {
      final bodyDecode = json.decode(response.body);
      if (bodyDecode["message"] == "Done" &&
          bodyDecode["status"] == "200" &&
          bodyDecode["data"] != null) {
        final resultData = bodyDecode["data"];
        setState(() {
          if (resultData["store"] != null) {
            final storeData = resultData["store"];
            storeName.text = storeData["storeName"];
            storeAddress.text = storeData["storeAddress"];
            storePhone.text = storeData["phone"];
            storeOpen.text = Helpers.getTimeFromString(storeData["openAt"]).format(context);
            storeClose.text = Helpers.getTimeFromString(storeData["closedAt"]).format(context);
            storeDescription.text = storeData["description"];
          }
        });
      }
    }
  }

  Future<void> updateStore() async {
    final Map<String, dynamic> bodies = {
      "storeId": 1,
      "userId": 1,
      "storeName": storeName.text,
      "storeAddress": storeAddress.text,
      "phone": storePhone.text,
      "description": storeDescription.text,
      "openAt": Helpers.convertTimeOfDayToString(storeOpen.text),
      "closedAt": Helpers.convertTimeOfDayToString(storeClose.text),
      "active": true
    };
    try {
      late Response response;
        response = await httpSingleton.putRequestWithAuthorize(
            endpoint: "/store/update_store", bodies: bodies);
      if (response.body.isNotEmpty) {
        final responseBodyDecode = jsonDecode(response.body);
        if (responseBodyDecode != null) {
          if (responseBodyDecode["status"] == "200") {
            if (context.mounted) {
              final scaffold = ScaffoldMessenger.of(context);
              scaffold.showSnackBar(SnackBar(
                content: Text(responseBodyDecode["message"]),
                action: SnackBarAction(
                  label: 'Cancel',
                  onPressed: scaffold.hideCurrentSnackBar,
                ),
              ));
            }
            setState(() {
              readOnly = !readOnly;
            });
          }
        }
      } else {
        if (context.mounted) {
          final scaffold = ScaffoldMessenger.of(context);
          scaffold.showSnackBar(SnackBar(
            content: const Text("Server Busy"),
            action: SnackBarAction(
              label: 'Cancel',
              onPressed: scaffold.hideCurrentSnackBar,
            ),
          ));
        }
      }
    } catch (error) {
      print(error);
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }

  Future<void> _selectTime(BuildContext context, bool store) async {
    final TimeOfDay? pickedTime = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (pickedTime != null && pickedTime != selectedTime) {
      setState(() {
        selectedTime = pickedTime;
        store
            ? storeOpen.text = pickedTime.format(context)
            : storeClose.text = pickedTime.format(context);
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getStoreDetail();
    textField = [
      {
        "name": "Tên cửa hàng",
        "controller": storeName,
        "onChange": (String value) {
          setState(() {
            storeName.text = value;
          });
        }
      },
      {
        "name": "Địa chỉ",
        "controller": storeAddress,
        "onChange": (String value) {
          setState(() {
            storeAddress.text = value;
          });
        }
      },
      {
        "name": "Số điện thoại",
        "controller": storePhone,
        "onChange": (String value) {
          setState(() {
            storePhone.text = value;
          });
        }
      },
      {
        "name": "Email",
        "controller": storeEmail,
        "onChange": (String value) {
          setState(() {
            storeEmail.text = value;
          });
        }
      },
      {
        "name": "Loại hình",
        "controller": storeType,
        "onChange": (String value) {
          setState(() {
            storeType.text = value;
          });
        }
      },
    ];
  }



  Future modalDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              insetPadding: const EdgeInsets.all(4),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 24),
                    child: Text(
                      "Bạn có chắc chắn hủy cập nhật không?",
                      style: const TextStyle(fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        OutlineAppButton(
                          title: "Không",
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          widthButton: MediaQuery.sizeOf(context).width * 0.35,
                          fontSize: 14,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        DefaultButton(
                            title: "Hủy",
                            onPressed: () {
                              setState(() {
                                readOnly = !readOnly;
                              });
                              Navigator.pop(context);
                            },
                            fontSize: 14,
                            widthButton:
                                MediaQuery.sizeOf(context).width * 0.35)
                      ],
                    ),
                  )
                ],
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 28, vertical: 24),
          width: MediaQuery.sizeOf(context).width,
          height: 120,
          decoration: BoxDecoration(
            color: AppColors.blackColor,
            image: DecorationImage(
              image: NetworkImage(
                  "https://s3-alpha-sig.figma.com/img/c02f/8307/c372dd61b1fe0344364f0d514c564bb3?Expires=1700438400&Signature=ce5Kjs1uLfGzIEeja1bP1BvYLLy~P5UZ5QDcGPJJgJHPXrwJdBdwkLSRhLCo2sQTFc7fUB-Sdxlq1QIUBiny1EycDhc1OFvS~udIpt5qBD5IUENxEqreioLHRk4rstUy547-e5MhXDv04jkRJyZbud26Q3EnnO8Brg0ShiDNbuwBchkr5lCVYOk5ewDwP8dxah2SZfAT2LSBXE3ssb2Q-6uj2w9kUrAt7Q0wOOzLCRzlhFb9fJJlT7Rb~X0dXvtdaiUo0sNaVnWWXAzvBfdLeGi4keuQYcdRrD~Fm~cyXYqsD6p28hyeWAIyoIZD4379DKdboJeC6obA-x-h6eywWg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"),
              fit: BoxFit.cover,
              opacity: 0.5,
            ),
          ),
          child: Stack(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      Container(
                        width: 70,
                        height: 70,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              AppAssets.teoEmPath,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                          right: 0,
                          child: Container(
                              width: 18,
                              height: 18,
                              decoration: BoxDecoration(
                                  color: AppColors.whiteColor,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(
                                Icons.circle,
                                color: AppColors.onlineColor,
                                size: 16,
                              ))),
                    ],
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Bánh Mì Tèo Em - Cơ sở 1",
                        style: TextStyle(
                            color: AppColors.whiteColor,
                            fontWeight: FontWeight.w600,
                            fontSize: 16),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        "Đang mở cửa",
                        style: TextStyle(
                            color: AppColors.whiteColor, fontSize: 12),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                        decoration: BoxDecoration(
                            color: AppColors.blackColor.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(50)),
                        child: Row(
                          children: [
                            Icon(
                              Icons.star,
                              color: Colors.orangeAccent,
                              size: 16,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Text("4.0/5.0",
                                style: TextStyle(
                                    color: AppColors.whiteColor, fontSize: 12)),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Positioned(
                right: 0,
                bottom: 0,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      readOnly = !readOnly;
                    });
                  },
                  child: SvgPicture.asset(
                    AppIcons.edit,
                    width: 24,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              readOnly ? Text(
                storeDescription.text,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
              ) : TextField(
                  controller: storeDescription,
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      storeDescription.text = value;
                    });
                  },
                  minLines: 4,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  textAlignVertical: TextAlignVertical.center,
                  style: TextStyle(
                    fontSize: 13, // This is not so important
                  ),
                  decoration: AppStyles.outlineTextarea),
              SizedBox(
                height: 20,
              ),
              Column(
                children: textField.asMap().entries.map((e) {
                  final item = e.value;
                  final title = item["name"];
                  final controller = item["controller"];
                  final onChange = item["onChange"];
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 6),
                    height: 32,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Text(
                              title,
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )),
                        Expanded(
                            flex: 2,
                            child: TextField(
                                readOnly: readOnly,
                                controller: controller,
                                onChanged: (value) {
                                  onChange(value);
                                },
                                textAlignVertical: TextAlignVertical.center,
                                style: TextStyle(
                                  fontSize: 13, // This is not so important
                                ),
                                decoration: AppStyles.outlineInput))
                      ],
                    ),
                  );
                }).toList(),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 6),
                height: 32,
                child: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Text(
                          "Giờ mở cửa",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        )),
                    Expanded(
                        flex: 2,
                        child: TextField(
                            readOnly: readOnly,
                            controller: storeOpen,
                            // onChanged: (value) {
                            //   onChange(value);
                            // },
                            onTap: () =>
                                readOnly ? {} : _selectTime(context, true),
                            textAlignVertical: TextAlignVertical.center,
                            style: TextStyle(
                              fontSize: 13, // This is not so important
                            ),
                            decoration: AppStyles.outlineInput))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 6),
                height: 32,
                child: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Text(
                          "Giờ đóng cửa",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        )),
                    Expanded(
                        flex: 2,
                        child: TextField(
                            readOnly: readOnly,
                            controller: storeClose,
                            // onChanged: (value) {
                            //   onChange(value);
                            // },
                            onTap: () =>
                                readOnly ? {} : _selectTime(context, false),
                            textAlignVertical: TextAlignVertical.center,
                            style: TextStyle(
                              fontSize: 13, // This is not so important
                            ),
                            decoration: AppStyles.outlineInput))
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              readOnly
                  ? Container()
                  : Row(
                      children: [
                        Expanded(
                            child: OutlineAppButton(
                          title: "Hủy",
                          onPressed: () {
                            modalDialog(context);
                          },
                          heightButton: 32,
                          fontSize: 14,
                        )),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: DefaultButton(
                                title: "Lưu",
                                onPressed: () async {
                                   await updateStore();
                                },
                                heightButton: 32,
                                fontSize: 14))
                      ],
                    )
            ],
          ),
        ),
      ],
    );
  }
}
