import 'dart:convert';

import 'package:fushipment/services/singleton/http_singleton.dart';
import 'package:http/http.dart';

class CategoryService {
  final MyHttpWithSingleton _httpWithSingleton;

  CategoryService(this._httpWithSingleton);

  Future<List<dynamic>> getListCategory() async {
    try {
      const storeId = 1;
      Response response = await _httpWithSingleton.getRequestWithAuthorize(
          endpoint: "/category/get_category/$storeId");
      if (response.body.isNotEmpty) {
        final bodyDecode = json.decode(response.body);
        if (bodyDecode["message"] == "Get data successfully" &&
            bodyDecode["status"] == "200" &&
            bodyDecode["data"] != null) {
          return bodyDecode["data"];
        }
      }
    } catch (error) {
      print(error);
    }
    return [];
  }
}
