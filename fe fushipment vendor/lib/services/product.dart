import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/pages/home_page.dart';
import 'package:fushipment/services/singleton/http_singleton.dart';
import 'package:http/http.dart';

class ProductService {
  final MyHttpWithSingleton _httpSingleton;

  ProductService(this._httpSingleton);

  Future<List<dynamic>> getListProduct({
    required int storeId,
    int categoryId = 0,
    String productName = "",
    bool isInStock = true,
    int page = 1,
    int pageSize = 5,
  }) async {
    final Map<String, dynamic> bodies = {
      "storeId": storeId,
      "categoryId": categoryId,
      "productName": productName,
      "isInStock": isInStock,
      "page": page,
      "pageSize": pageSize,
    };

    try {
      Response response = await _httpSingleton.postRequestWithAuthorize(
        endpoint: "/product/get_product",
        bodies: bodies,
      );

      if (response.body.isNotEmpty) {
        final bodyDecode = json.decode(response.body);
        if (bodyDecode["message"] == "Success" &&
            bodyDecode["resultData"] != null) {
          return bodyDecode["resultData"];
        }
      }
    } catch (error) {
      print(error);
    }

    return [];
  }

  Future<void> submitProduct({required int storeId,
    required int productCategory,
    required TextEditingController productName,
    required TextEditingController productPrice,
    required TextEditingController productDescription,
    required String productImage,
    required bool productStatus,
    required BuildContext context,
    dynamic productData,
  }) async {
    final Map<String, dynamic> bodies = {
      "storeId": storeId,
      "categoryId": productCategory,
      "productName": productName.text,
      "price": Helpers.convertMoneyVietNamToNumber(productPrice.text, ""),
      "description": productDescription.text,
      "image": productImage,
      "isInStock": productStatus
    };
    try {
      late Response response;
      if (productData != null) {
        bodies["productId"] = productData["productId"];
        bodies["image"] = "image${productData["productId"]}.jpg";
        response = await _httpSingleton.putRequestWithAuthorize(
            endpoint: "/product/update_product", bodies: bodies);
      } else {
        response = await _httpSingleton.postRequestWithAuthorize(
            endpoint: "/product/create_product", bodies: bodies);
      }
      if (response.body.isNotEmpty) {
        final responseBodyDecode = jsonDecode(response.body);
        if (responseBodyDecode != null) {
          if (responseBodyDecode["status"] == "200") {
            if (context.mounted) {
              final scaffold = ScaffoldMessenger.of(context);
              scaffold.showSnackBar(SnackBar(
                content: Text(responseBodyDecode["message"]),
                action: SnackBarAction(
                  label: 'Cancel',
                  onPressed: scaffold.hideCurrentSnackBar,
                ),
              ));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          const HomePage(
                            selectedIndex: 2,
                          )));
            }
          }
        }
      } else {
        if (context.mounted) {
          final scaffold = ScaffoldMessenger.of(context);
          scaffold.showSnackBar(SnackBar(
            content: const Text("Server Busy"),
            action: SnackBarAction(
              label: 'Cancel',
              onPressed: scaffold.hideCurrentSnackBar,
            ),
          ));
        }
      }
    } catch (error) {
      print(error);
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }
}
