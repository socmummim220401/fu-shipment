import 'dart:convert';
import 'package:fushipment/utils/routes.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyHttpWithSingleton {
  static final MyHttpWithSingleton _singleton = MyHttpWithSingleton._internal();

  factory MyHttpWithSingleton() {
    return _singleton;
  }

  MyHttpWithSingleton._internal();

  late Response response;
  late SharedPreferences prefs;
  late Uri uriParse;

  Future<Response> getRequestWithUnauthorize({
    Map<String, String>? headers = const {
      "Content-Type": "application/json",
    },
    required String endpoint,
  }) async {
    try {
      String url = "${AppRoutes.endPointLoginBackEnd}$endpoint";
      uriParse = Uri.parse(url);
      response = await get(uriParse, headers: headers);
    } catch (error) {
      print(error);
    }
    return response;
  }

  Future<Response> postRequestWithUnauthorize({
    Map<String, dynamic>? bodies = const {},
    Map<String, String>? headers = const {
      "Content-Type": "application/json",
    },
    required String endpoint,
  }) async {
    try {
      String url = "${AppRoutes.endPointLoginBackEnd}$endpoint";
      uriParse = Uri.parse(url);
      response =
          await post(uriParse, headers: headers, body: jsonEncode(bodies));
    } catch (error) {
      print(error);
    }
    return response;
  }

  Future<Response> getRequestWithAuthorize({
    Map<String, String>? headers = const {
      "Content-Type": "application/json",
    },
    required String endpoint,
  }) async {
    try {
      await checkExpiredToken();
      String url = "${AppRoutes.endPointBackEnd}$endpoint";
      uriParse = Uri.parse(url);
      prefs = await SharedPreferences.getInstance();
      final authorization = prefs.getString("authorization");
      if (authorization != "" && authorization != null) {
        final authorizationDecode = json.decode(authorization);
        final token = authorizationDecode["token"];
        if (authorizationDecode != null) {
          headers = {
            "Content-Type": "application/json",
            "Authorization": token
          };
        }
      }
      response = await get(uriParse, headers: headers);
    } catch (error) {
      print(error);
    }
    return response;
  }

  Future<Response> postRequestWithAuthorize({
    Map<String, String>? headers = const {
      "Content-Type": "application/json",
    },
    Map<String, dynamic>? bodies = const {},
    required String endpoint,
  }) async {
    try {
      await checkExpiredToken();

      String url = "${AppRoutes.endPointBackEnd}$endpoint";
      uriParse = Uri.parse(url);
      prefs = await SharedPreferences.getInstance();
      final authorization = prefs.getString("authorization");
      if (authorization != "" && authorization != null) {
        final authorizationDecode = json.decode(authorization);
        final token = authorizationDecode["token"];
        if (token != null) {
          headers = {
            "Content-Type": "application/json",
            "Authorization": token
          };
        }
      }
      print(headers);
      print(jsonEncode(bodies));
      print(uriParse);
      response =
          await post(uriParse, headers: headers, body: jsonEncode(bodies));
    } catch (error) {
      print(error);
    }
    return response;
  }

  Future<Response> putRequestWithAuthorize({
    Map<String, String>? headers = const {
      "Content-Type": "application/json",
    },
    Map<String, dynamic>? bodies = const {},
    required String endpoint,
  }) async {
    try {
      await checkExpiredToken();
      String url = "${AppRoutes.endPointBackEnd}$endpoint";
      uriParse = Uri.parse(url);
      prefs = await SharedPreferences.getInstance();
      final authorization = prefs.getString("authorization");
      if (authorization != "" && authorization != null) {
        final authorizationDecode = json.decode(authorization);
        final token = authorizationDecode["token"];
        if (authorizationDecode != null) {
          headers = {
            "Content-Type": "application/json",
            "Authorization": token
          };
        }
      }
      response =
          await put(uriParse, headers: headers, body: jsonEncode(bodies));
    } catch (error) {
      print(error);
    }
    return response;
  }

  Future<Response> deleteRequestWithAuthorize({
    Map<String, String>? headers = const {
      "Content-Type": "application/json",
    },
    Map<String, String>? bodies = const {},
    required String endpoint,
  }) async {
    try {
      await checkExpiredToken();
      String url = "${AppRoutes.endPointBackEnd}$endpoint";
      uriParse = Uri.parse(url);
      prefs = await SharedPreferences.getInstance();
      final authorization = prefs.getString("authorization");
      if (authorization != "" && authorization != null) {
        final authorizationDecode = json.decode(authorization);
        final token = authorizationDecode["token"];
        if (authorizationDecode) {
          headers = {
            "Content-Type": "application/json",
            "Authorization": token
          };
        }
      }
      response =
          await delete(uriParse, headers: headers, body: jsonEncode(bodies));
    } catch (error) {
      print(error);
    }

    return response;
  }

  Future<void> checkExpiredToken() async {
    try {
      prefs = await SharedPreferences.getInstance();
      final authorization = prefs.getString("authorization");
      if (authorization != "" && authorization != null) {
        final authorizationDecode = json.decode(authorization);
        if (authorizationDecode != null) {
          String expirationTimeString = authorizationDecode["expiredTime"];
          DateTime expirationTime = DateTime.parse(expirationTimeString);
          DateTime currentTime = DateTime.now();
          if (expirationTime.toUtc().isBefore(currentTime.toUtc())) {
            //expired
            final refreshToken =
                "Bearer " + authorizationDecode["refreshToken"];
            print("The object is expired");
            String url = "${AppRoutes.endPointLoginBackEnd}/re_generate_token";
            uriParse = Uri.parse(url);
            response =
                await get(uriParse, headers: {"Authorization": refreshToken});
            if (response.body.isNotEmpty) {
              var responseBody = jsonDecode(response.body);
              if (responseBody != null && responseBody["status"] == "200") {
                prefs.setString("authorization", jsonEncode(responseBody));
              }
            }
          } else {
            print("The object is not expired");
          }
        }
      }
    } catch (error) {
      print(error);
    }
  }
}
