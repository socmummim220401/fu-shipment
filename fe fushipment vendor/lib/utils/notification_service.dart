// import 'dart:convert';
// import 'dart:io';
// import 'dart:math';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:fushipment/core/store.dart';
//
// class NotificationService {
//   FirebaseMessaging messaging = FirebaseMessaging.instance;
//   final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
//       FlutterLocalNotificationsPlugin();
//
//   void initLocalNotification(
//       BuildContext context, RemoteMessage message) async {
//     const androidInitializationSetting =
//         AndroidInitializationSettings('@mipmap/ic_launcher');
//     const iosInitializationSetting = DarwinInitializationSettings();
//     const initSettings = InitializationSettings(
//         android: androidInitializationSetting, iOS: iosInitializationSetting);
//     await _flutterLocalNotificationsPlugin.initialize(initSettings);
//   }
//
//   void requestNotificationPermission() async {
//     NotificationSettings settings = await messaging.requestPermission(
//         alert: true,
//         announcement: true,
//         badge: true,
//         carPlay: true,
//         criticalAlert: true,
//         provisional: true,
//         sound: true);
//     if (settings.authorizationStatus == AuthorizationStatus.authorized) {
//       print("permission");
//     } else if (settings.authorizationStatus == AuthorizationStatus.authorized) {
//       print("personal permission");
//     } else {
//       print("decline");
//     }
//   }
//
//   void firebaseInit(BuildContext context) {
//     FirebaseMessaging.onMessage.listen((message) {
//       UpdateOrderItems(
//           message.data['created_by'], json.decode(message.data['order']));
//       if (Platform.isAndroid) {
//         initLocalNotification(context, message);
//         showNotification(message);
//       } else {
//         showNotification(message);
//       }
//       print("Add");
//     });
//   }
//
//   Future<void> showNotification(RemoteMessage message) async {
//     AndroidNotificationChannel channel = AndroidNotificationChannel(
//         Random.secure().nextInt(100000).toString(),
//         "High Important Notification");
//     AndroidNotificationDetails androidNotificationDetails =
//         AndroidNotificationDetails(
//             channel.id.toString(), channel.name.toString(),
//             channelDescription: channel.description.toString(),
//             importance: Importance.high,
//             priority: Priority.high,
//             ticker: 'ticker');
//     const DarwinNotificationDetails darwinNotificationDetails =
//         DarwinNotificationDetails(
//             presentAlert: true, presentBadge: true, presentSound: true);
//     NotificationDetails notificationDetails = NotificationDetails(
//         android: androidNotificationDetails, iOS: darwinNotificationDetails);
//     Future.delayed(Duration.zero, () {
//       _flutterLocalNotificationsPlugin.show(0, message.notification!.title,
//           message.notification!.body, notificationDetails);
//     });
//   }
//
//   Future<String> getDeviceToken() async {
//     String? token = await messaging.getToken();
//     return token!;
//   }
//
//   void isTokenRefresh() async {
//     messaging.onTokenRefresh.listen((event) {
//       event.toString();
//       print("f5");
//     });
//   }
// }
