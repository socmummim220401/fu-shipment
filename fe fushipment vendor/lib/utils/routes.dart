class AppRoutes {
  // back-end/port
  static const endPointLoginBackEnd = "http://10.0.2.2:8080/api/v1/auth";
  static const endPointBackEnd = "http://10.0.2.2:8080";

  //user
  static const homeRoute = "/home";
  static const loginRoute = "/login";
  static const indexRoute = "/";

  static const webViewGoogleRoute = "/web-view/google-sign-in";
  static const webViewFacebookRoute = "/web-view/facebook-sign-in";

  static const homeRoute2 = "/home2";
  static const cartRoute2 = "/cart2";
  static const otpRoute = "/otp";

  //vendor
  static const yieldRoute = "/yield";
  static const orderRoute = "/order";
  static const addYieldRoute = "/add-yield";

  //login with via-through
  static const signInRoute = "http://be-api.us-east-2.elasticbeanstalk.com/sign_in/user1";
}