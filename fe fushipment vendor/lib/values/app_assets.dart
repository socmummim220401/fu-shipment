// define app of asset

class AppAssets {
  static const String imagePath = 'assets/images/';
  static const String hoLaPath = "${imagePath}logo_name.png";

  //store
  static const String storePath = "${imagePath}store/";
  static const String teoEmPath = "${storePath}teo_em.png";
}
