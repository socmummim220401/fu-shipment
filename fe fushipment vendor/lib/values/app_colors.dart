import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xff3C8ED3);
  static const Color borderColor = Color(0xffd1d1d1);
  static const Color secondaryColor = Color(0xffF5FBFF);
  static const Color whiteColor = Color(0xffffffff);
  static const Color blackColor = Color(0xff000000);
  static const Color dangerousColor = Color(0xffE03500);
  static const  Color onlineColor = Color(0xff36CC47);
  static const  Color disableColor = Color(0xffA8A8A8);
}
