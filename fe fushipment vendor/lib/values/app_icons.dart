class AppIcons {
  //link
  static const String iconPath = "assets/icons/";

  static const String flag = "${iconPath}flag.svg";
  static const String drawer = "${iconPath}drawer.svg";
  static const String store = "${iconPath}store.svg";
  static const String order = "${iconPath}order.svg";
  static const String voucher = "${iconPath}voucher.svg";
  static const String static = "${iconPath}static.svg";
  static const String setting = "${iconPath}setting.svg";
  static const String logOut = "${iconPath}log_out.svg";
  static const String hoLa = "${iconPath}hola.svg";
  static const String user = "${iconPath}user_avatar.svg";
  static const String edit = "${iconPath}edit.svg";
  static const String addProduct = "${iconPath}addProduct.svg";
  static const String addImage = "${iconPath}addImage.svg";

  static const String logoVendor = "${iconPath}logo_navbar_vendor.svg";
}
