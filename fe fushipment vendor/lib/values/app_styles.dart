import 'package:flutter/material.dart';
import 'package:fushipment/values/app_colors.dart';

class FontFamily {
  static const inter = "Inter";
}

class AppStyles {
  //text
  static TextStyle h1 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 109.66, color: Colors.black);
  static TextStyle h2 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 67.77, color: Colors.black);
  static TextStyle h3 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 41.89, color: Colors.black);
  static TextStyle h4 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 25.89, color: Colors.black);
  static TextStyle h5 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 16, color: Colors.black);
  static TextStyle h6 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 9.89, color: Colors.black);

  //drop down
  static InputDecoration dropDownOutline = const InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 12),
      border: OutlineInputBorder());

  //textarea
  static InputDecoration outlineTextarea = const InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      border: OutlineInputBorder());

  //input
  static InputDecoration outlineInput = const InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 12),
      border: OutlineInputBorder());


  //button
  static ButtonStyle defaultButton = const ButtonStyle(
      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 0)),
      foregroundColor: MaterialStatePropertyAll<Color>(Colors.white),
      backgroundColor: MaterialStatePropertyAll<Color>(AppColors.primaryColor),
      shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2)),
              side: BorderSide(color: AppColors.primaryColor))));

  static ButtonStyle dangerousButton = const ButtonStyle(
      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 0)),
      foregroundColor: MaterialStatePropertyAll<Color>(Colors.white),
      backgroundColor:
          MaterialStatePropertyAll<Color>(AppColors.dangerousColor),
      shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2)),
              side: BorderSide(color: AppColors.dangerousColor))));

  static ButtonStyle disableButton = const ButtonStyle(
      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 0)),
      foregroundColor: MaterialStatePropertyAll<Color>(Colors.white),
      backgroundColor:
      MaterialStatePropertyAll<Color>(AppColors.disableColor),
      shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2)),
              side: BorderSide(color: AppColors.disableColor))));

  static ButtonStyle outlineButton = const ButtonStyle(
      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 0)),
      foregroundColor: MaterialStatePropertyAll(AppColors.primaryColor),
      shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              // borderRadius: BorderRadius.circular(18.0),
              borderRadius: BorderRadius.all(Radius.circular(2)),
              side: BorderSide(color: AppColors.primaryColor))));

  static ButtonStyle disableOutlineButton =const ButtonStyle(
      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 0)),
      foregroundColor: MaterialStatePropertyAll(AppColors.disableColor),
      shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            // borderRadius: BorderRadius.circular(18.0),
              borderRadius: BorderRadius.all(Radius.circular(2)),
              side: BorderSide(color: AppColors.disableColor))));
}
