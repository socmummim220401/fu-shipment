import 'package:flutter/material.dart';
import 'package:fushipment/values/app_styles.dart';

class DefaultButton extends StatefulWidget {
  final String? title;
  final VoidCallback? onPressed;
  final double? fontSize;
  final double? widthButton;
  final double? heightButton;
  final String? type; // default, dangerous

  const DefaultButton(
      {super.key,
      required this.title,
      required this.onPressed,
      this.fontSize,
      this.heightButton,
      this.type,
      this.widthButton});

  @override
  State<DefaultButton> createState() => _DefaultButtonState();
}

class _DefaultButtonState extends State<DefaultButton> {
  @override
  Widget build(BuildContext context) {
    String title = widget.title ?? "";
    VoidCallback onPressed = widget.onPressed ?? () {};
    double fontSize = widget.fontSize ?? 10;
    String type = widget.type ?? "default";

    return SizedBox(
      width: widget.widthButton,
      height: widget.heightButton,
      child: ElevatedButton(
        onPressed: onPressed,
        style: type == "dangerous"
            ? AppStyles.dangerousButton
            : type == "disable"
                ? AppStyles.disableButton
                : AppStyles.defaultButton,
        child: Text(
          title,
          style: TextStyle(fontSize: fontSize),
        ),
      ),
    );
  }
}
