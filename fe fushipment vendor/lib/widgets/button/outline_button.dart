import 'package:flutter/material.dart';
import 'package:fushipment/values/app_styles.dart';

class OutlineAppButton extends StatefulWidget {
  final String? title;
  final VoidCallback? onPressed;
  final double? fontSize;
  final double? widthButton;
  final double? heightButton;
  final String? type;

  const OutlineAppButton(
      {super.key,
      required this.title,
      required this.onPressed,
      this.fontSize,
      this.heightButton,
      this.type,
      this.widthButton});

  @override
  State<OutlineAppButton> createState() => _OutlineAppButtonState();
}

class _OutlineAppButtonState extends State<OutlineAppButton> {
  @override
  Widget build(BuildContext context) {
    String title = widget.title ?? "";
    VoidCallback onPressed = widget.onPressed ?? () {};
    double fontSize = widget.fontSize ?? 10;
    String type = widget.type ?? "default";
    return SizedBox(
      width: widget.widthButton,
      height: widget.heightButton,
      child: TextButton(
        onPressed: onPressed,
        style: type == "disable"
            ? AppStyles.disableOutlineButton
            : AppStyles.outlineButton,
        child: Text(
          title,
          maxLines: 1,
          style: TextStyle(fontSize: fontSize),
        ),
      ),
    );
  }
}
