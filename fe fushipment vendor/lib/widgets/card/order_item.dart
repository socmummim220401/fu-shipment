import 'package:flutter/material.dart';
import 'package:fushipment/pages/order_detail_page.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';

class CardOrderItem extends StatefulWidget {
  final int? tabSelected;

  const CardOrderItem({super.key, required this.tabSelected});

  @override
  State<CardOrderItem> createState() => _CardOrderItemState();
}

class _CardOrderItemState extends State<CardOrderItem> {
  @override
  Widget build(BuildContext context) {
    int tabSelected = widget.tabSelected ?? 1;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4, horizontal: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.white,
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4),
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 1.0,
            spreadRadius: 0.1,
          ),
        ],
      ),
      child: InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => OrderDetailPage(tabSelected: tabSelected)));
        },
        child: Container(
          child: Padding(
            padding:
                const EdgeInsets.only(left: 8.0, right: 12, top: 8, bottom: 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Image.network(
                        'https://cdn.tgdd.vn/2021/08/CookProduct/1-1200x676-42.jpg',
                        // Add your image in assets folder
                        width: 52,
                        height: 52,
                        fit: BoxFit.cover),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Lê Anh Sơn',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold)),
                              Text("1:00 AM",
                                  style: TextStyle(
                                      fontSize: 9,
                                      color: AppColors.primaryColor)),
                            ],
                          ),
                          Text('My house 3, Nhà thị đấu Thạch Hòa',
                              style: TextStyle(fontSize: 8)),
                          SizedBox(height: 6),
                          Text('Bánh mỳ, Đồ uống (3 sản phẩm)',
                              style: TextStyle(fontSize: 9)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text('10.000.000đ',
                                  style: TextStyle(
                                      fontSize: 11,
                                      color: AppColors.primaryColor,
                                      fontWeight: FontWeight.w600)),
                              SizedBox(
                                  height: 16,
                                  width: MediaQuery.sizeOf(context).width * 0.3,
                                  child: actionButton(tabSelected, context)),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget actionButton(int tabSelected, BuildContext context) {
  return tabSelected == 0
      ? Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: OutlineAppButton(
                fontSize: 8,
                onPressed: () {},
                title: 'Từ chối',
              ),
            ),
            SizedBox(
              width: 4,
            ),
            Expanded(
              child: DefaultButton(
                fontSize: 8,
                onPressed: () {
                  modalDialog(context, true);
                },
                title: 'Chấp nhận',
              ),
            ),
          ],
        )
      : tabSelected == 1
          ? Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: OutlineAppButton(
                    fontSize: 8,
                    onPressed: () {
                      modalDialog(context, false);
                    },
                    title: 'Hủy đơn',
                  ),
                ),
                SizedBox(
                  width: 4,
                ),
                Expanded(
                  child: DefaultButton(
                    fontSize: 8,
                    onPressed: () {},
                    title: 'Chi tiết',
                  ),
                ),
              ],
            )
          : tabSelected == 2
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: SizedBox(),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Expanded(
                      child: DefaultButton(
                        fontSize: 8,
                        onPressed: () {},
                        title: 'Chi tiết',
                      ),
                    ),
                  ],
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: SizedBox(),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Expanded(
                      child: DefaultButton(
                        fontSize: 8,
                        onPressed: () {},
                        title: 'Chi tiết',
                        type: "dangerous",
                      ),
                    ),
                  ],
                );
}

Future modalDialog(BuildContext context, bool status) {
  return showDialog(
      context: context,
      builder: (context) => AlertDialog(
            insetPadding: const EdgeInsets.all(4),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20, bottom: 24),
                  child: Text(
                    status
                        ? "Bạn có chắc chắn nhận đơn này không?"
                        : "Bạn có chắc chắn từ chối đơn này không?",
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                ),
                Container(
                  height: 40,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      OutlineAppButton(
                        title: "Từ chối",
                        onPressed: () {},
                        widthButton: MediaQuery.sizeOf(context).width * 0.35,
                        fontSize: 14,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      DefaultButton(
                          title: "Chấp nhận",
                          onPressed: () {},
                          fontSize: 14,
                          widthButton: MediaQuery.sizeOf(context).width * 0.35)
                    ],
                  ),
                )
              ],
            ),
          ));
}
