import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/pages/product_detail_page.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/widgets/button/default_button.dart';
import 'package:fushipment/widgets/button/outline_button.dart';

class ProductItem extends StatelessWidget {
  final int tabSelected;
  final dynamic item;
  final VoidCallback changeStatusProduct;

  const ProductItem({super.key, required this.tabSelected, required this.item, required this.changeStatusProduct});

  @override
  Widget build(BuildContext context) {
    print(["item"]);
    final productName = item["productName"];
    final productPrice = item["price"];
    final productImage = item["imgUrl"];
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.white,
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4),
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 1.0,
            spreadRadius: 0.1,
          ),
        ],
      ),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductDetail(
                        productData: item,
                      )));
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  UnconstrainedBox(
                    child: Image.network(
                        productImage == "image1.jpg" || productImage == "string"
                            ? 'https://cdn.tgdd.vn/2021/08/CookProduct/1-1200x676-42.jpg'
                            : 'https://cdn.tgdd.vn/2021/08/CookProduct/1-1200x676-42.jpg',
                        // Add your image in assets folder
                        width: 80,
                        height: 80,
                        fit: BoxFit.cover),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        productName,
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(Helpers.convertToMoneyVietNam(productPrice, "đ"),
                              style: const TextStyle(
                                  fontSize: 12,
                                  decoration: TextDecoration.lineThrough,
                                  color: Color(0xffcccccc),
                                  decorationColor: Color(0xffcccccc),
                                  decorationThickness: 2.0,
                                  fontWeight: FontWeight.w600)),
                          Text(Helpers.convertToMoneyVietNam(productPrice, "đ"),
                              style: const TextStyle(
                                  fontSize: 14,
                                  color: AppColors.primaryColor,
                                  fontWeight: FontWeight.w600)),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: tabSelected == 0
                    ? [
                        DefaultButton(
                          title: "Dừng bán",
                          onPressed: changeStatusProduct,
                          heightButton: 24,
                          widthButton: 80,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        OutlineAppButton(
                            title: "Xóa",
                            onPressed: () {},
                            heightButton: 24,
                            widthButton: 80)
                      ]
                    : [
                        DefaultButton(
                          title: "Bán",
                          onPressed: () {},
                          heightButton: 24,
                          widthButton: 80,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        OutlineAppButton(
                            title: "Xóa",
                            onPressed: () {},
                            heightButton: 24,
                            widthButton: 80)
                      ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
