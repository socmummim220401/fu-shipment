import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fushipment/pages/dashboard_page.dart';
import 'package:fushipment/pages/order_page.dart';
import 'package:fushipment/pages/product_detail_page.dart';
import 'package:fushipment/pages/product_page.dart';
import 'package:fushipment/pages/store_detail_page.dart';
import 'package:fushipment/widgets/layout/widgets/bottom_navigator.dart';
import '../../values/app_colors.dart';
import '../../values/app_icons.dart';

class DefaultLayout extends StatefulWidget {
  final int? selectedIndex;

  const DefaultLayout({super.key, this.selectedIndex});

  @override
  State<DefaultLayout> createState() => _DefaultLayoutState();
}

class _DefaultLayoutState extends State<DefaultLayout> {
  late List<Widget> _pages;
  late int selectedIndex;

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedIndex = widget?.selectedIndex ?? 0;
    _pages = <Widget>[
      const DashboardPage(),
      const OrderPage(),
      ProductPage(),
      const Text('Voucher'),
      StoreDetailPage(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          selectedIndex == 0 ? AppColors.secondaryColor : AppColors.whiteColor,
      appBar: selectedIndex == 4
          ? PreferredSize(
              preferredSize: const Size.fromHeight(80.0),
              child: Container(
                margin: const EdgeInsets.only(top: 20),
                child: AppBar(
                  backgroundColor: AppColors.whiteColor,
                  centerTitle: true,
                  elevation: 0,
                  title: Text(
                    "Thông tin cửa hàng",
                    style: TextStyle(color: AppColors.blackColor),
                  ),
                  leading: Builder(
                    builder: (BuildContext context) {
                      return IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.arrow_back,
                          size: 32,
                          color: AppColors.primaryColor,
                        ),
                      );
                    },
                  ),
                ),
              ),
            )
          : PreferredSize(
              preferredSize: const Size.fromHeight(80.0),
              child: Container(
                margin: const EdgeInsets.only(top: 20),
                child: AppBar(
                  backgroundColor: selectedIndex == 0
                      ? AppColors.secondaryColor
                      : AppColors.whiteColor,
                  centerTitle: true,
                  elevation: 0,
                  title: SvgPicture.asset(AppIcons.logoVendor, width: 132),
                  actions: selectedIndex == 2
                      ? [
                          InkWell(
                            child: SvgPicture.asset(AppIcons.addProduct),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductDetail()));
                            },
                          ),
                          SizedBox(
                            width: 20,
                          )
                        ]
                      : null,
                  // leading: Builder(
                  //   builder: (BuildContext context) {
                  //     return IconButton(
                  //       onPressed: () {
                  //         Scaffold.of(context).openDrawer();
                  //       },
                  //       icon: SvgPicture.asset(
                  //         AppIcons.drawer,
                  //         width: 20,
                  //       ),
                  //     );
                  //   },
                  // ),
                ),
              ),
            ),
      bottomNavigationBar: BottomNavigation(
          selectedIndex: selectedIndex,
          onPress: (index) {
            onItemTapped(index);
          }),
      // drawer: const MyDrawer(),
      body: _pages[selectedIndex],
    );
  }
}
