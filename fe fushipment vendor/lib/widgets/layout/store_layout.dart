import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fushipment/pages/dashboard_page.dart';
import 'package:fushipment/pages/order_page.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_icons.dart';
import 'package:fushipment/widgets/layout/widgets/bottom_navigator.dart';

class StoreLayout extends StatefulWidget {
  @override
  State<StoreLayout> createState() => _StoreLayoutState();
}

class _StoreLayoutState extends State<StoreLayout> {
  late List<Widget> _pages;
  int selectedIndex = 0;

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pages = <Widget>[
      const DashboardPage(),
      const OrderPage(),
      const DashboardPage(),
      const Text('My account'),
    ];
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80.0),
        child: Container(
          margin: const EdgeInsets.only(top: 20),
          child: AppBar(
            backgroundColor: AppColors.whiteColor,
            centerTitle: true,
            elevation: 0,
            title:  Text("Thông tin cửa hàng"),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    size: 20,
                  ),
                );
              },
            ),
            actions: [
              InkWell(
                child: SvgPicture.asset(AppIcons.edit, width: 20,),
                onTap: () {

                },
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigation(
          selectedIndex: selectedIndex,
          onPress: (index) {
            onItemTapped(index);
          }
      ),
      // drawer: const MyDrawer(),
      body: _pages[selectedIndex],
    );
  }
}