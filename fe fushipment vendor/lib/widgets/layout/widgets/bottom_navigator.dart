import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_icons.dart';

class BottomNavigation extends StatefulWidget {
  final int? selectedIndex;
  final ValueChanged<int>? onPress; // Use ValueChanged<int> for onTap

  const BottomNavigation({
    super.key,
    required this.selectedIndex,
    required this.onPress,
  });

  @override
  State<BottomNavigation> createState() => _BottomNavigation();
}

class _BottomNavigation extends State<BottomNavigation> {
  @override
  Widget build(BuildContext context) {
    final widthIcon = MediaQuery.sizeOf(context).width * 0.08;
    return Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(top: BorderSide(color: Colors.grey, width: 1.0))),
        child: BottomNavigationBar(
            backgroundColor: Colors.black,
            selectedItemColor: Colors.pinkAccent,
            unselectedItemColor: Colors.white,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            onTap: (index) {
              if (widget.onPress != null) {
                widget.onPress!(index);
              }
            },
            currentIndex: widget.selectedIndex ?? 0,
            items: [
              BottomNavigationBarItem(
                icon: SvgPicture.asset(AppIcons.hoLa, width: widthIcon),
                label: 'Dashboard',
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(AppIcons.order, width: widthIcon),
                label: 'Order',
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(AppIcons.store, width: widthIcon),
                label: 'Store',
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(AppIcons.voucher, width: widthIcon),
                label: 'Voucher',
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(AppIcons.user, width: widthIcon),
                label: 'Home',
              ),
            ]));
    ;
  }
}
