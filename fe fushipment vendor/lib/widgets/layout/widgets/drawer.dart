import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fushipment/pages/order_page.dart';
import 'package:fushipment/values/app_assets.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/values/app_icons.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> items = [
      {"title": "Đơn hàng", "icon": AppIcons.order, "router": const OrderPage()},
      {"title": "Cửa hàng", "icon": AppIcons.store},
      {"title": "Voucher", "icon": AppIcons.voucher},
      {"title": "Thống kê", "icon": AppIcons.static},
      {"title": "Cài đặt", "icon": AppIcons.setting},
      {"title": "Đăng xuất", "icon": AppIcons.logOut},
    ];
    return Drawer(
      width: MediaQuery.sizeOf(context).width,
      backgroundColor: Colors.white,
      child: Container(
        margin: EdgeInsets.symmetric(
            vertical: MediaQuery.sizeOf(context).height * 0.08),
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 160,
                  height: 160,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        AppAssets.teoEmPath,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 12),
                const Text(
                  "Bánh Mì Tèo Em",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: items.asMap().entries.map((entry) {
                  final index = entry.key;
                  final item = entry.value;
                  final isLastItem =
                      index == items.length - 1; // Check if it's the last item
                  return Container(
                    margin: EdgeInsets.only(top: !isLastItem ? 6 : 12),
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Colors.white,
                        width: 4.0,
                      ),
                      borderRadius:
                          !isLastItem ? BorderRadius.circular(4) : null,
                      boxShadow: !isLastItem
                          ? [
                              const BoxShadow(
                                color: Colors.grey,
                                blurRadius: 1.0,
                                spreadRadius: 0.1,
                              ),
                            ]
                          : null,
                    ),
                    child: InkWell(
                      onTap: () {
                        print(item["router"]);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => item["router"]));
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: SvgPicture.asset(item["icon"],
                              width: 32
                            ),
                          )),
                          SizedBox(
                            width: MediaQuery.sizeOf(context).width * 0.04,
                          ),
                          Expanded(
                              flex: 4,
                              child: Text(
                                item["title"],
                                style: const TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 15),
                              ))
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
