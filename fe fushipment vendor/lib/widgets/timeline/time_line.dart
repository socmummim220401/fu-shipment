import 'package:flutter/material.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:fushipment/widgets/timeline/time_line_item.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TimelineWidget extends StatefulWidget {
  final int? step;

  const TimelineWidget({super.key, required this.step});

  @override
  State<TimelineWidget> createState() => _TimelineWidgetState();
}

class _TimelineWidgetState extends State<TimelineWidget> {
  late int? status;

  @override
  void initState() {
    // TODO: implement initState
    status = widget.step;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.sizeOf(context).width,
      height: 40,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: TimeLineItem(
              isFail: false,
              isFirst: true,
              isLast: false,
              isPast: true,
              axis: TimelineAxis.horizontal,
              widgets: Container(
                  margin: EdgeInsets.only(top: 8),
                  child: Text(
                    "Chờ duyệt",
                    style:
                        TextStyle(fontSize: 12, color: AppColors.primaryColor),
                  )),
            ),
          ),
          Expanded(
            flex: 3,
            child: TimeLineItem(
              isFail: false,
              isFirst: status != 0 ? false : true,
              isLast: status != 0 ? false : true,
              isPast: status != 0 ? true : false,
              axis: TimelineAxis.horizontal,
              widgets: Container(
                  margin: EdgeInsets.only(top: 8),
                  child: Text("Đã duyệt",
                      style: TextStyle(
                          fontSize: 12, color: AppColors.primaryColor))),
            ),
          ),
          Expanded(
            flex: 1,
            child: TimeLineItem(
              isFirst: status == 2 || status == 3 ? false : true,
              isLast: true,
              isPast: status == 2 || status == 3 ? true : false,
              isFail: status == 3 ? true : false,
              axis: TimelineAxis.horizontal,
              widgets: Container(
                  margin: EdgeInsets.only(top: 8),
                  child: Text("Hoàn tất",
                      style: TextStyle(
                          fontSize: 12, color: AppColors.primaryColor))),
            ),
          ),
        ],
      ),
    );
  }
}
