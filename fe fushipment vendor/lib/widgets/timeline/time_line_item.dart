import 'package:flutter/material.dart';
import 'package:fushipment/values/app_colors.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TimeLineItem extends StatefulWidget {
  final bool isFirst;
  final bool isLast;
  final bool isPast;
  final TimelineAxis axis;
  final Widget widgets;
  final bool isFail;

  const TimeLineItem(
      {super.key,
      required this.isFirst,
      required this.isLast,
      required this.axis,
      required this.isPast,
      required this.isFail,
      required this.widgets});

  @override
  State<TimeLineItem> createState() => _TimeLineItemState();
}

class _TimeLineItemState extends State<TimeLineItem> {
  @override
  Widget build(BuildContext context) {
    bool isFirst = widget.isFirst;
    bool isFail = widget.isFail;
    bool isLast = widget.isLast;
    TimelineAxis axis = widget.axis;
    bool isPast = widget.isPast;
    Widget widgets = widget.widgets;

    return TimelineTile(
      axis: axis,
      isFirst: isFirst,
      isLast: isLast,
      beforeLineStyle: LineStyle(
          color: isFail
              ? AppColors.dangerousColor
              : isPast
                  ? AppColors.primaryColor
                  : Colors.blue.shade50),
      indicatorStyle: IndicatorStyle(
          width: 8,
          color: isFail
              ? AppColors.dangerousColor
              : isPast
                  ? AppColors.primaryColor
                  : Colors.blue.shade50,
          height: 8),
      endChild: widgets,
    );
  }
}
