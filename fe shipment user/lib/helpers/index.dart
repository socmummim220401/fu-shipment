import 'dart:convert';
import 'package:fushipment/utils/routes.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';

class Helpers {

  static String formatNumber(num number) {
    // convert number to money form
    NumberFormat formatter = NumberFormat("#,###.### đ", "en_US");
    return formatter.format(number);
  }

  static Future<Response> returnResponse(
      {Map<String, String>? bodies = const {},
      Map<String, String>? headers = const {
        "Content-Type": "application/json",
      },
      required String endpoint,
      required String method}) async {
    String url = "${AppRoutes.endPointBackEnd}$endpoint";
    Uri uriParse = Uri.parse(url);

    late Response response;
    if (method == "post") {
      response =
          await post(uriParse, body: jsonEncode(bodies), headers: headers);
    } else if (method == "get") {
      response = await get(uriParse, headers: headers);
    } else if (method == "put") {
      response = await put(uriParse, headers: headers, body: jsonEncode(bodies));
    } else if (method == "delete") {
      response = await delete(uriParse, body: jsonEncode(bodies), headers: headers);
    }
    return response;
  }

}
