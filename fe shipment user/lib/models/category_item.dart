import 'package:flutter/cupertino.dart';

class CategoryItem {
  int id;
  String title;
  IconData iconData;

  CategoryItem(this.id, this.title, this.iconData);
}