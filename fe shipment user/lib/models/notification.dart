class NotificationConstructor {
  int? id;
  String? image;
  String? title;
  String? publicDate;
  String? detail;

  NotificationConstructor({this.id, this.image, this.title, this.publicDate, this.detail});

  NotificationConstructor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    title = json['title'];
    publicDate = json['publicDate'];
    detail = json['detail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['image'] = image;
    data['title'] = title;
    data['publicDate'] = publicDate;
    data['detail'] = detail;
    return data;
  }
}
