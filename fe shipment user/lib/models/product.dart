class Product {
  int? id;
  String? name;
  String? image;
  int? price;
  String? des;

  Product({this.id, this.name, this.image, this.price, this.des});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    des = json['des'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['des'] = des;
    return data;
  }
}