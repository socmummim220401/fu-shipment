class Voucher {
  int? id;
  String? name;
  String? image;
  double? price;
  String? des;
  double? score;

  Voucher({this.id, this.name, this.image, this.price, this.des, this.score});

  Voucher.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    des = json['des'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['des'] = des;
    data['score'] = score;
    return data;
  }
}
