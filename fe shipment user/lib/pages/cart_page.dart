// import 'dart:convert';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fushipment/core/store.dart';
// import 'package:fushipment/utils/notification_service.dart';
// import 'package:fushipment/widgets/footer_bar_widget.dart';
// import 'package:fushipment/models/catalog.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../widgets/user/cart/cart_items_widget.dart';
// import 'package:velocity_x/velocity_x.dart';
// import 'package:http/http.dart' as http;
//
// class CartPage extends StatefulWidget {
//   const CartPage({super.key});
//
//   @override
//   State<CartPage> createState() => _CartPageState();
// }
//
// class _CartPageState extends State<CartPage> {
//   MyStore? store = VxState.store as MyStore?;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _loadDataCartPage();
//   }
//
//   Future _loadDataCartPage() async {
//     final prefs = await SharedPreferences.getInstance();
//     final dataCart = prefs.getString("cart");
//     var listItems = json.decode(dataCart!);
//     if (listItems.runtimeType == List) {
//       setState(() {
//         store?.cartItems = List.from(listItems)
//             .map<CartItems>((e) => CartItems.fromMap(e))
//             .toList();
//       });
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [RemoveFromCart, UpdateCartItems]);
//     return Scaffold(
//       appBar: AppBar(
//           backgroundColor: Colors.white,
//           elevation: 0.5,
//           title: const Row(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Icon(
//                 Icons.shopping_cart,
//                 color: Colors.black,
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Text(
//                 "Giỏ hàng",
//                 style: TextStyle(color: Colors.black),
//               ),
//             ],
//           ),
//           automaticallyImplyLeading: false
//           // title: Text("Product Detail"),
//           ),
//       body: Column(
//         children: [_CartList(), _CartTotal()],
//       ),
//       bottomNavigationBar: const FooterBarWidget(),
//     );
//   }
// }
//
// class _CartList extends StatefulWidget {
//   @override
//   State<_CartList> createState() => _CartListState();
// }
//
// class _CartListState extends State<_CartList> {
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [RemoveFromCart, UpdateCartItems]);
//     MyStore? store = VxState.store as MyStore?;
//     return ListView.builder(
//         itemCount: store?.cartItems.length,
//         itemBuilder: (context, index) {
//           return CartItemsWidget(
//             item: store!.cartItems[index],
//             cartItems: store!.cartItems,
//             index: index,
//           );
//         }).p24().expand();
//   }
// }
//
// class _CartTotal extends StatefulWidget {
//   @override
//   State<_CartTotal> createState() => _CartTotalState();
// }
//
// class _CartTotalState extends State<_CartTotal> {
//   NotificationService notificationService = NotificationService();
//   String name = "Socquekute";
//
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [RemoveFromCart, UpdateCartItems]);
//     MyStore? store = VxState.store as MyStore?;
//     num totalPrice = store!.cartItems
//         .fold(0, (total, curr) => total + curr.price * curr.quantity);
//
//     return Container(
//       height: 100,
//       color: Colors.white,
//       child: Padding(
//         padding: EdgeInsets.symmetric(horizontal:  store.role == "user" ? 10 : 20),
//         child: Row(
//           mainAxisAlignment:  store.role == "user" ? MainAxisAlignment.spaceAround : MainAxisAlignment.spaceBetween,
//           children: store.role == "user"
//               ? [
//                   VxConsumer(
//                       notifications: {},
//                       builder: (context, store, status) {
//                         return Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           children: [
//                             const Text("Tổng tiền",
//                                 style: TextStyle(
//                                     fontSize: 16,
//                                     fontWeight: FontWeight.w500,
//                                     color: Color(0xFFa9a9a9))),
//                             Text(
//                               "$totalPrice.000 đ",
//                               style: const TextStyle(
//                                   fontSize: 24, fontWeight: FontWeight.w500),
//                             )
//                           ],
//                         );
//                       },
//                       mutations: const {RemoveFromCart, UpdateCartItems}),
//                   30.widthBox,
//                   ElevatedButton(
//                     onPressed: () async {
//                       final prefs = await SharedPreferences.getInstance();
//                       final dataCart = prefs.getString("cart");
//
//                       notificationService.getDeviceToken().then((value) async {
//                         var data = {
//                           'to':
//                               "cYxlOgd_TJ2SG1wCet3zw9:APA91bGgyKQvVNeK9K0HvAChvQk_O1Y17o-9pKVcQVELhnxBEYQ5gAZX1Tb6RhC4PEBwjsx3PX_8-CsMnNa50SAhWFiWd9XO62PXMsjiAU-y8-_jKJHuxGPxPcz9nnuP3pKA9PcXM5_6",
//                           'priority': 'high',
//                           'notification': {
//                             'title': "FuShipment",
//                             'body': 'Bạn có đơn đặt hàng từ ${name}'
//                           },
//                           'data': {'order': dataCart, 'created_by': name}
//                         };
//                         await http.post(
//                             Uri.parse("https://fcm.googleapis.com/fcm/send"),
//                             body: json.encode(data),
//                             headers: {
//                               'Content-Type': 'application/json; charset=UTF-8',
//                               'Authorization':
//                                   'key=AAAAUOCFiWo:APA91bGQHANgygUS3c2dovIdO5-3eehqJU-LPEXEMAXCxdxrwGO_akKfBTYFZNFdIQh7O1KRj_140FEAngAIHTmTQXuKhYQC2lcaVgcWyCshQaPF7L9gCm3buWNHuegB5MhqZoEjDgTD'
//                             });
//                       });
//                     },
//                     style: ButtonStyle(
//                         backgroundColor: MaterialStateProperty.all(
//                             context.theme.primaryColor)),
//                     child: "Thanh toán".text.white.make(),
//                   ).w32(context)
//                 ]
//               : [
//                   Text(
//                     "Tổng tiền",
//                     style: const TextStyle(
//                         fontSize: 28, fontWeight: FontWeight.w500),
//                   ),
//                   Text(
//                     "$totalPrice.000 đ",
//                     style: const TextStyle(
//                         fontSize: 28, fontWeight: FontWeight.w500),
//                   )
//                 ],
//         ),
//       ),
//     );
//   }
// }
