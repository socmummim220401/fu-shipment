import 'package:flutter/material.dart';
import 'package:fushipment/pages/index.dart';
import 'package:fushipment/pages/vendor/order_page2.dart';
import 'package:fushipment/values/app_assets.dart';
import 'package:fushipment/values/app_colors.dart';

class HoLaPage extends StatelessWidget {
  const HoLaPage({super.key}) ;
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 5), () {
      Navigator.push(context, MaterialPageRoute(builder: (context)=> const OrderPage()));
    });
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Center(
        child: (Image.asset(
          AppAssets.hoLaPath,
          width: 200,
        )),
      ),
    );
  }
}
