import 'package:flutter/material.dart';
import 'package:fushipment/pages/user/index.dart';
import 'package:fushipment/utils/routes.dart';
import 'package:fushipment/values/app_assets.dart';

class IndexPage extends StatelessWidget {
  const IndexPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.64,
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
                color: Colors.orangeAccent,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            child: Image.asset(AppAssets.bannerShopping),
          ),
          const SizedBox(
            height: 24,
          ),
          customButton(
              onPressed: () {
                Navigator.pushNamed(context, AppRoutes.loginRoute);
              },
              title: "Đăng nhập",
              backgroundColor: Colors.orangeAccent,
              borderColor: Colors.orangeAccent,
              context: context,
              padding: EdgeInsets.symmetric(
                  vertical: 12,
                  horizontal: MediaQuery.of(context).size.width * 0.36),
              textStyle: const TextStyle(color: Colors.white, fontSize: 16)),
          const SizedBox(
            height: 8,
          ),
          customButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const DefaultPage()));
              },
              padding: EdgeInsets.symmetric(
                  vertical: 12,
                  horizontal: MediaQuery.of(context).size.width * 0.33),
              title: "View as guest",
              textStyle: const TextStyle(color: Colors.orangeAccent, fontSize: 16),
              context: context,
              borderColor: Colors.orangeAccent,
              backgroundColor: Colors.white),
          const SizedBox(
            height: 20,
          ),
          customButton(
              onPressed: () {},
              title: "Tiếng Việt >",
              textStyle:
                  const TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              context: context,
              icon: const Icon(Icons.blur_circular_rounded, color: Colors.red),
              borderColor: const Color(0xffdedede),
              backgroundColor: Colors.white)
        ],
      ),
    );
  }
}

Widget customButton(
    {BuildContext? context,
    required String title,
    Color? backgroundColor,
    Color? borderColor,
    required VoidCallback onPressed,
    EdgeInsetsGeometry? padding,
    TextStyle? textStyle,
    Widget? icon}) {
  return UnconstrainedBox(
      child: icon != null
          ? ElevatedButton.icon(
              icon: icon,
              onPressed: onPressed,
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                  color: borderColor!,
                )),
                padding: MaterialStateProperty.all(padding),
                shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(9999)))),
                backgroundColor: MaterialStateProperty.all(backgroundColor),
              ),
              label: Text(title, style: textStyle))
          : ElevatedButton(
              onPressed: onPressed,
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                  color: borderColor!,
                )),
                padding: MaterialStateProperty.all(padding),
                shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(9999)))),
                backgroundColor: MaterialStateProperty.all(backgroundColor),
              ),
              child: Text(title, style: textStyle))

      // child: Text(title!, style: textStyle)),
      );
}
