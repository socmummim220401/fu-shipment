// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fushipment/core/store.dart';
// import 'package:fushipment/utils/routes.dart';
// import 'package:fushipment/values/app_assets.dart';
// import 'package:fushipment/values/app_icons.dart';
// import 'package:velocity_x/velocity_x.dart';
// import 'package:intl/intl.dart';
//
// class LoginPage extends StatefulWidget {
//   const LoginPage({super.key});
//
//   @override
//   State<LoginPage> createState() => _LoginPageState();
// }
//
// class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
//   MyStore? store = VxState.store as MyStore?;
//   String username = "";
//   String password = "";
//   String phone = "";
//   String email = "";
//   TextEditingController dob = TextEditingController();
//   late TabController _tabController;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     _tabController = TabController(length: 2, vsync: this);
//     super.initState();
//   }
//
//   void SignInAccount() {
//     Map<String, dynamic> _data = {
//       "username": username,
//       "password": password
//     };
//     Navigator.pushNamed(context, AppRoutes.otpRoute);
//     // if (username == "user" && password == "user") {
//     //   store?.role = "user";
//     //   Navigator.pushNamed(context, AppRoutes.homeRoute);
//     // } else if (username == "vendor" && password == "vendor") {
//     //   store?.role = "vendor";
//     //   Navigator.pushNamed(context, AppRoutes.orderRoute);
//     // }
//   }
//   void SignUpAccount() {
//     Map<String, dynamic> _data = {
//       "username": username,
//       "password": password,
//       "dob": dob,
//       "email": email,
//       "phone": phone
//     };
//     Navigator.pushNamed(context, AppRoutes.homeRoute);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       resizeToAvoidBottomInset: false,
//       body: Center(
//         // child: SingleChildScrollView(
//         child: Container(
//           color: Colors.blueAccent,
//           child: Column(
//             children: [
//               Expanded(
//                 flex: 2,
//                 child: Container(
//                   color: Colors.blueAccent,
//                   child: Column(
//                     children: [
//                       Expanded(
//                           flex: 6,
//                           child: Column(
//                             children: [
//                               const SizedBox(
//                                 height: 50,
//                               ),
//                               Image.asset(
//                                 AppAssets.bannerShipper,
//                                 height: 200,
//                                 width: 350,
//                               ),
//                             ],
//                           )),
//                       Expanded(
//                         flex: 1,
//                         child: Align(
//                           alignment: Alignment.centerLeft,
//                           child: TabBar(
//                             isScrollable: true,
//                             labelPadding: const EdgeInsets.symmetric(horizontal: 20),
//                             controller: _tabController,
//                             labelStyle: const TextStyle(
//                               fontSize: 16.0,
//                             ),
//                             indicatorColor: Colors.white,
//                             indicatorPadding: const EdgeInsets.only(
//                                 bottom: 10, left: 15, right: 15),
//                             //For Selected tab
//                             unselectedLabelStyle: const TextStyle(
//                               fontSize: 16,
//                             ),
//                             tabs: const [Tab(text: "Sign In"), Tab(text: "Sign Up")],
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//               Expanded(
//                 flex: 3,
//                 child: TabBarView(controller: _tabController, children: [
//                   Container(
//                     decoration: const BoxDecoration(
//                         color: Colors.white,
//                         borderRadius: BorderRadius.only(
//                             topLeft: Radius.circular(40.0),
//                             topRight: Radius.circular(40.0))),
//                     child: (Column(
//                       children: [
//                         const SizedBox(
//                           height: 32.0,
//                         ),
//                         Text(
//                           "Welcome to FuShipment ! $username",
//                           style: const TextStyle(
//                             fontSize: 24,
//                             fontWeight: FontWeight.bold,
//                           ),
//                         ),
//                         const SizedBox(
//                           height: 12.0,
//                         ),
//                         const Text(
//                           "Please enter the detail below to continue",
//                           style: TextStyle(
//                               fontSize: 14,
//                               color: Color(0xFFc6ccd2),
//                               fontWeight: FontWeight.bold),
//                         ),
//                         SizedBox(
//                           height: 12.0,
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(
//                               vertical: 16.0, horizontal: 32.0),
//                           child: Column(
//                             children: [
//                               TextFormField(
//                                 decoration: InputDecoration(
//                                     border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(20.0),
//                                       borderSide: const BorderSide(
//                                         width: 0,
//                                         style: BorderStyle.none,
//                                       ),
//                                     ),
//                                     prefixIcon: Icon(Icons.email_rounded),
//                                     hintText: "Enter username",
//                                     filled: true,
//                                     fillColor: Color(0xFFf4f6f8)),
//                                 validator: (value) {
//                                   if (value!.isEmpty) {
//                                     return "Username cannot be empty";
//                                   }
//
//                                   return null;
//                                 },
//                                 onChanged: (value) {
//                                   username = value;
//                                   setState(() {});
//                                 },
//                               ),
//                               const SizedBox(
//                                 height: 16,
//                               ),
//                               TextFormField(
//                                 obscureText: true,
//                                 decoration: InputDecoration(
//                                     border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(20.0),
//                                       borderSide: const BorderSide(
//                                         width: 0,
//                                         style: BorderStyle.none,
//                                       ),
//                                     ),
//                                     prefixIcon: Icon(Icons.lock),
//                                     hintText: "Enter password",
//                                     filled: true,
//                                     fillColor: Color(0xFFf4f6f8)),
//                                 validator: (value) {
//                                   if (value!.isEmpty) {
//                                     return "Password cannot be empty";
//                                   } else if (value.length < 6) {
//                                     return "Password length should be atleast 6";
//                                   }
//                                   return null;
//                                 },
//                                 onChanged: (value) {
//                                   password = value;
//                                 },
//                               ),
//                               Row(
//                                 children: [
//                                   const Expanded(
//                                     flex: 3,
//                                     child: SizedBox(),
//                                   ),
//                                   const SizedBox(
//                                     width: 6,
//                                   ),
//                                   Expanded(
//                                     flex: 2,
//                                     child: TextButton(
//                                         onPressed: () {},
//                                         child: const Text("Forgot password?")),
//                                   )
//                                 ],
//                               ),
//                               SizedBox(
//                                 height: 44,
//                                 width: double.infinity,
//                                 child: ElevatedButton(
//                                     style: ElevatedButton.styleFrom(
//                                         shape: RoundedRectangleBorder(
//                                             borderRadius:
//                                                 BorderRadius.circular(12))),
//                                     onPressed: () =>
//                                         SignInAccount(),
//                                     child: (const Text(
//                                       "Sign In",
//                                       style: TextStyle(fontSize: 16),
//                                     ))),
//                               ),
//                               const SizedBox(
//                                 height: 16,
//                               ),
//                               Row(
//                                 mainAxisAlignment:
//                                     MainAxisAlignment.spaceEvenly,
//                                 children: [
//                                   InkWell(
//                                     child: Container(
//                                       height: 52,
//                                       width: 52,
//                                       decoration: BoxDecoration(
//                                         border: Border.all(
//                                             width: 1, color: const Color(0xFFeff4fc)),
//                                         borderRadius: BorderRadius.circular(
//                                             100), //<-- SEE HERE
//                                       ),
//                                       child: const CircleAvatar(
//                                         radius: 32, // Image radius
//                                         backgroundImage:
//                                             AssetImage(AppIcons.googleIcon),
//                                       ),
//                                     ),
//                                     onTap: () {
//                                       Navigator.pushNamed(context,
//                                           AppRoutes.webViewGoogleRoute);
//                                     },
//                                   ),
//                                   InkWell(
//                                     child: Container(
//                                       height: 52,
//                                       width: 52,
//                                       decoration: BoxDecoration(
//                                         border: Border.all(
//                                             width: 1, color: const Color(0xFFeff4fc)),
//                                         borderRadius: BorderRadius.circular(
//                                             100), //<-- SEE HERE
//                                       ),
//                                       child: const CircleAvatar(
//                                         radius: 32, // Image radius
//                                         backgroundImage:
//                                             AssetImage(AppIcons.facebookIcon),
//                                       ),
//                                     ),
//                                     onTap: () {
//                                       Navigator.pushNamed(context,
//                                           AppRoutes.webViewFacebookRoute);
//                                     },
//                                   ),
//                                 ],
//                               ),
//                               // SizedBox(
//                               //   height: 16,
//                               // ),
//                               Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   const Text("Don't have an account?"),
//                                   TextButton(
//                                       onPressed: () {},
//                                       child: const Text("Register Now"))
//                                 ],
//                               )
//                             ],
//                           ),
//                         )
//                       ],
//                     )),
//                   ),
//                   Container(
//                       decoration: const BoxDecoration(
//                           color: Colors.white,
//                           borderRadius: BorderRadius.only(
//                               topLeft: Radius.circular(40.0),
//                               topRight: Radius.circular(40.0))),
//                       child: (Padding(
//                         padding: const EdgeInsets.symmetric(
//                             vertical: 16.0, horizontal: 32.0),
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           children: [
//                             const SizedBox(
//                               height: 12.0,
//                             ),
//                             const Text(
//                               "Sign Up Account !",
//                               style: TextStyle(
//                                 fontSize: 24,
//                                 fontWeight: FontWeight.bold,
//                               ),
//                             ),
//                             const SizedBox(
//                               height: 12.0,
//                             ),
//                             const Text(
//                               "Please enter the detail below to continue",
//                               style: TextStyle(
//                                   fontSize: 14,
//                                   color: Color(0xFFc6ccd2),
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             const SizedBox(
//                               height: 15,
//                             ),
//                             TextFormField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(20.0),
//                                     borderSide: const BorderSide(
//                                       width: 0,
//                                       style: BorderStyle.none,
//                                     ),
//                                   ),
//                                   prefixIcon: Icon(Icons.email_rounded),
//                                   hintText: "Enter username",
//                                   filled: true,
//                                   fillColor: const Color(0xFFf4f6f8)),
//                               validator: (value) {
//                                 if (value!.isEmpty) {
//                                   return "Username cannot be empty";
//                                 }
//
//                                 return null;
//                               },
//                               onChanged: (value) {
//                                 username = value;
//                                 setState(() {});
//                               },
//                             ),
//                             const SizedBox(
//                               height: 16,
//                             ),
//                             TextFormField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(20.0),
//                                     borderSide: const BorderSide(
//                                       width: 0,
//                                       style: BorderStyle.none,
//                                     ),
//                                   ),
//                                   prefixIcon: Icon(Icons.phone_android),
//                                   hintText: "Enter phone",
//                                   filled: true,
//                                   fillColor: Color(0xFFf4f6f8)),
//                               validator: (value) {
//                                 if (value!.isEmpty) {
//                                   return "Phone cannot be empty";
//                                 }
//
//                                 return null;
//                               },
//                               onChanged: (value) {
//                                 phone = value;
//                                 setState(() {});
//                               },
//                             ),
//                             SizedBox(
//                               height: 16,
//                             ),
//                             TextField(
//                               controller: dob,
//                               readOnly: true,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(20.0),
//                                     borderSide: const BorderSide(
//                                       width: 0,
//                                       style: BorderStyle.none,
//                                     ),
//                                   ),
//                                   prefixIcon: Icon(Icons.date_range),
//                                   hintText: "Enter dob",
//                                   filled: true,
//                                   fillColor: Color(0xFFf4f6f8)),
//                               onTap: () async {
//                                 DateTime? pickedDate = await showDatePicker(
//                                     context: context,
//                                     initialDate: DateTime.now(),
//                                     firstDate: DateTime(2000),
//                                     //DateTime.now() - not to allow to choose before today.
//                                     lastDate: DateTime(2101));
//
//                                 if (pickedDate != null) {
//                                   print(
//                                       pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
//                                   String formattedDate =
//                                       DateFormat('yyyy-MM-dd')
//                                           .format(pickedDate);
//                                   print(
//                                       formattedDate); //formatted date output using intl package =>  2021-03-16
//                                   //you can implement different kind of Date Format here according to your requirement
//
//                                   setState(() {
//                                     dob.text = formattedDate;//set output date to TextField value.
//                                   });
//                                 } else {
//                                   print("Date is not selected");
//                                 }
//                               },
//
//                             ),
//                             const SizedBox(
//                               height: 16,
//                             ),
//                             TextFormField(
//                               obscureText: true,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(20.0),
//                                     borderSide: const BorderSide(
//                                       width: 0,
//                                       style: BorderStyle.none,
//                                     ),
//                                   ),
//                                   prefixIcon: Icon(Icons.lock),
//                                   hintText: "Enter password",
//                                   filled: true,
//                                   fillColor: Color(0xFFf4f6f8)),
//                               validator: (value) {
//                                 if (value!.isEmpty) {
//                                   return "Password cannot be empty";
//                                 } else if (value.length < 6) {
//                                   return "Password length should be atleast 6";
//                                 }
//                                 return null;
//                               },
//                               onChanged: (value) {
//                                 password = value;
//                               },
//                             ),
//                             const SizedBox(
//                               height: 16,
//                             ),
//                             SizedBox(
//                               height: 44,
//                               width: double.infinity,
//                               child: ElevatedButton(
//                                   style: ElevatedButton.styleFrom(
//                                       shape: RoundedRectangleBorder(
//                                           borderRadius:
//                                               BorderRadius.circular(12))),
//                                   onPressed: () =>
//                                       SignUpAccount(),
//                                   child: (const Text(
//                                     "Sign Up",
//                                     style: TextStyle(fontSize: 16),
//                                   ))),
//                             ),
//                           ],
//                         ),
//                       )))
//                 ]),
//               )
//             ],
//           ),
//         ),
//         // ),
//       ),
//     );
//   }
// }
