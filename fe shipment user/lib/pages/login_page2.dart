import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/pages/otp_page.dart';
import 'package:fushipment/values/app_styles.dart';
import 'package:http/http.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool? checked = true;
  String dropDownValue = "+84";
  late TextEditingController _controller;

  Future<void> loginAccount() async {
    String phone = '$dropDownValue${_controller.text}';
    //test +84971858758
    Map<String, String> bodies = {
      "phoneNumber": phone,
    };
    try {
      Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => OTPPage(
                              phone: phone,
                            )));

      // Response response = await Helpers.returnResponse(
      //     endpoint: "/login", method: "post", bodies: bodies);
      // if (response.body.isNotEmpty) {
      //   var responseBody = jsonDecode(response.body);
      //   if (responseBody != null && responseBody["status"] == "200") {
      //     if (context.mounted) {
      //       Navigator.push(
      //           context,
      //           MaterialPageRoute(
      //               builder: (context) => OTPPage(
      //                     phone: phone,
      //                   )));
      //     }
      //   } else {
      //     if (context.mounted) {
      //       final scaffold = ScaffoldMessenger.of(context);
      //       scaffold.showSnackBar(SnackBar(
      //         content: Text(responseBody["message"]),
      //         action: SnackBarAction(
      //           label: 'Cancel',
      //           onPressed: scaffold.hideCurrentSnackBar,
      //         ),
      //       ));
      //     }
      //   }
        // } else {
        //   if (context.mounted) {
        //     final scaffold = ScaffoldMessenger.of(context);
        //     scaffold.showSnackBar(SnackBar(
        //       content: const Text("Server Busy"),
        //       action: SnackBarAction(
        //         label: 'Cancel',
        //         onPressed: scaffold.hideCurrentSnackBar,
        //       ),
        //     ));
        //   }
        // }
    } catch (_) {
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = TextEditingController();
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.4,
                  child: Image.network(
                      "https://media.istockphoto.com/id/1202073158/vector/online-shopping-concept.jpg?s=612x612&w=0&k=20&c=6V3ncvtoNhv3klHoMJVTd-Dp_fN4BaUoLulzv75juX0=",
                      fit: BoxFit.fill),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Đăng nhập / Đăng ký",
                        style: AppStyles.h5.copyWith(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      const Text("Nhập số điện thoại để tiếp tục"),
                      const SizedBox(
                        height: 12,
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.orangeAccent),
                            borderRadius: BorderRadius.circular(9999)),
                        child: Row(
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.16,
                              child: DropdownButtonFormField(
                                  decoration: const InputDecoration(
                                    border: InputBorder.none, // Hide the border
                                  ),
                                  value: dropDownValue,
                                  items: <String>["+84"]
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (String? value) {
                                    setState(() {
                                      dropDownValue = value!;
                                    });
                                  }),
                            ),
                            Expanded(
                              child: TextField(
                                controller: _controller,
                                onChanged: (value) {
                                  setState(() {
                                    _controller.text = value;
                                    _controller.selection =
                                        TextSelection.collapsed(
                                            offset: _controller.text.length);
                                  });
                                },
                                decoration: const InputDecoration(
                                  hintText: 'Enter text',
                                  border: InputBorder.none, // Hide the border
                                  contentPadding: EdgeInsets.all(
                                      12), // Adjust padding as needed
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      _controller.text.isEmpty
                          ? customButton(
                              title: "Tiếp tục",
                              backgroundColor: const Color(0xffcccccc),
                              borderColor: const Color(0xffcccccc),
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.38,
                                  vertical: 8),
                              context: context,
                              textStyle: const TextStyle(
                                  color: Colors.white, fontSize: 16),
                              onPressed: () {
                                loginAccount();
                              })
                          : customButton(
                              title: "Tiếp tục",
                              backgroundColor: Colors.orangeAccent,
                              borderColor: Colors.orangeAccent,
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.38,
                                  vertical: 8),
                              context: context,
                              textStyle: const TextStyle(
                                  color: Colors.white, fontSize: 16),
                              onPressed: () {
                                loginAccount();
                              }),
                      const SizedBox(
                        height: 12,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Row(
            children: [
              Checkbox(
                  value: checked,
                  activeColor: Colors.blue,
                  onChanged: (value) {
                    setState(() {
                      checked = value;
                    });
                  }),
              Expanded(
                child: RichText(
                  text: const TextSpan(
                    text: 'Bằng việc sử dụng FuShipment, bạn đồng ý với ',
                    style: TextStyle(fontSize: 12, color: Colors.black),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Điều khoản sử dụng',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueAccent,
                              decoration: TextDecoration.underline)),
                      TextSpan(text: ' và '),
                      TextSpan(
                          text: 'Chính sách riêng tư',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueAccent,
                              decoration: TextDecoration.underline)),
                      TextSpan(text: ' của FuShipment'),
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

Widget customButton(
    {BuildContext? context,
    required String title,
    Color? backgroundColor,
    Color? borderColor,
    required VoidCallback onPressed,
    EdgeInsetsGeometry? padding,
    TextStyle? textStyle,
    Widget? icon}) {
  return UnconstrainedBox(
      child: icon != null
          ? ElevatedButton.icon(
              icon: icon,
              onPressed: onPressed,
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                  color: borderColor!,
                )),
                padding: MaterialStateProperty.all(padding),
                shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(9999)))),
                backgroundColor: MaterialStateProperty.all(backgroundColor),
              ),
              label: Text(title, style: textStyle))
          : ElevatedButton(
              onPressed: onPressed,
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                  color: borderColor!,
                )),
                padding: MaterialStateProperty.all(padding),
                shape: MaterialStateProperty.all(const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(9999)))),
                backgroundColor: MaterialStateProperty.all(backgroundColor),
              ),
              child: Text(title, style: textStyle))

      // child: Text(title!, style: textStyle)),
      );
}
