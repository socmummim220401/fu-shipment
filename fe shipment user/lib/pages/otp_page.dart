import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/pages/user/index.dart';
import 'package:http/http.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pinput/pinput.dart';

class OTPPage extends StatefulWidget {
  final String? phone;
  const OTPPage({super.key, required this.phone});

  @override
  State<OTPPage> createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> {
  int _time = 60;
  late Timer _timer;
  String otpCode = "";

  @override
  void initState() {
    super.initState();
    // Start the countdown timer
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_time > 0) {
        setState(() {
          _time--;
        });
      } else {
        _timer.cancel(); // Stop the timer when it reaches 0
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel(); // Cancel the timer to avoid memory leaks
    super.dispose();
  }

  final defaultPinTheme = PinTheme(
    width: 56,
    height: 60,
    textStyle: const TextStyle(
      fontSize: 22,
      color: Colors.black,
    ),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(8),
      border: Border.all(color: Colors.orangeAccent),
    ),
  );

  Future<void> resendOTP() async {
    String phone = widget.phone ?? "";
    //test +84971858758
    Map<String, String> bodies = {
      "phoneNumber": phone,
    };
    try {
      Response response = await Helpers.returnResponse(
          endpoint: "/login", method: "post", bodies: bodies);
      if (response.body.isNotEmpty) {
        var responseBody = jsonDecode(response.body);
        if (responseBody != null && responseBody["status"] == "200") {
          if (context.mounted) {
            final scaffold = ScaffoldMessenger.of(context);
            scaffold.showSnackBar(SnackBar(
              content: const Text("Send OTP success"),
              action: SnackBarAction(
                label: 'Cancel',
                onPressed: scaffold.hideCurrentSnackBar,
              ),
            ));
            setState(() {
              _time = 60;
              _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
                if (_time > 0) {
                  setState(() {
                    _time--;
                  });
                } else {
                  _timer.cancel();
                }
              });
            });
          }
        } else {
          if (context.mounted) {
            final scaffold = ScaffoldMessenger.of(context);
            scaffold.showSnackBar(SnackBar(
              content: Text(responseBody["message"]),
              action: SnackBarAction(
                label: 'Cancel',
                onPressed: scaffold.hideCurrentSnackBar,
              ),
            ));
          }
        }
      } else {
        if (context.mounted) {
          final scaffold = ScaffoldMessenger.of(context);
          scaffold.showSnackBar(SnackBar(
            content: const Text("Server Busy"),
            action: SnackBarAction(
              label: 'Cancel',
              onPressed: scaffold.hideCurrentSnackBar,
            ),
          ));
        }
      }
    } catch (_) {
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }

  Future<void> authenticationOTPCode() async {
    Map<String, String> bodies = {
      "phoneNumber": widget.phone ?? "",
      "smsOtp": otpCode
    };
    try {
      Response response = await Helpers.returnResponse(
          endpoint: "/confirm_token_sms", method: "post", bodies: bodies);
      var responseBody = jsonDecode(response.body);
      // print(responseBody);
      if (responseBody != null && responseBody["status"] == "200") {
        if (context.mounted) {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const DefaultPage()));
        }
      } else {
        if (context.mounted) {
          final scaffold = ScaffoldMessenger.of(context);
          scaffold.showSnackBar(SnackBar(
            content: Text(responseBody["message"]),
            action: SnackBarAction(
              label: 'Cancel',
              onPressed: scaffold.hideCurrentSnackBar,
            ),
          ));
        }
      }
    } catch (_) {
      if (context.mounted) {
        final scaffold = ScaffoldMessenger.of(context);
        scaffold.showSnackBar(SnackBar(
          content: const Text("Network Error"),
          action: SnackBarAction(
            label: 'Cancel',
            onPressed: scaffold.hideCurrentSnackBar,
          ),
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(12),
        color: Colors.white,
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.arrow_back_ios))
            ]),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.network(
                    "https://t3.ftcdn.net/jpg/05/22/42/06/360_F_522420661_5kiI2AiF3pOCgUwl4hveh88dAZDaD9Ol.jpg"),
                const Text(
                  "Xác thực số điện thoại",
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 16,
                ),
                Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width * 0.76,
                  child: Text(
                      "Vui lòng nhập OTP đã được gửi đến SMS đến số điện thoại ${widget.phone!.substring(0, 6)}*****",
                      textAlign: TextAlign.center,
                      textScaleFactor: 1),
                ),
                const SizedBox(
                  height: 16,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      LineIcons.clock,
                      color: Colors.red,
                      size: 24,
                    ),
                    Text(
                      "Hết hạn trong ${_time}s",
                      style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.red),
                    )
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Pinput(
                  length: 6,
                  defaultPinTheme: defaultPinTheme,
                  focusedPinTheme: defaultPinTheme.copyWith(
                    decoration: defaultPinTheme.decoration!.copyWith(
                      border: Border.all(color: Colors.orangeAccent),
                    ),
                  ),
                  onCompleted: (pin) {
                    setState(() {
                      otpCode = pin;
                    });
                  },
                ),
                const SizedBox(
                  height: 24,
                ),
                ElevatedButton(
                    onPressed: () async {
                      if (_time == 0) {
                        resendOTP();
                      } else if (otpCode.length == 6 && _time != 0) {
                        authenticationOTPCode();
                      }
                    },
                    style: ButtonStyle(
                      side: MaterialStateProperty.all(const BorderSide(
                        color: Colors.orangeAccent,
                      )),
                      padding: MaterialStateProperty.all(EdgeInsets.symmetric(
                          vertical: 12,
                          horizontal:
                              MediaQuery.of(context).size.width * 0.37)),
                      shape: MaterialStateProperty.all(
                          const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(9999)))),
                      backgroundColor: MaterialStateProperty.all(
                          otpCode.length == 6 || _time == 0
                              ? Colors.orangeAccent
                              : Colors.white),
                    ),
                    child: Text(_time == 0 ? "Gửi lại OTP" : "Tiếp tục",
                        style: TextStyle(
                            color: otpCode.length == 6 || _time == 0
                                ? Colors.white
                                : Colors.orangeAccent)))
              ],
            )
          ],
        ),
      ),
    );
  }
}
