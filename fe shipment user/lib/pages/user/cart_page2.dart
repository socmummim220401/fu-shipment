import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class CartPage2 extends StatefulWidget {
  const CartPage2({super.key});

  @override
  State<CartPage2> createState() => _CartPage2State();
}

class _CartPage2State extends State<CartPage2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.orangeAccent,
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text(
            "Giỏ hàng",
            style: TextStyle(color: Colors.black),
          )),
      body: Column(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(12),
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                    child: const Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Icon(LineIcons.shoppingCart, size: 40),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Expanded(
                          flex: 8,
                          child: Text(
                            "Nhận tại quầy",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Icon(LineIcons.angleRight, size: 20))
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    padding: const EdgeInsets.only(
                        left: 12, right: 12, bottom: 12, top: 8),
                    child: Column(children: [
                      Container(
                        margin: const EdgeInsets.only(top: 8, bottom: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Expanded(
                              flex: 1,
                              child: Text(
                                "2x",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Expanded(
                                flex: 8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    const Text(
                                      "Kem Vani",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    const SizedBox(
                                      height: 12,
                                    ),
                                    Row(
                                      children: [
                                        InkWell(
                                          onTap: null,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 4, horizontal: 8),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(9999),
                                                border: Border.all(
                                                    color: const Color(0xffcccccc)),
                                                color: Colors.white),
                                            child: const Row(
                                              children: [
                                                Icon(
                                                  Icons.edit,
                                                  color: Colors.orangeAccent,
                                                  size: 14,
                                                ),
                                                SizedBox(
                                                  width: 2,
                                                ),
                                                Text("Thay đổi",
                                                    style:
                                                        TextStyle(fontSize: 12))
                                              ],
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        InkWell(
                                          onTap: null,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 4, horizontal: 8),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(9999),
                                                border: Border.all(
                                                    color: const Color(0xffcccccc)),
                                                color: Colors.white),
                                            child: const Row(
                                              children: [
                                                Icon(
                                                  Icons.delete_forever,
                                                  color: Colors.orangeAccent,
                                                  size: 14,
                                                ),
                                                SizedBox(
                                                  width: 2,
                                                ),
                                                Text(
                                                  "Xóa",
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            const Expanded(
                              flex: 2,
                              child: Text(
                                "44.000đ",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        margin: const EdgeInsets.only(top: 12, bottom: 8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: const Color(0xffFEF7ED)),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Bạn có muốn thêm món",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.w500),
                            ),
                            Icon(
                              Icons.add,
                              color: Colors.red,
                            )
                          ],
                        ),
                      ),
                      const Divider(
                        color: Color(0xffdedede),
                        thickness: 1,
                      ),
                      TextField(
                        style: const TextStyle(fontSize: 14),
                        // controller: descText,
                        onChanged: (text) {
                          setState(() {
                            // descText.text = text;
                            // descText.selection = TextSelection.collapsed(offset: descText.text.length);
                          });
                        },
                        decoration: const InputDecoration(
                            // border: InputBorder.none,
                            hintText: "Thêm lưu ý cho quán",
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(bottom: 8.0),
                              child: Icon(Icons.add_card,
                                  color: Color(0xFF222222)),
                            )),
                      ),
                      Container(
                        padding:
                            const EdgeInsets.symmetric(vertical: 12, horizontal: 4),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Thành tiền (1 phần)"),
                            Text(
                              "266.000đ",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      const Divider(
                        color: Color(0xffdedede),
                        thickness: 1,
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        margin: const EdgeInsets.only(top: 4, bottom: 4),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: const Color(0xffF8FFEF)),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Thêm mã khuyến mãi",
                              style: TextStyle(
                                  color: Color(0xff58ca30),
                                  fontWeight: FontWeight.w500),
                            ),
                            Icon(
                              Icons.add,
                              color: Color(0xff58ca30),
                            )
                          ],
                        ),
                      ),
                      const Divider(
                        color: Color(0xffdedede),
                        thickness: 1,
                      ),
                      Container(
                        padding:
                            const EdgeInsets.symmetric(vertical: 12, horizontal: 4),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Tổng thanh toán",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500)),
                            Text(
                              "266.000đ",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.red),
                            ),
                          ],
                        ),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          ),

          //navigator
          Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: [
                ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all(EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.28,
                          vertical: 12)),
                      backgroundColor:
                          MaterialStateProperty.all(Colors.orangeAccent),
                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(9999)))),
                  child: const Text(
                    "Tiếp tục thanh toán",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        fontSize: 16),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}