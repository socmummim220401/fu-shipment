import 'package:flutter/material.dart';
import 'package:fushipment/models/history.dart';
import 'package:fushipment/widgets/user/history/history_body.dart';
import 'package:fushipment/widgets/user/history/history_header.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({super.key});

  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  List<History> histories = [
    History(
        id: 1,
        name: "ẨM THỰC QUÊ NHÀ",
        datePayment: "30/06/2023 - 17:33",
        price: 10000,
        status: 1,
        type: 1),
    History(
        id: 4,
        name: "ẨM THỰC QUÊ NHÀ",
        datePayment: "30/06/2023 - 17:33",
        price: 10000,
        status: 1,
        type: 1),
    History(
        id: 2,
        name: "Voucher CGV",
        datePayment: "30/07/2023 - 17:33",
        price: 10000,
        status: 1,
        type: 0),
    History(
        id: 5,
        name: "ẨM THỰC QUÊ NHÀ",
        datePayment: "30/06/2023 - 17:33",
        price: 10000,
        status: 1,
        type: 1),
    History(
        id: 3,
        name: "Voucher CGV",
        datePayment: "31/07/2023 - 17:33",
        price: 10000,
        status: 1,
        type: 0)
  ];
  final TextEditingController rangeController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff2f2f2),
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        title: const Text(
          "Lịch sử giao dịch",
          style: TextStyle(color: Colors.black, letterSpacing: 1),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          HistoryHeader(rangeController: rangeController),
          HistoryBody(histories: histories)
        ],
      ),
    );
  }
}
