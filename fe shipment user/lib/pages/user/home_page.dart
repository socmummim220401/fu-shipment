// import 'package:flutter/material.dart';
// import '../../widgets/user/home/app_bar_widget.dart';
// import '../../widgets/user/home/categories_widget.dart';
// import '../../widgets/footer_bar_widget.dart';
// import '../../widgets/user/home/product_backup_widget.dart';
// import '../../widgets/user/home/search_bar_widget.dart';
//
// class HomePage extends StatefulWidget {
//   const HomePage({super.key});
//
//   @override
//   State<HomePage> createState() => _HomePageState();
// }
//
// class _HomePageState extends State<HomePage> {
//   @override
//   Widget build(BuildContext context) {
//
//     return Scaffold(
//       backgroundColor: const Color(0xFFFFFFFF),
//       body: ListView(
//         children: [
//           const AppBarWidget(),
//           const SearchBarWidget(),
//           const Padding(
//             padding: EdgeInsets.only(top: 20, left: 10),
//             child: Text(
//               "Categories",
//               style: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 fontSize: 20,
//               ),
//             ),
//           ),
//           CategoriesWidget(),
//           const Padding(
//             padding: EdgeInsets.only(top: 20, left: 10),
//             child: Text(
//               "Drink",
//               style: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 fontSize: 20,
//               ),
//             ),
//           ),
//           // ProductWidget(),
//           const ProductBackupWidget(),
//           const ProductBackupWidget(),
//           const ProductBackupWidget(),
//         ],
//       ) ,
//       bottomNavigationBar: const FooterBarWidget(),
//     );
//   }
// }
