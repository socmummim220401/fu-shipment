import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fushipment/models/banner_collection.dart';
import 'package:fushipment/models/user_post.dart';
import 'package:fushipment/values/app_data_default.dart';
import 'package:fushipment/widgets/user/home/all_category_widget/index.dart';
import 'package:fushipment/widgets/user/home/list_user_post_widget/index.dart';
import 'package:fushipment/widgets/user/home/main_categories_widget/index.dart';
import 'package:fushipment/widgets/user/home/nav_bar_widget/index.dart';
import 'package:fushipment/widgets/user/layout/slide_widget/index.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<UserPost> listUserPost = [];

  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    var client = http.Client();
    const String url = "https://api.jsonbin.io/v3/b/65198ebb54105e766fbc4d7a";
    Map<String, String> headers = {
      "X-Master-Key":
          r"$2a$10$d4IOXNrCViVNT9ItoAR1H.RA1IhZgHMG.xsO8xDdKig8Bg0aee6Oe",
      "X-Access-Key":
          r"$2a$10$stSl/9JSGzsZWReAE/BAy.lbeN8r36deMnAfejjjgHj2JFBd4DQ3a"
    };
    var response = await client.get(Uri.parse(url), headers: headers);
    final decodeData = jsonDecode(response.body);
    var userPostData = decodeData["record"]["UserPost"];
    List<UserPost> list = List.from(userPostData)
        .map<UserPost>((item) => UserPost.fromJson(item))
        .toList();
    listUserPost = list;
    setState(() {});
  }

  List<BannerCollection> bannerCollection = [
    BannerCollection(1,
        "https://gongcha.com.vn/wp-content/uploads/2023/07/nha%CC%83n.799x500-01-scaled.jpg"),
    BannerCollection(
      2,
      "https://danviet.mediacdn.vn/296231569849192448/2023/3/21/anh-chup-man-hinh-2023-03-21-luc-181359-16793972531391854030969.png",
    ),
    BannerCollection(3,
        "https://cdn.vietnambiz.vn/2019/9/5/571769907380898232555215921961732487418836n-1567666923764485576938.jpg"),
    BannerCollection(4,
        "https://res.cloudinary.com/practicaldev/image/fetch/s---zAYcBhD--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/i/w605r3q5eduhw5tqly08.png"),
    BannerCollection(5,
        "https://mastigado.files.wordpress.com/2020/12/banner_flutter_dark.jpg"),
  ];

  @override
  Widget build(BuildContext context) {
    return
      // backgroundColor: Colors.yellow,
       ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          const NavBarWidget(),
          MainCategoryWidget(categoryData: AppDataDefault.mainCategoryData),
          AllCategoryWidget(categoryData: AppDataDefault.allCategoryData),
          SlideWidget(
            bannerCollection: bannerCollection,
            border: 8,
            width: MediaQuery.of(context).size.width * 0.94,
            height: 140,
            topPosition: 8,
            leftPosition: 8,
            paddingParent: 12,
            actionButton: false,
          ),
          ListUserPostWidget(
            listUserPost: listUserPost,
          )
        ],
      // ),
    );
  }
}
