import 'package:flutter/material.dart';
import 'package:fushipment/pages/user/history_page.dart';
import 'package:fushipment/pages/user/home_page2.dart';
import 'package:fushipment/widgets/user/bottom_navigator_widget.dart';

import 'my_voucher_page.dart';

class DefaultPage extends StatefulWidget {
  const DefaultPage({super.key});

  @override
  State<DefaultPage> createState() => _DefaultPageState();
}

class _DefaultPageState extends State<DefaultPage> {
  static List<Widget> _pages = [];
  int selectedIndex = 0;

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pages = <Widget>[
      const HomePage(),
      const HistoryPage(),
      const MyVoucherPage(),
      const Text('My account'),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      bottomNavigationBar: BottomNavigationUser(
          selectedIndex: selectedIndex,
          onPress: (index) {
            onItemTapped(index);
          }
      ),
      body: _pages[selectedIndex],
    );
  }
}
