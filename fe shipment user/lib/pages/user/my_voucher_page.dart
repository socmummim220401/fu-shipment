import 'package:flutter/material.dart';
import 'package:fushipment/models/my_voucher.dart';
import 'package:fushipment/widgets/user/my_voucher/voucher_item.dart';

class MyVoucherPage extends StatefulWidget {
  const MyVoucherPage({super.key});

  @override
  State<MyVoucherPage> createState() => _MyVoucherPageState();
}

class _MyVoucherPageState extends State<MyVoucherPage>
    with TickerProviderStateMixin {
  late TabController _tabController;

  List<MyVoucher>? vouchers;

  @override
  void initState() {
    // TODO: implement initState
    _tabController = TabController(length: 2, vsync: this);
    
    vouchers = [
      MyVoucher(
          id: 1,
          title: "Giảm 9K cho đơn từ 90K khi đặt hàng online",
          image:
              "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
          expireDate: "Hết hạn: 30/11/2023",
          quantity: 8),
      MyVoucher(
          id: 1,
          title: "Giảm 9K cho đơn từ 90K khi đặt hàng online",
          image:
              "https://kenh14cdn.com/2018/4/25/3b-mango-gong-cha-1524592487686108361458.jpg",
          expireDate: "Hết hạn: 30/11/2023",
          quantity: 8),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff2f2f2),
      body: Stack(
        children: [
          Container(
            color: const Color(0xfff2f2f2),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.sizeOf(context).width,
            color: Colors.orangeAccent,
            height: 100,
            child: const Text(
              "Voucher của tôi",
              style: TextStyle(
                  color: Colors.black,
                  letterSpacing: 1,
                  fontSize: 20,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Positioned(
            top: 80,
            left: MediaQuery.of(context).size.width * 0.02,
            child: Card(
              // color: Colors.black,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Align(
                alignment: Alignment.center,
                child: TabBar(
                  isScrollable: true,
                  labelPadding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.11,
                      vertical: 4),
                  controller: _tabController,
                  labelStyle: const TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.w600),
                  indicatorColor: Colors.orangeAccent,
                  indicatorPadding:
                      const EdgeInsets.only(top: 28, left: 15, right: 15),
                  //For Selected tab
                  unselectedLabelStyle: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.w600),
                  labelColor: Colors.black,
                  tabs: const [
                    Tab(text: "Còn hiệu lực"),
                    Tab(text: "Hết hiệu lực")
                  ],
                ),
              ),
            ),
          ),
          Positioned(
              top: 160,
              child: Container(
                width: MediaQuery.sizeOf(context).width,
                padding: const EdgeInsets.symmetric(horizontal: 12),
                height: MediaQuery.sizeOf(context).height * 0.7,
                child: ListView(
                  children: [
                    ...vouchers!
                        .map((e) => MyVoucherItem(
                              voucher: e,
                            ))
                        .toList(),
                    Container(
                        margin: const EdgeInsets.symmetric(vertical: 16),
                        alignment: Alignment.center,
                        child: const Text(
                          "Đã hết danh sách",
                          style: TextStyle(fontSize: 16),
                        ))
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
