// import 'dart:convert';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fushipment/core/store.dar';
// import 'package:fushipment/widgets/user/product/quantity_selector_widget.dart';
// import 'package:fushipment/values/app_icons.dart';
// import 'package:fushipment/values/app_styles.dart';
// import 'package:share_plus/share_plus.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:velocity_x/velocity_x.dart';
// import '../../models/product.dart';
// import '../../utils/routes.dart';
// import '../../widgets/footer_bar_widget.dart';
// import 'home_page.dart';
//
// class ProductDetailPage extends StatefulWidget {
//   final Product product;
//
//   const ProductDetailPage({super.key, required this.product});
//
//   @override
//   State<ProductDetailPage> createState() => _ProductDetailPageState();
// }
//
// class _ProductDetailPageState extends State<ProductDetailPage> {
//   TextEditingController descText = TextEditingController(text: "");
//   TextEditingController quantityText = TextEditingController(text: '1');
//   num totalPrice = 0;
//   MyStore? myStore = VxState.store as MyStore?;
//
//   @override
//   void initState() {
//     TotalPrice(1, widget.product.price);
//   }
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [TotalPrice]);
//     Future _addToCart() async {
//       final SharedPreferences prefs = await SharedPreferences.getInstance();
//       final cart = prefs.getString("cart");
//       Map<String, dynamic> _itemData = {
//         "id": widget.product.id,
//         "name": widget.product.name,
//         "price": widget.product.price,
//         "image": widget.product.image,
//         "quantity": int.parse(quantityText.text),
//         "desc": descText.text
//       };
//       if (cart == "" || cart == null) {
//         final listItemData = [];
//         listItemData.add(_itemData);
//         String encodeData = json.encode(listItemData);
//         await prefs.setString("cart", encodeData);
//       } else {
//         final cartData = prefs.getString("cart");
//         var decodeCartData = json.decode(cartData!);
//         decodeCartData.add(_itemData);
//         String encodeData = json.encode(decodeCartData);
//         await prefs.setString("cart", encodeData);
//       }
//       Navigator.pushNamed(context, AppRoutes.cartRoute);
//     }
//
//     return Scaffold(
//       backgroundColor: Colors.white,
//       resizeToAvoidBottomInset: false,
//       body: Stack(children: [
//         Padding(
//           padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 20),
//           child: Row(
//             mainAxisSize: MainAxisSize.max,
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               GestureDetector(
//                 child: Image.asset(AppIcons.cancelIcon),
//                 onTap: () {
//                   Navigator.pushAndRemoveUntil(
//                       context,
//                       MaterialPageRoute(builder: (_) => const HomePage()),
//                       (route) => false);
//                 },
//               ),
//               GestureDetector(
//                 child: Image.asset(AppIcons.shareIcon),
//                 onTap: () {
//                   Share.share(
//                       'https://gitlab.com/thanhlonganninh2000/fushipment/-/blob/main/Hola/lib/Pages/ProductDetailPage.dart?ref_type=heads');
//                 },
//               ),
//             ],
//           ),
//         ),
//         Column(
//           children: [
//             Expanded(flex: 8, child: Image.asset(widget.product.image)),
//             Expanded(
//               flex: 3,
//               child: Row(
//                 children: [
//                   Expanded(
//                       flex: 2,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Padding(
//                             padding: const EdgeInsets.fromLTRB(20, 15, 15, 10),
//                             child: Text(
//                               widget.product.name,
//                               textAlign: TextAlign.left,
//                               style: AppStyles.h5.copyWith(
//                                   fontSize: 22, fontWeight: FontWeight.w600),
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.fromLTRB(20, 0, 0, 5),
//                             child: Text(
//                               "Bánh mỳ đặc ruột thơm ngon",
//                               style: AppStyles.h5.copyWith(fontSize: 14),
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
//                             child: Text("Thịt nướng sốt đẫm nước dùng",
//                                 style: AppStyles.h5.copyWith(fontSize: 14)),
//                           )
//                         ],
//                       )),
//                   Expanded(
//                       flex: 1,
//                       child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.end,
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: [
//                             Padding(
//                               padding: const EdgeInsets.fromLTRB(0, 20, 20, 0),
//                               child: Text(
//                                 "${widget.product.price}.000 đ",
//                                 style: AppStyles.h5.copyWith(
//                                     fontSize: 18, fontWeight: FontWeight.w600),
//                               ),
//                             )
//                           ]))
//                 ],
//               ),
//             ),
//             const Padding(
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               child: Divider(color: Color.fromRGBO(0, 0, 0, 0.4)),
//             ),
//             Expanded(
//               flex: 2,
//               child: Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 child: Row(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Text(
//                       "Số lượng",
//                       style: AppStyles.h5
//                           .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
//                     ),
//                     QuantitySelectorWidget(widget.product, quantityText)
//                   ],
//                 ),
//               ),
//             ),
//             const Padding(
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               child: Divider(color: Color.fromRGBO(0, 0, 0, 0.4)),
//             ),
//              Expanded(
//               flex: 4,
//               child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
//                   child: SizedBox(
//                     height: 175,
//                     child: TextField(
//                       style: const TextStyle(fontSize: 14),
//                       maxLines: 6,
//                       controller: descText,
//                       onChanged: (text) {
//                         setState(() {
//                           descText.text = text;
//                           descText.selection = TextSelection.collapsed(offset: descText.text.length);
//                         });
//                       },
//                       decoration: const InputDecoration(
//                           border: InputBorder.none,
//                           hintText: "Thêm lưu ý cho quán",
//                           prefixIcon: Padding(
//                             padding: EdgeInsetsDirectional.only(bottom: 50),
//                             child:
//                                 Icon(Icons.add_card, color: Color(0xFF222222)),
//                           )),
//                     ),
//                   )),
//             ),
//             const Padding(
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               child: Divider(color: Color.fromRGBO(0, 0, 0, 0.4)),
//             ),
//             Expanded(
//               flex: 2,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Padding(
//                     padding: const EdgeInsets.only(left: 20),
//                     child: Text("Thanh toán",
//                         style: AppStyles.h5.copyWith(
//                             fontSize: 18, fontWeight: FontWeight.w600)),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(right: 20),
//                     child: Text(
//                       "${myStore?.totalPrice}.000 đ",
//                       style: AppStyles.h5
//                           .copyWith(fontSize: 24, fontWeight: FontWeight.w600),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//             Container(
//               margin: const EdgeInsets.only(bottom: 25),
//               padding: const EdgeInsets.symmetric(horizontal: 20),
//               width: double.maxFinite,
//               height: 48,
//               child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                       shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(10))),
//                   onPressed: _addToCart,
//                   child: Text("Thêm vào giỏ hàng",
//                       style: AppStyles.h5.copyWith(color: Colors.white))),
//             )
//           ],
//         )
//       ]),
//       bottomNavigationBar: const FooterBarWidget(),
//     );
//   }
// }
