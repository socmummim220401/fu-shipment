import 'package:flutter/material.dart';
import 'package:fushipment/models/banner_collection.dart';
import 'package:fushipment/widgets/user/layout/slide_widget/index.dart';
import 'package:fushipment/widgets/user/store/body_widget/index.dart';
import 'package:fushipment/widgets/user/store/body_widget/total_item_widget.dart';
import 'package:fushipment/widgets/user/store/header_widget/index.dart';

//ignore: must_be_immutable
class StorePage extends StatelessWidget {
  String? storeActionType; // "order" , "shipping"
  StorePage({super.key, this.storeActionType});

  @override
  Widget build(BuildContext context) {
    List<BannerCollection> bannerCollection = [
      BannerCollection(1,
          "https://gongcha.com.vn/wp-content/uploads/2023/07/nha%CC%83n.799x500-01-scaled.jpg"),
      BannerCollection(
        2,
        "https://danviet.mediacdn.vn/296231569849192448/2023/3/21/anh-chup-man-hinh-2023-03-21-luc-181359-16793972531391854030969.png",
      ),
      BannerCollection(3,
          "https://cdn.vietnambiz.vn/2019/9/5/571769907380898232555215921961732487418836n-1567666923764485576938.jpg"),
      BannerCollection(4,
          "https://res.cloudinary.com/practicaldev/image/fetch/s---zAYcBhD--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/i/w605r3q5eduhw5tqly08.png"),
      BannerCollection(5,
          "https://mastigado.files.wordpress.com/2020/12/banner_flutter_dark.jpg"),
    ];
    return (Scaffold(
      backgroundColor: const Color(0xffeeeeee),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          _contentStorePage(context, storeActionType, bannerCollection),
          TotalItemStore(storeActionType: storeActionType, showIcon: true, total: 0,)
        ],
      ),
    ));
  }
}

Widget _contentStorePage (BuildContext context, String? storeActionType, List<BannerCollection> bannerCollection) {
  return Expanded(
    child: ListView(
      padding: const EdgeInsets.only(bottom: 40),
      scrollDirection: Axis.vertical,
      children: <Widget>[
        SlideWidget(
          leftPosition: MediaQuery.of(context).size.width * 0.42,
          topPosition: 184,
          width: MediaQuery.of(context).size.width,
          height: 200,
          bannerCollection: bannerCollection,
          border: 0,
          paddingParent: 0,
          actionButton: true,
        ),
        StoreHeader(storeActionType: storeActionType),
        StoreBody(storeActionType: storeActionType),
      ],
    ),
  );
}
