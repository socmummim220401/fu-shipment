import 'package:flutter/material.dart';
import 'package:fushipment/utils/routes.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewFacebook extends StatefulWidget {
  const WebViewFacebook({super.key});

  @override
  State<WebViewFacebook> createState() => _WebViewsState();
}

class _WebViewsState extends State<WebViewFacebook> {
  late final WebViewController _controller;

  @override
  void initState() {
    super.initState();
    _controller = WebViewController();

    //login with facebook
    _controller.loadFlutterAsset('assets/web_view/sign_in.html');

    _controller.setJavaScriptMode(JavaScriptMode.unrestricted);
    _controller.setUserAgent("random");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebViewWidget(controller: _controller),
      bottomNavigationBar: FooterWebView(controller: _controller,),
    );
  }
}

class FooterWebView extends StatelessWidget {
  final WebViewController controller;

  const FooterWebView({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF343434),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          IconButton(
            icon: const Icon(Icons.arrow_back_ios, color: Color(0xFFcdcdcd)),
            onPressed: () async {
              final messenger = ScaffoldMessenger.of(context);
              if (await controller.canGoBack()) {
                await controller.goBack();
              } else {
                messenger.showSnackBar(
                  const SnackBar(content: Text('No back history item')),
                );
                return;
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.arrow_forward_ios, color: Color(0xFFcdcdcd)),
            onPressed: () async {
              final messenger = ScaffoldMessenger.of(context);
              if (await controller.canGoForward()) {
                await controller.goForward();
              } else {
                messenger.showSnackBar(
                  const SnackBar(content: Text('No forward history item')),
                );
                return;
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.replay, color: Color(0xFFcdcdcd)),
            onPressed: () {
              controller.reload();
            },
          ),
          IconButton(
            icon: const Icon(Icons.close, color: Color(0xFFcdcdcd)),
            onPressed: () {
              Navigator.pushNamed(context, AppRoutes.loginRoute);
            },
          ),
        ],
      ),
    );
  }
}
