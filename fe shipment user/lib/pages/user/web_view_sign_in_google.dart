import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fushipment/core/store.dart';
import 'package:fushipment/utils/routes.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewGoogle extends StatefulWidget {
  const WebViewGoogle({super.key});

  @override
  State<WebViewGoogle> createState() => _WebViewsState();
}

class _WebViewsState extends State<WebViewGoogle> {
  late final WebViewController _controller;
  MyStore? store = VxState.store as MyStore?;

  @override
  void initState() {
    super.initState();
    _controller = WebViewController();
    //login with google
    _controller.loadRequest(Uri.parse(
        "http://be-api.us-east-2.elasticbeanstalk.com/sign_in/user1"));
    _controller.setJavaScriptMode(JavaScriptMode.unrestricted);
    _controller.setUserAgent("random");
    _controller.setNavigationDelegate(
        NavigationDelegate(onPageFinished: (String url) async {
      if (url.contains(
          "http://be-api.us-east-2.elasticbeanstalk.com/sign_in/user1")) {
        var result = await _controller.runJavaScriptReturningResult(
            'if(document.querySelector("body > pre")) {document.querySelector("body > pre").innerHTML}');
        if (json.decode(result.toString()) != null) {
          store?.userData = json.decode(json.decode(result.toString()));
          if (context.mounted) {
            Navigator.pushNamed(context, AppRoutes.homeRoute);
          }
        }
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebViewWidget(controller: _controller),
      bottomNavigationBar: FooterWebView(controller: _controller),
    );
  }
}

class FooterWebView extends StatelessWidget {
  final WebViewController controller;

  const FooterWebView({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF343434),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          IconButton(
            icon: const Icon(Icons.arrow_back_ios, color: Color(0xFFcdcdcd)),
            onPressed: () async {
              final messenger = ScaffoldMessenger.of(context);
              if (await controller.canGoBack()) {
                await controller.goBack();
              } else {
                messenger.showSnackBar(
                  const SnackBar(content: Text('No back history item')),
                );
                return;
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.arrow_forward_ios, color: Color(0xFFcdcdcd)),
            onPressed: () async {
              final messenger = ScaffoldMessenger.of(context);
              if (await controller.canGoForward()) {
                await controller.goForward();
              } else {
                messenger.showSnackBar(
                  const SnackBar(content: Text('No forward history item')),
                );
                return;
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.replay, color: Color(0xFFcdcdcd)),
            onPressed: () {
              controller.reload();
            },
          ),
          IconButton(
            icon: const Icon(Icons.close, color: Color(0xFFcdcdcd)),
            onPressed: () {
              Navigator.pushNamed(context, AppRoutes.loginRoute);
            },
          ),
        ],
      ),
    );
  }
}
