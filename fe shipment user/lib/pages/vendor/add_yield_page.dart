import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fushipment/models/product.dart';
import 'package:fushipment/utils/routes.dart';
import 'package:image_picker/image_picker.dart';

import '../../core/store.dar';
import '../../widgets/footer_bar_widget.dart';
import '../../widgets/vendor/drawer.dart';

class AddYieldPage extends StatefulWidget {
  const AddYieldPage({super.key});

  @override
  State<AddYieldPage> createState() => _AddYieldPageState();
}

class _AddYieldPageState extends State<AddYieldPage> {
  File? image;
  String productName = "";
  int productPrice = 0;
  String productDesc = "";

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;

      final imageTemporary = File(image.path);
      setState(() {
        this.image = imageTemporary;
      });
    } on PlatformException catch (e) {
      print(e);
    }
  }

  _addProduct() async {
    if (image != null) {
      String uniqueFileName = DateTime.now().microsecondsSinceEpoch.toString();
      Reference referenceRoot = FirebaseStorage.instance
          .ref()
          .child('product/images/$uniqueFileName');
      try {
        await referenceRoot.putFile(File(image!.path));
        String imageLink = await referenceRoot.getDownloadURL();
        AddProduct(Product(0, productName, imageLink, productPrice, productDesc));
        Navigator.pushNamed(context, AppRoutes.yieldRoute);
      } catch (error) {
        print("Upload image fail: $error");
      }
    } else {
      return;
    }
  }

  Future<ImageSource?> showImageSource(BuildContext context) async {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<ImageSource>(
          context: context,
          builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.camera);
                      },
                      child: Text("Camera")),
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.gallery);
                      },
                      child: Text("Gallery"))
                ],
              ));
    } else {
      return showModalBottomSheet(
          context: context,
          builder: (context) => Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                    onTap: () {
                      Navigator.of(context).pop(ImageSource.camera);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.image_outlined),
                    title: Text("Gallery"),
                    onTap: () {
                      Navigator.of(context).pop(ImageSource.gallery);
                    },
                  )
                ],
              ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final imagePath = image != null
        ? image!.path
        : "https://cdn-icons-png.flaticon.com/512/3566/3566345.png";
    final imageShow = imagePath.contains('https://')
        ? NetworkImage(imagePath)
        : FileImage(File(imagePath));
    return Scaffold(
      appBar: AppBar(
        title: Text("Thêm sản phẩm"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            children: [
              Spacer(),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Stack(
                          children: [
                            Ink.image(
                              image: imageShow as ImageProvider,
                              fit: BoxFit.cover,
                              width: 100,
                              height: 100,
                              child: InkWell(
                                onTap: () async {
                                  final source = await showImageSource(context);
                                  if (source == null) return;
                                  pickImage(source);
                                  // ValueChanged<ImageSource>(source);
                                },
                              ),
                            ),
                            imagePath ==
                                    "https://cdn-icons-png.flaticon.com/512/3566/3566345.png"
                                ? Positioned(
                                    bottom: 0,
                                    right: 24,
                                    child: ClipOval(
                                      child: Container(
                                        width: 28,
                                        height: 28,
                                        color: Colors.blue,
                                        child: InkWell(
                                          onTap: () async {
                                            final source =
                                                await showImageSource(context);
                                            if (source == null) return;
                                            pickImage(source);
                                            // ValueChanged<ImageSource>(source);
                                          },
                                          child: Icon(
                                            Icons.add_a_photo,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox()
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Column(
                          children: [
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: "Nhập tên sản phẩm",
                                labelText: "Tên sản phẩm",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Tên sản phẩm không thể bỏ trống";
                                }

                                return null;
                              },
                              onChanged: (value) {
                                productName = value;
                                setState(() {});
                              },
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: "Nhập giá sản phẩm",
                                labelText: "Giá sản phẩm",
                                suffixText: ".000 đ"
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Giá sản phẩm không thể bỏ trống";
                                }

                                return null;
                              },
                              keyboardType: TextInputType.number,
                              onChanged: (value) {
                                productPrice = int.tryParse(value) ?? 0;
                                setState(() {});
                              },
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 175,
                child: TextField(
                  style: const TextStyle(fontSize: 14),
                  maxLines: 6,
                  onChanged: (value) {
                    productDesc = value;
                    setState(() {});
                  },
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: "Mô tả về sản phẩm",
                      prefixIcon: Padding(
                        padding: EdgeInsetsDirectional.only(bottom: 80),
                        child: Icon(Icons.add_card, color: Color(0xFF222222)),
                      )),
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    _addProduct();
                  },
                  style: ElevatedButton.styleFrom(
                      minimumSize: Size.fromHeight(52),
                      textStyle: TextStyle(fontSize: 16)),
                  child: Text("Thêm sản phẩm")),
              SizedBox(
                height: 20,
              ),
              // ElevatedButton(
              //     onPressed: () {
              //       pickImage(ImageSource.camera);
              //     },
              //     style: ElevatedButton.styleFrom(
              //         minimumSize: Size.fromHeight(56),
              //         textStyle: TextStyle(fontSize: 20)),
              //     child: Row(
              //       children: [
              //         Icon(Icons.camera_alt_outlined),
              //         Text("Pick Camera")
              //       ],
              //     )),
              Spacer(),
            ],
          ),
        ),
      ),
      drawer: VendorDrawer(),
      bottomNavigationBar: const FooterBarWidget(),
    );
  }
}
