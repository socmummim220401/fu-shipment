// import 'dart:convert';
//
// import 'package:flutter/material.dart';
// import 'package:fushipment/core/store.dart';
// import 'package:fushipment/models/order_item.dart';
// import 'package:fushipment/utils/routes.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:velocity_x/velocity_x.dart';
//
// import '../../models/catalog.dart';
// import '../../utils/notification_service.dart';
// import '../../widgets/footer_bar_widget.dart';
// import '../../widgets/vendor/drawer.dart';
//
// class OrderPage extends StatefulWidget {
//   const OrderPage({super.key});
//
//   @override
//   State<OrderPage> createState() => _OrderPageState();
// }
//
// class _OrderPageState extends State<OrderPage> {
//   MyStore? myStore = VxState.store as MyStore?;
//
//   NotificationService notificationService = NotificationService();
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     notificationService.requestNotificationPermission();
//     notificationService.firebaseInit(context);
//     // notificationService.isTokenRefresh();
//     notificationService.getDeviceToken().then((value) => {print(value)});
//     myStore!.orderItems = [
//       OrderItem(1, "Socquekute", 1, [
//         {
//           "id": 1,
//           "name": "Coca",
//           "price": 20,
//           "image": "assets/images/category/coca.png",
//           "quantity": 2,
//           "desc": ""
//         }
//       ]),
//       OrderItem(2, "Shark", 0, [
//         {
//           "id": 1,
//           "name": "Coca",
//           "price": 20,
//           "image": "assets/images/category/coca.png",
//           "quantity": 2,
//           "desc": ""
//         },
//         {
//           "id": 3,
//           "name": "Hamburger",
//           "price": 40,
//           "image": "assets/images/category/hamburger.png",
//           "quantity": 4,
//           "desc": "123"
//         }
//       ])
//     ];
//   }
//
//   Future<void> viewOrderDetail(int index) async {
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     var cartData = myStore?.orderItems[index].cart;
//     prefs.setString('cart', json.encode(cartData));
//     Navigator.pushNamed(context, AppRoutes.cartRoute);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [UpdateOrderItems]);
//
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           "Danh sách đơn hàng",
//           style: TextStyle(color: Colors.black),
//         ),
//         // automaticallyImplyLeading: false,
//         centerTitle: true,
//         backgroundColor: Colors.white,
//         iconTheme: IconThemeData(color: Colors.black),
//       ),
//       body: Center(
//         child: Container(
//           padding: EdgeInsets.symmetric(vertical: 20),
//           child: VxConsumer(
//             mutations: {UpdateOrderItems},
//             notifications: {},
//             builder: (context, store, status) {
//               return ListView.builder(
//                 itemCount: myStore?.orderItems.length,
//                 itemBuilder: (context, index) {
//                   num totalPrice = List.from(myStore!.orderItems[index].cart)
//                       .map<CartItems>((e) => CartItems.fromMap(e))
//                       .toList()
//                       .fold(0,
//                           (total, curr) => total + curr.price * curr.quantity);
//                   return Padding(
//                     padding:
//                         const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
//                     child: InkWell(
//                       onTap: () => viewOrderDetail(index),
//                       child: (Card(
//                         shape: RoundedRectangleBorder(
//                           side: BorderSide(
//                             color: Theme.of(context).colorScheme.outline,
//                           ),
//                           borderRadius: const BorderRadius.all(Radius.circular(16)),
//                         ),
//                         child: ListTile(
//                             leading: ClipRRect(
//                               borderRadius: BorderRadius.circular(8.0),
//                               child: Image.network(
//                                 "https://cdn.icon-icons.com/icons2/3708/PNG/512/man_person_people_avatar_icon_230017.png",
//                               ),
//                             ),
//                             title: Text(
//                               myStore!.orderItems[index].created_by,
//                               style: TextStyle(fontSize: 18),
//                             ),
//                             subtitle: Text("Tổng tiền: $totalPrice.000đ"),
//                             trailing: myStore?.orderItems[index].status == 1
//                                 ? Icon(
//                                     Icons.check_circle,
//                                     color: Colors.green,
//                                   )
//                                 : Icon(
//                                     Icons.cancel,
//                                     color: Colors.red,
//                                   )),
//                       )),
//                     ),
//                   );
//                 },
//               );
//             },
//           ),
//         ),
//       ),
//       drawer: VendorDrawer(),
//       bottomNavigationBar: const FooterBarWidget(),
//     );
//   }
// }
