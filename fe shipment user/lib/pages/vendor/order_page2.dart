import 'package:flutter/material.dart';
import 'package:fushipment/values/app_colors.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({super.key});

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  List<Map<String, dynamic>> items = [
    {"title": "Đơn hàng", "icon": Icons.date_range},
    {"title": "Cửa hàng", "icon": Icons.store},
    {"title": "Voucher", "icon": Icons.card_giftcard},
    {"title": "Thống kê", "icon": Icons.bar_chart},
    {"title": "Cài đặt", "icon": Icons.settings},
    {"title": "Đăng xuất", "icon": Icons.exit_to_app},
  ];
  List<Map<String, dynamic>> orders = [
    {"title": "Chờ xác nhận", "quantity": 1},
    {"title": "Đang chuẩn bị", "quantity": 0},
    {"title": "Đang giao", "quantity": 0},
    {"title": "Đã giao", "quantity": 0},
    {"title": "Bị hủy", "quantity": 0},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("HOBUY"),
      ),
      drawer: Drawer(
        width: MediaQuery.sizeOf(context).width,
        backgroundColor: Colors.white,
        child: Container(
          margin: EdgeInsets.symmetric(
              vertical: MediaQuery.sizeOf(context).height * 0.08),
          child: ListView(
            shrinkWrap: true,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 140,
                    height: 140,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          "https://cdn.tgdd.vn/2021/05/CookRecipe/Avatar/banh-mi-thit-bo-nuong-thumbnail-1.jpg",
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 8),
                  const Text(
                    "Bánh Mì Tèo Em",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.only(top: 8),
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: items.asMap().entries.map((entry) {
                    final index = entry.key;
                    final item = entry.value;
                    final isLastItem = index ==
                        items.length - 1; // Check if it's the last item
                    return Container(
                      margin: EdgeInsets.only(top: !isLastItem ? 6 : 12),
                      padding: const EdgeInsets.all(4),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.white,
                          width: 4.0,
                        ),
                        borderRadius:
                            !isLastItem ? BorderRadius.circular(4) : null,
                        boxShadow: !isLastItem
                            ? [
                                const BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 1.0,
                                  spreadRadius: 0.1,
                                ),
                              ]
                            : null,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Container(
                                alignment: Alignment.centerRight,
                                child: Icon(
                                  item["icon"],
                                  color: AppColors.primaryColor,
                                  size: 32,
                                )),
                          ),
                          SizedBox(
                            width: MediaQuery.sizeOf(context).width * 0.04,
                          ),
                          Expanded(
                              flex: 4,
                              child: Text(
                                item["title"],
                                style: const TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 15),
                              ))
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
            child: Column(
              children: [
                Container(margin: EdgeInsets.only(bottom: 12, top: 4),child: const Text("Đơn hàng hôm nay", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),)),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.white,
                      width: 4.0,
                    ),
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1.0,
                        spreadRadius: 0.1,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                    child: Column(
                      children: orders.asMap().entries.map((entry) {
                        final index = entry.key;
                        final item = entry.value;
                        final isLastItem = index == orders.length - 1;
                        final isFirstItem = index == 0;
                        return Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),

                              child: (Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Text(item["title"]),
                                      const SizedBox(
                                        width: 2,
                                      ),
                                      (isFirstItem || isLastItem)
                                          ? Icon(
                                              Icons.circle,
                                              size: 10,
                                              color: isFirstItem
                                                  ? Colors.green
                                                  : Colors.red,
                                            )
                                          : Container()
                                    ],
                                  ),
                                  RichText(
                                    text: TextSpan(
                                      text: '',
                                      style: const TextStyle(color: Colors.black),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: item["quantity"].toString(),
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        const TextSpan(text: ' ĐH'),
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            !isLastItem ? const Divider(color: Color(0xffcccccc),) : const SizedBox()
                          ],
                        );
                      }).toList(),
                    ),
                  ),
                ),
                Container(margin: EdgeInsets.only(bottom: 12, top: 16),child: const Text("Voucher", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),)),
                Container(
                  height: 160,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.white,
                      width: 4.0,
                    ),
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1.0,
                        spreadRadius: 0.1,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
