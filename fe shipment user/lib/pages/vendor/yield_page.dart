// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fushipment/core/store.dar';
// import 'package:fushipment/widgets/vendor/drawer.dart';
// import 'package:velocity_x/velocity_x.dart';
//
// import '../../utils/notification_service.dart';
// import '../../utils/routes.dart';
// import '../../widgets/footer_bar_widget.dart';
//
// class YieldPage extends StatefulWidget {
//   @override
//   State<YieldPage> createState() => _YieldPage();
// }
//
// class _YieldPage extends State<YieldPage> {
//   NotificationService notificationService = NotificationService();
//   MyStore? store = VxState.store as MyStore?;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     notificationService.requestNotificationPermission();
//     notificationService.firebaseInit(context);
//     // notificationService.isTokenRefresh();
//     notificationService.getDeviceToken().then((value) => {print(value)});
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [RemoveProduct]);
//
//     return Scaffold(
//       appBar: AppBar(
//         iconTheme: IconThemeData(color: Colors.black),
//         title: Text(
//           "Danh sách sản phẩm",
//           style: TextStyle(color: Colors.black),
//         ),
//         // automaticallyImplyLeading: false,
//         backgroundColor: Colors.white,
//         actions: <Widget>[
//           TextButton(
//             onPressed: () {
//               Navigator.pushNamed(context, AppRoutes.addYieldRoute);
//             },
//             child: const Text('Thêm'),
//           ),
//         ],
//       ),
//       body: Center(
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
//             child: Container(
//                 child: store!.productItems.isNotEmpty
//                     ? ListView.builder(
//                         itemBuilder: (context, index) {
//                           var item = store!.productItems[index];
//                           return Padding(
//                             padding: const EdgeInsets.symmetric(vertical: 4.0),
//                             child: Card(
//                               child: Row(
//                                 children: [
//                                   Expanded(
//                                       child: Image.network(
//                                         item.image,
//                                         fit: BoxFit.fill,
//                                       ),
//                                       flex: 1),
//                                   SizedBox(
//                                     width: 10,
//                                   ),
//                                   Expanded(
//                                       child: Column(
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         children: [
//                                           Text(
//                                             item.name,
//                                             style: TextStyle(
//                                                 fontSize: 20,
//                                                 fontWeight: FontWeight.w500),
//                                           ),
//                                           SizedBox(height: 5,),
//                                           SizedBox(
//                                               height: 110, child: Text(item.des)),
//                                           Text(
//                                             "Giá: ${item.price}.000 đ",
//                                             style: TextStyle(
//                                                 fontSize: 16,
//                                                 fontWeight: FontWeight.w500),
//                                           )
//                                         ],
//                                       ),
//                                       flex: 2),
//                                   Expanded(
//                                       child: Column(
//                                         crossAxisAlignment: CrossAxisAlignment.end,
//                                         children: [
//                                           Container(
//                                             width: 80,
//                                             color: Colors.green,
//                                             height: 80,
//                                             child: InkWell(
//                                               onTap: () {},
//                                               child: Icon(
//                                                 Icons.edit,
//                                                 size: 30,
//                                                 color: Colors.white,
//                                               ),
//                                             ),
//                                           ),
//                                           Container(
//                                             width: 80,
//                                             height: 80,
//                                             color: Colors.red,
//                                             child: InkWell(
//                                               onTap: () {
//                                                 RemoveProduct(index);
//                                               },
//                                               child: Icon(Icons.cancel,
//                                                   size: 30, color: Colors.white),
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                       flex: 2),
//                                 ],
//                               ),
//                             ),
//                           );
//                         },
//                         itemCount: store!.productItems.length ?? 0,
//                       )
//                     : Column(
//                         children: [
//                           Spacer(),
//                           Text("Không có sản phẩm nào được tạo"),
//                           SizedBox(
//                             height: 20,
//                           ),
//                           ElevatedButton(
//                               onPressed: () => Navigator.pushNamed(
//                                   context, AppRoutes.addYieldRoute),
//                               child: Text("Thêm sảm phẩm")),
//                           Spacer()
//                         ],
//                       )),
//           )),
//       drawer: VendorDrawer(),
//       bottomNavigationBar: const FooterBarWidget(),
//     );
//   }
// }
