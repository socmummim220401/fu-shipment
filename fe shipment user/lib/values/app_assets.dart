// define app of asset

class AppAssets {
  static const String imagePath = 'assets/images/';

  //logo
  static const String logoPath = "${imagePath}logo/";
  static const String hoLaPath = "${logoPath}logo_name.png";


  //banner
  static const String bannerPath = "${imagePath}banner/";
  static const String bannerShipper = "${bannerPath}banner_shipper.png";
  static const String bannerShopping = "${bannerPath}banner_shopping.png";

}