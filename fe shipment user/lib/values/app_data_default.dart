import 'package:fushipment/models/category_item.dart';
import 'package:line_icons/line_icons.dart';

class AppDataDefault {
  static List<CategoryItem> mainCategoryData = [
    CategoryItem(1, "Ví Fu", LineIcons.wallet),
    CategoryItem(2, "Nạp thẻ ĐT", LineIcons.mobilePhone),
    CategoryItem(3, "Membership", LineIcons.userFriends),
    CategoryItem(4, "Quét QR", LineIcons.qrcode)
  ];
  static List<CategoryItem> allCategoryData = [
    CategoryItem(1, "Ví Fu", LineIcons.wallet),
    CategoryItem(2, "Nạp thẻ ĐT", LineIcons.mobilePhone),
    CategoryItem(3, "Membership", LineIcons.userFriends),
    CategoryItem(4, "Quét QR", LineIcons.qrcode),
    CategoryItem(5, "Ví Fu", LineIcons.wallet),
    CategoryItem(6, "Nạp thẻ ĐT", LineIcons.mobilePhone),
    CategoryItem(7, "Membership", LineIcons.userFriends),
    CategoryItem(8, "Quét QR", LineIcons.qrcode),
    CategoryItem(9, "Ví Fu", LineIcons.wallet),
    CategoryItem(10, "Nạp thẻ ĐT", LineIcons.mobilePhone),
    CategoryItem(11, "Membership", LineIcons.userFriends),
    CategoryItem(12, "Quét QR", LineIcons.qrcode),
  ];
}
