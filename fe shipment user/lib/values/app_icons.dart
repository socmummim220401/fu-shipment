class AppIcons {
  static const String iconPath = "assets/icons/";

  static const String documentIcon = "${iconPath}document.png";
  static const String cancelIcon = "${iconPath}cancel.png";
  static const String shareIcon = "${iconPath}share.png";

  static const String googleIcon = "${iconPath}google_icon.png";
  static const String facebookIcon = "${iconPath}facebook_icon.png";

}