import 'package:flutter/material.dart';

class FontFamily {
  static const inter = "Inter";
}

class AppStyles {
  static TextStyle h1 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 109.66, color: Colors.black);
  static TextStyle h2 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 67.77, color: Colors.black);
  static TextStyle h3 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 41.89, color: Colors.black);
  static TextStyle h4 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 25.89, color: Colors.black);
  static TextStyle h5 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 16, color: Colors.black);
  static TextStyle h6 = const TextStyle(
      fontFamily: FontFamily.inter, fontSize: 9.89, color: Colors.black);

}
