// import 'package:flutter/material.dart';
// import 'package:fushipment/utils/routes.dart';
// import 'package:velocity_x/velocity_x.dart';
//
// import '../core/store.dart';
//
// class FooterBarWidget extends StatefulWidget {
//   const FooterBarWidget({super.key});
//
//   @override
//   State<FooterBarWidget> createState() => _FooterBarWidgetState();
// }
//
// class _FooterBarWidgetState extends State<FooterBarWidget> {
//   MyStore? store = VxState.store as MyStore?;
//   @override
//   Widget build(BuildContext context) {
//     int _currentTab = 0;
//     void changeTab(currentPage) {
//       setState(() {
//         _currentTab = currentPage;
//       });
//       switch (currentPage) {
//         case 0:
//           store?.role == "user" ? Navigator.pushNamed(context, AppRoutes.homeRoute) : Navigator.pushNamed(context, AppRoutes.orderRoute);
//           break;
//         case 1:
//           Navigator.pushNamed(context, AppRoutes.yieldRoute);
//           break;
//         case 2:
//           Navigator.pushNamed(context, AppRoutes.cartRoute);
//           break;
//         default:
//           Navigator.pushNamed(context, AppRoutes.homeRoute);
//       }
//     }
//     return store!.role == "user" ? BottomNavigationBar(
//       items: const [
//         BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
//         BottomNavigationBarItem(icon: Icon(Icons.map), label: 'Map'),
//         BottomNavigationBarItem(icon: Icon(Icons.shopping_cart), label: 'Cart'),
//         BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
//       ],
//       currentIndex: _currentTab,
//       selectedItemColor: Colors.blue,
//       unselectedItemColor: Colors.blueAccent,
//       showUnselectedLabels: true,
//       onTap: (index) => {
//         changeTab(index)
//       },
//     ) : BottomNavigationBar(
//       items: const [
//         BottomNavigationBarItem(icon: Icon(Icons.feed_outlined), label: 'Đơn hàng'),
//         BottomNavigationBarItem(icon: Icon(Icons.fastfood), label: 'Sản phẩm'),
//       ],
//       currentIndex: _currentTab,
//       selectedItemColor: Colors.blue,
//       unselectedItemColor: Colors.blueAccent,
//       showUnselectedLabels: true,
//       onTap: (index) => {
//         changeTab(index)
//       },
//     );
//   }
// }