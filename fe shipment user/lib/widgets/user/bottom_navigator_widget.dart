import 'package:flutter/material.dart';

class BottomNavigationUser extends StatefulWidget {
  final int? selectedIndex;
  final  ValueChanged<int>? onPress; // Use ValueChanged<int> for onTap

  const BottomNavigationUser({super.key,
    required this.selectedIndex,
    required this.onPress,
  });

  @override
  State<BottomNavigationUser> createState() => _BottomNavigationUser();
}

class _BottomNavigationUser extends State<BottomNavigationUser> {

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
          ),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.history,
          ),
          label: 'Business',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.airplane_ticket,
          ),
          label: 'School',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.menu,
          ),
          label: 'Settings',
        ),
      ],
      currentIndex: widget.selectedIndex ?? 0,
      selectedItemColor: Colors.amber[800],
      unselectedItemColor: const Color(0xffdedede),
      selectedLabelStyle: const TextStyle(fontSize: 0),
      onTap: (index) {
        if (widget.onPress != null) {
          widget.onPress!(index);
        }
      },
    );
  }
}
