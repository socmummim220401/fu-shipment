// import 'dart:convert';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fushipment/core/store.dart';
// import 'package:fushipment/models/catalog.dart';
// import 'package:fushipment/models/product.dart';
// import 'package:fushipment/values/app_styles.dart';
// import 'package:fushipment/widgets/user/product/quantity_selector_widget.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:velocity_x/velocity_x.dart';
//
// class CartItemsWidget extends StatefulWidget {
//   final CartItems item;
//   final List<CartItems> cartItems;
//   final int index;
//
//   const CartItemsWidget(
//       {Key? key,
//       required this.item,
//       required this.cartItems,
//       required this.index})
//       : assert(item != null),
//         super(key: key);
//
//   @override
//   State<CartItemsWidget> createState() => _CartItemsWidgetState();
// }
//
// class _CartItemsWidgetState extends State<CartItemsWidget> {
//   MyStore? store = VxState.store as MyStore?;
//
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [RemoveFromCart, UpdateCartItems]);
//     TextEditingController quantityText =
//         TextEditingController(text: '${widget.item.quantity}');
//
//     TextEditingController desText =
//         TextEditingController(text: widget.item.desc);
//
//     void showInformationItem() {
//       showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//                 scrollable: true,
//                 title: const Text('Thông tin sản phẩm'),
//                 content: Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Form(
//                     child: Column(
//                       children: <Widget>[
//                         TextFormField(
//                           readOnly: store!.role == "user" ? false : true,
//                           decoration: const InputDecoration(
//                             labelText: 'Số lượng',
//                             icon: Icon(Icons.bar_chart_sharp),
//                           ),
//                           controller: quantityText,
//                           keyboardType: TextInputType.number,
//                           onChanged: (text) {
//                             setState(() {
//                               quantityText.text = text;
//                               quantityText.selection = TextSelection.collapsed(
//                                   offset: desText.text.length);
//                             });
//                           },
//                         ),
//                         TextFormField(
//                           readOnly: store!.role == "user" ? false : true,
//                           decoration: const InputDecoration(
//                             labelText: 'Thêm lưu ý cho quán',
//                             icon: Icon(Icons.add_card),
//                           ),
//                           maxLines: 6,
//                           onChanged: (text) {
//                             setState(() {
//                               desText.text = text;
//                               desText.selection = TextSelection.collapsed(
//                                   offset: desText.text.length);
//                             });
//                           },
//                           controller: desText,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//                 actions: store!.role == "user"
//                     ? <Widget>[
//                         TextButton(
//                           style: TextButton.styleFrom(
//                             textStyle: Theme.of(context).textTheme.labelLarge,
//                           ),
//                           child: const Text('Đóng'),
//                           onPressed: () {
//                             Navigator.of(context).pop();
//                           },
//                         ),
//                         TextButton(
//                           style: TextButton.styleFrom(
//                             textStyle: Theme.of(context).textTheme.labelLarge,
//                           ),
//                           child: const Text('Chỉnh sửa'),
//                           onPressed: () {
//                             UpdateCartItems(widget.index,
//                                 int.parse(quantityText.text), desText.text);
//                             Navigator.of(context).pop();
//                           },
//                         ),
//                       ]
//                     : <Widget>[]);
//           });
//     }
//
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 5),
//       child: Card(
//         shape: RoundedRectangleBorder(
//           side: BorderSide(
//             color: Theme.of(context).colorScheme.outline,
//           ),
//           borderRadius: const BorderRadius.all(Radius.circular(16)),
//         ),
//         child: Row(
//           children: [
//             Expanded(
//                 flex: 1,
//                 child: Padding(
//                   padding: const EdgeInsets.all(5.0),
//                   child: Image.asset(
//                     widget.item.image,
//                     alignment: Alignment.center,
//                     width: 100,
//                     height: 100,
//                   ),
//                 )),
//             const SizedBox(
//               width: 10,
//             ),
//             Expanded(
//                 flex: 2,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(
//                       widget.item.name,
//                       style: const TextStyle(
//                           fontSize: 20, fontWeight: FontWeight.bold),
//                     ),
//                     const SizedBox(
//                       height: 20,
//                     ),
//                     Text("Số lượng: ${widget.item.quantity}",
//                         style: const TextStyle(
//                             fontSize: 16, color: Color(0xFFa9a9a9)))
//                   ],
//                 )),
//             Expanded(
//                 flex: 1,
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.end,
//                   children: [
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: store!.role == "user"
//                           ? [
//                               const SizedBox(
//                                 width: 1,
//                               ),
//                               InkWell(
//                                 onTap: showInformationItem,
//                                 child: const Icon(
//                                   Icons.edit,
//                                   color: Colors.green,
//                                 ),
//                               ),
//                               InkWell(
//                                 onTap: () {
//                                   RemoveFromCart(widget.index);
//                                 },
//                                 child: const Icon(
//                                   Icons.delete,
//                                   color: Colors.red,
//                                 ),
//                               ),
//                               const SizedBox(
//                                 width: 1,
//                               ),
//                             ]
//                           : [
//                               const SizedBox(
//                                 width: 1,
//                               ),
//                               const SizedBox(
//                                 width: 20,
//                               ),
//                               InkWell(
//                                 onTap: showInformationItem,
//                                 child: Icon(
//                                   Icons.info_rounded,
//                                   color: Colors.blue,
//                                 ),
//                               ),
//                               const SizedBox(
//                                 width: 1,
//                               ),
//                             ],
//                     ),
//                     const SizedBox(
//                       height: 20,
//                     ),
//                     Text(
//                       "${widget.item.price}.000 đ",
//                       style: const TextStyle(
//                           fontSize: 16,
//                           fontWeight: FontWeight.bold,
//                           color: Color(0xFFa9a9a9)),
//                     )
//                   ],
//                 )),
//           ],
//         ),
//       ),
//     );
//   }
// }
