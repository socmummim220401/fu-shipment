import 'package:flutter/material.dart';
import 'package:fushipment/models/history.dart';
import 'package:fushipment/widgets/user/history/history_items/index.dart';

//ignore: must_be_immutable
class HistoryBody extends StatefulWidget {
  List<History>? histories;

  HistoryBody({super.key, required this.histories});

  @override
  State<HistoryBody> createState() => _HistoryBodyState();
}

class _HistoryBodyState extends State<HistoryBody> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      margin: const EdgeInsets.symmetric(vertical: 12),
      child: ListView(
        scrollDirection: Axis.vertical,
        children: widget.histories!.map((e) {
          return HistoryItem(history: e);
        }).toList(),
      ),
    ));
  }
}
