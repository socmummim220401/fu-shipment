import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

//ignore: must_be_immutable
class HistoryHeader extends StatefulWidget {
  TextEditingController? rangeController;


  HistoryHeader({super.key, required this.rangeController});

  @override
  State<HistoryHeader> createState() => _HistoryHeaderState();
}

class _HistoryHeaderState extends State<HistoryHeader> {
  final DateRangePickerController _dateRangePickerController =
      DateRangePickerController();

  void showPopUpDatePicker() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SfDateRangePicker(
                view: DateRangePickerView.month,
                monthViewSettings: const DateRangePickerMonthViewSettings(
                    firstDayOfWeek: 1,
                    dayFormat: 'EEE',
                    viewHeaderStyle: DateRangePickerViewHeaderStyle(
                        textStyle: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.black))),
                selectionMode: DateRangePickerSelectionMode.range,
                showActionButtons: true,
                showNavigationArrow: true,
                controller: _dateRangePickerController,
                startRangeSelectionColor: Colors.deepOrangeAccent,
                endRangeSelectionColor: Colors.deepOrangeAccent,
                rangeSelectionColor: Colors.orangeAccent,
                onSubmit: (Object? value) {
                  if (value is PickerDateRange) {
                    widget.rangeController!.text =
                        '${DateFormat('dd/MM/yyyy').format(value.startDate as DateTime)} -'
                        ' ${DateFormat('dd/MM/yyyy').format(value.endDate ?? value.startDate as DateTime)}';
                    FocusScope.of(context).unfocus();
                    Navigator.pop(context);
                  }
                },
                onCancel: () {
                  _dateRangePickerController.selectedRange = null;
                  widget.rangeController!.text = "";
                },
                headerStyle: const DateRangePickerHeaderStyle(
                    textAlign: TextAlign.center),
                initialSelectedRange: PickerDateRange(
                    DateTime.now().subtract(const Duration(days: 4)),
                    DateTime.now().add(const Duration(days: 3))),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle bs = ButtonStyle(
        padding: MaterialStatePropertyAll(EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.08)));
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(top: 16, right: 16, left: 16, bottom: 8),
      child: Column(
        children: [
          SizedBox(
            height: 40,
            child: TextField(
              maxLines: 1,
              controller: widget.rangeController, // Use the text controller
              onTap: showPopUpDatePicker,
              decoration: const InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffdedede), width: 1),
                ),
                contentPadding: EdgeInsets.zero,
                hintText: '--Chọn ngày--',
                prefixIcon: Icon(
                  Icons.calendar_month,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              OutlinedButton(
                  onPressed: null,
                  style: bs,
                  child: const Text(
                    "Tất cả",
                    style: TextStyle(color: Colors.black),
                  )),
              OutlinedButton(
                  onPressed: null,
                  style: bs,
                  child: const Text(
                    "Voucher",
                    style: TextStyle(color: Colors.black),
                  )),
              OutlinedButton(
                  onPressed: null,
                  style: bs,
                  child: const Text(
                    "Cửa hàng",
                    style: TextStyle(color: Colors.black),
                  )),
            ],
          ),
          const SizedBox(
            height: 12,
          )
        ],
      ),
    );
  }
}
