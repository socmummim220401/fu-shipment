import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/models/history.dart';

//ignore: must_be_immutable
class HistoryItem extends StatelessWidget {
  History? history;

  HistoryItem({super.key, required this.history});

  @override
  Widget build(BuildContext context) {
    // int? id = history!.id;
    int? type = history!.type;
    String? name = history!.name;
    String? datePayment = history!.datePayment;
    int? status = history!.status;
    double? price = history!.price;

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 4),
      padding: const EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Icon(
                type == 0
                    ? Icons.local_movies_rounded
                    : Icons.home_filled,
                color: Colors.orangeAccent,
                size: 40,
              )),
          Expanded(
              flex: 8,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name!,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    datePayment!,
                    style: const TextStyle(
                        color: Color(0xffa5a5a5), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        status == 1 ? "Hoàn thành" : "Chưa thanh toán",
                        style: TextStyle(
                            color: status == 1
                                ? Colors.green
                                : Colors.red),
                      ),
                      Text(
                        Helpers.formatNumber(price!),
                        style:
                        const TextStyle(fontWeight: FontWeight.w600),
                      )
                    ],
                  )
                ],
              )),
          const Expanded(
              flex: 1,
              child: Column(
                children: [Icon(Icons.arrow_forward_ios)],
              ))
        ],
      ),
    );
  }

}