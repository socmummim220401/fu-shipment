import 'package:flutter/material.dart';
import 'package:fushipment/models/category_item.dart';

import '../categories_item_widget.dart';

//ignore: must_be_immutable
class AllCategoryWidget extends StatelessWidget {
  List<CategoryItem> categoryData;

  AllCategoryWidget({super.key, required this.categoryData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: UnconstrainedBox(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 4.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
            boxShadow: const [
              BoxShadow(color: Colors.white, spreadRadius: 3),
            ],
          ),
          width: MediaQuery.of(context).size.width * 0.94,
          height: 184,
          child: GridView(
            scrollDirection: Axis.horizontal,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 100,
                childAspectRatio: 0.9,
                crossAxisSpacing: 0,
                mainAxisSpacing: 0),
            children: categoryData.map((item) {
              return CategoriesItemWidget(
                item: item,
                fontTitle: 13,
                iconSize: 40,
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
