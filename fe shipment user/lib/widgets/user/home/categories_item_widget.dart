import 'package:flutter/material.dart';
import 'package:fushipment/models/category_item.dart';

//ignore: must_be_immutable
class CategoriesItemWidget extends StatelessWidget {
  CategoryItem item;
  double fontTitle = 16;
  double iconSize = 48;

  CategoriesItemWidget({super.key, required this.item, required this.fontTitle, required this.iconSize});

  @override
  Widget build(BuildContext context) {
    String itemTitle = item.title;
    IconData itemIconData = item.iconData;
    return Column(
      children: [
        Icon(
          itemIconData,
          size: iconSize,
        ),
        const SizedBox(
          height: 4,
        ),
        Text(itemTitle, style: TextStyle(fontSize: fontTitle),),
      ],
    );
  }
}
