// import 'package:flutter/material.dart';
// import 'package:fushipment/values/app_assets.dart';
//
// import '../../../models/product.dart';
//
// class CategoriesWidget extends StatelessWidget {
//   List<Product> productList = [
//     Product(1, "Coca", AppAssets.coca, 20, ""),
//     Product(2, "Pepsi", AppAssets.pepsi, 30, ""),
//     Product(3, "Hamburger", AppAssets.hamburger, 40, ""),
//     Product(4, "Bread", AppAssets.bread, 20, ""),
//     Product(5, "7up", AppAssets.up, 40, ""),
//     Product(6, "Chocolate", AppAssets.chocolate, 10, ""),
//     Product(7, "Cà ry", AppAssets.carry, 20, ""),
//   ];
//
//   CategoriesWidget({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       scrollDirection: Axis.horizontal,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
//         child: Row(
//           children: productList.map((product) {
//             return Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 10),
//               child: Container(
//                 padding: const EdgeInsets.all(8),
//                 decoration: BoxDecoration(
//                   color: Colors.white,
//                   borderRadius: BorderRadius.circular(10),
//                   boxShadow: [
//                     BoxShadow(
//                       color: Colors.grey.withOpacity(0.5),
//                       spreadRadius: 2,
//                       blurRadius: 10,
//                       offset: const Offset(0, 3),
//                     )
//                   ],
//                 ),
//                 child: Image.asset(
//                   product.image,
//                   width: 50,
//                   height: 50,
//                 ),
//               ),
//             );
//           }).toList(),
//         ),
//       ),
//     );
//   }
// }
