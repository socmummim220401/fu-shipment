import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/models/voucher.dart';

//ignore: must_be_immutable
class StoreVoucherItem extends StatelessWidget {
  Voucher? voucher;
  bool? visibleDivider;

  StoreVoucherItem({super.key, this.voucher, this.visibleDivider});

  @override
  Widget build(BuildContext context) {
    String? image = voucher?.image;
    String? name = voucher?.name;
    String? des = voucher?.des;
    double? score = voucher?.score;
    double? price = voucher?.price;
    List<int> stars = List.filled(5, 0);

    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 6, top: 4),
          child: (Row(
            children: [
              Expanded(
                  flex: 1,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(
                      image!,
                      width: MediaQuery.of(context).size.width * 0.2,
                    ),
                  )),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                  flex: 2,
                  child: SizedBox(
                    height: 68,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          name!,
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Visibility(
                          visible: score != 0,
                          child: Container(
                            margin: const EdgeInsets.only(top: 4),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                ...stars.map((star) {
                                  return (const Icon(
                                    Icons.star,
                                    color: Colors.orangeAccent,
                                    size: 12,
                                  ));
                                }).toList(),
                                const SizedBox(
                                  width: 4,
                                ),
                                Text(score.toString(), style: const TextStyle(fontSize: 12),)
                              ],
                            ),
                          ),
                        ),
                        Text(
                          des!,
                          style: const TextStyle(
                              color: Color(0xffcccccc), fontSize: 12, fontWeight: FontWeight.w500),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(Helpers.formatNumber(price!),
                            style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.orangeAccent,
                                fontSize: 15)),
                      ],
                    ),
                  ))
            ],
          )),
        ),
        Visibility(
            visible: visibleDivider ?? false,
            child: const Divider(color: Color(0xffcccccc))),
      ],
    );
  }
}
