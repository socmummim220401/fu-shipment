import 'package:flutter/material.dart';
import 'package:fushipment/models/user_post.dart';
import 'package:fushipment/widgets/user/home/list_user_post_widget/user_post_item_widget/index.dart';

//ignore: must_be_immutable
class ListUserPostWidget extends StatelessWidget {
  List<UserPost>? listUserPost;

  ListUserPostWidget({this.listUserPost, super.key});

  @override
  Widget build(BuildContext context) {
    return (Column(
      children: listUserPost!.map((item) {
        return UserPostWidget(userPost: item);
      }).toList(),
    ));
  }
}
