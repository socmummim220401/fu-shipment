import 'package:flutter/material.dart';
import 'package:fushipment/models/user_post.dart';
import 'package:fushipment/pages/user/store_page.dart';

//ignore: must_be_immutable
class BodyUserPost extends StatelessWidget {
  UserPost? userPost;

  BodyUserPost({super.key,  this.userPost});

  @override
  Widget build(BuildContext context) {
    String voucherDetail = userPost!.voucherDetail ?? "";
    List<String> productImage = userPost!.productImage ?? [];

    return (Container(
      margin: const EdgeInsets.symmetric(vertical: 12),
      child:
          // voucher detail
          Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Visibility(
                child: Row(
              children: [
                const Icon(
                  Icons.airplane_ticket,
                  color: Colors.orangeAccent,
                ),
                const SizedBox(width: 6),
                RichText(
                    text: TextSpan(
                        text: "",
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                      TextSpan(
                          text: voucherDetail.split(" ")[0],
                          style: const TextStyle(
                              color: Colors.red, fontWeight: FontWeight.w900)),
                      TextSpan(
                        text:
                            '  ${voucherDetail.substring(voucherDetail.indexOf(voucherDetail.split(" ")[1]))}',
                      )
                    ])),
              ],
            )),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    productImage[0],
                    width: MediaQuery.of(context).size.width * 0.54,
                  )),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        productImage[1],
                        width: MediaQuery.of(context).size.width * 0.29,
                      )),
                  const SizedBox(height: 8,),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        productImage[2],
                        width: MediaQuery.of(context).size.width * 0.29,
                      ))
                ],
              )
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> StorePage()));
              },
              child: const Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.house, color: Colors.orangeAccent,),
                  SizedBox(width: 4,),
                  Text("Xem cửa hàng >", style: TextStyle(fontWeight: FontWeight.w500),)
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
