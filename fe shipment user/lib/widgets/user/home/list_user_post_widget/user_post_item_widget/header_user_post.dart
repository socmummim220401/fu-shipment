import 'package:flutter/material.dart';
import 'package:fushipment/models/user_post.dart';
import 'package:intl/intl.dart';

//ignore: must_be_immutable
class HeaderUserPost extends StatelessWidget {
  UserPost? userPost;

  HeaderUserPost({super.key, this.userPost});

  @override
  Widget build(BuildContext context) {
    List<int> stars = List.filled(5, 0);
    String timeOpen = userPost!.timeOpen ?? "";
    String timeClose = userPost!.timeClose ?? "";
    String iconImage = userPost!.iconImage ?? "";
    String userPostName = userPost!.userPostName ?? "";
    double rate = userPost!.rate ?? 0;
    String description = userPost!.description ?? "";

    DateTime now = DateTime.now();

    DateTime parsedTimeOpen = DateFormat('H:m:s').parse(timeOpen);
    DateTime dateTimeOpen = DateTime(
      now.year,
      now.month,
      now.day,
      parsedTimeOpen.hour,
      parsedTimeOpen.minute,
      parsedTimeOpen.second,
    );

    DateTime parsedTimeClose = DateFormat('H:m:s').parse(timeClose);
    DateTime dateTimeClose = DateTime(
      now.year,
      now.month,
      now.day,
      parsedTimeClose.hour,
      parsedTimeClose.minute,
      parsedTimeClose.second,
    );
    bool checkStoreOpen =
        dateTimeOpen.isBefore(now) && dateTimeClose.isAfter(now);

    return (Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipOval(
            child: SizedBox.fromSize(
          size: const Size.fromRadius(24), // Image radius
          child: Image.network(iconImage),
        )),
        const SizedBox(
          width: 12,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              userPostName,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            ),
            Visibility(
              visible: rate > 0,
              child: Container(
                margin: const EdgeInsets.only(top: 6),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ...stars.map((star) {
                      return (const Icon(
                        Icons.star,
                        color: Colors.orangeAccent,
                        size: 16,
                      ));
                    }).toList(),
                    const SizedBox(
                      width: 4,
                    ),
                    Text(userPost!.rate.toString())
                  ],
                ),
              ),
            ),
            Visibility(
              visible: !checkStoreOpen,
              child: Container(
                margin: const EdgeInsets.only(top: 6),
                child: RichText(
                  text: TextSpan(
                    text: '',
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      const TextSpan(
                          text: 'Đã đóng cửa ',
                          style: TextStyle(color: Colors.red, fontSize: 13)),
                      const TextSpan(
                          text: '|',
                          style: TextStyle(
                              color: Color(0xffcccccc), fontSize: 13)),
                      const TextSpan(
                          text: ' Quay lại lúc ',
                          style: TextStyle(fontSize: 13)),
                      TextSpan(
                          text: '${timeOpen.substring(0, 5)} ngày mai',
                          style: const TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 13)),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: userPost!.voucher!.isNotEmpty,
              child: Row(
                children: userPost!.voucher!.map((item) {
                  return (Container(
                    padding: const EdgeInsets.all(3.0),
                    margin: const EdgeInsets.only(top: 8, right: 6),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.orangeAccent)),
                    child: Text(item),
                  ));
                }).toList(),
              ),
            ),
            // const SizedBox(height: 6,),
            Container(
              margin: const EdgeInsets.only(top: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.bookmark,
                    color: Colors.orangeAccent,
                    size: 20,
                  ),
                  const SizedBox(width: 4,),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.64,
                    child: Text(
                      description,
                      style: const TextStyle(fontSize: 14),
                    ),
                  )
                ],
              ),
            )
          ],
        )
      ],
    ));
  }
}
