import 'package:flutter/material.dart';
import 'package:fushipment/models/user_post.dart';
import 'package:fushipment/pages/user/store_page.dart';
import 'package:fushipment/widgets/user/home/list_user_post_widget/user_post_item_widget/body_user_post.dart';
import 'header_user_post.dart';

//ignore: must_be_immutable
class UserPostWidget extends StatelessWidget {
  UserPost? userPost;

  UserPostWidget({super.key, this.userPost});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => StorePage()));
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: UnconstrainedBox(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 10,
                  offset: const Offset(0, 3),
                )
              ],
            ),
            width: MediaQuery.of(context).size.width * 0.94,
            // height: 360,
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 16, left: 16, bottom: 0, top: 16),
              child: (Column(
                children: [
                  HeaderUserPost(userPost: userPost),
                  BodyUserPost(userPost: userPost)
                ],
              )),
            ),
          ),
        ),
      ),
    );
  }
}
