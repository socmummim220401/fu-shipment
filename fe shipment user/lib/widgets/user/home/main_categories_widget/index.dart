import 'package:flutter/material.dart';
import 'package:fushipment/models/category_item.dart';
import 'package:fushipment/widgets/user/home/categories_item_widget.dart';

//ignore: must_be_immutable
class MainCategoryWidget extends StatelessWidget {
  List<CategoryItem> categoryData;
  MainCategoryWidget({super.key, required this.categoryData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: (SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 68,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: categoryData.map((item) {
              return CategoriesItemWidget(item: item, fontTitle: 14, iconSize: 48,);
            }).toList(),
          ))),
    );
  }
}
