import 'package:flutter/material.dart';
import 'package:fushipment/widgets/user/home/nav_bar_widget/notification_widget.dart';
import 'package:fushipment/widgets/user/home/search_bar_widget.dart';

class NavBarWidget extends StatefulWidget {
  const NavBarWidget({super.key});

  @override
  State<NavBarWidget> createState() => _NavBarWidgetState();
}

class _NavBarWidgetState extends State<NavBarWidget> {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SearchBarWidget(),
          NotificationWidget()
        ],
      ),
    );
  }
}