import 'package:flutter/material.dart';
import 'package:fushipment/pages/notification_page.dart';
import 'package:line_icons/line_icons.dart';

class NotificationWidget extends StatefulWidget {
  const NotificationWidget({super.key});

  @override
  State<NotificationWidget> createState() => _NotificationWidgetState();
}

class _NotificationWidgetState extends State<NotificationWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=> const NotificationPage()));
      },
      child: const Icon(
        LineIcons.bell,
        size: 36,
      ),
    );
  }
}
