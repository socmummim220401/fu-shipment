// import 'package:flutter/material.dart';
// import 'package:fushipment/pages/user/product_detail_page.dart';
// import 'package:fushipment/values/app_assets.dart';
//
// import '../../../models/product.dart';
//
// class ProductBackupWidget extends StatelessWidget {
//   const ProductBackupWidget({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     List<Product> productList = [
//       Product(1, "Coca", AppAssets.coca, 20, ""),
//       Product(2, "Pepsi", AppAssets.pepsi, 30, ""),
//       Product(3, "Hamburger", AppAssets.hamburger, 40, ""),
//       Product(4, "Bread", AppAssets.bread, 20, ""),
//       Product(5, "7up", AppAssets.up, 40, ""),
//       Product(6, "Chocolate", AppAssets.chocolate, 10, ""),
//       Product(7, "Cà ry", AppAssets.carry, 20, ""),
//     ];
//
//     return SingleChildScrollView(
//       scrollDirection: Axis.horizontal,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
//         child: Row(
//           children: [
//             for (int i = 0; i < productList.length; i++)
//               GestureDetector(
//                 onTap: () {
//                   // Handle the tap event and navigate to the detail page
//                   navigateToDetailPage(context, productList[i]);
//                 },
//                 child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 7),
//                   child: Container(
//                     width: 170,
//                     height: 225,
//                     decoration: BoxDecoration(
//                       color: Colors.white,
//                       borderRadius: BorderRadius.circular(10),
//                       boxShadow: [
//                         BoxShadow(
//                           color: Colors.grey.withOpacity(0.5),
//                           spreadRadius: 2,
//                           blurRadius: 10,
//                           offset: const Offset(0, 3),
//                         ),
//                       ],
//                     ),
//                     child: Padding(
//                       padding: const EdgeInsets.symmetric(horizontal: 10),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Container(
//                             alignment: Alignment.center,
//                             child: Image.asset(
//                               productList[i].image,
//                               width: 140,
//                               height: 140,
//                             ),
//                           ),
//                           Column(
//                             children: [
//                               Text(
//                                 productList[i].name,
//                                 style: const TextStyle(
//                                   fontSize: 20,
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               ),
//                               const SizedBox(height: 4),
//                               Text(
//                                 "Taste Our ${productList[i].name}",
//                                 style: const TextStyle(
//                                   fontSize: 15,
//                                 ),
//                               ),
//                               const SizedBox(height: 12),
//                                Row(
//                                 mainAxisAlignment:
//                                     MainAxisAlignment.spaceBetween,
//                                 children: [
//                                   Text(
//                                     "${productList[i].price}.000 đ",
//                                     style: const TextStyle(
//                                       fontSize: 17,
//                                       color: Colors.red,
//                                       fontWeight: FontWeight.bold,
//                                     ),
//                                   ),
//                                   const Icon(
//                                     Icons.favorite_border,
//                                     color: Colors.red,
//                                     size: 26,
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ElevatedButton(
//               onPressed: () {
//                 // Handle the next slide button press event
//                 // This could involve scrolling to the next set of products
//               },
//
//               style: ElevatedButton.styleFrom(
//                 shape: const CircleBorder(),
//                 backgroundColor: Colors.white,
//               ),
//               // child: IconButton(
//               //   icon: Icon(Icons.arrow_forward),
//               //   onPressed: () {
//               //     // Handle button press event
//               //   },
//               //   color: Colors.black,
//               // ),
//               child: const Icon(Icons.arrow_forward, color: Colors.black),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   void navigateToDetailPage(BuildContext context, Product product) {
//     // Navigate to the detail page and pass the selected product
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => ProductDetailPage(product: product)
//       ),
//     );
//   }
// }
