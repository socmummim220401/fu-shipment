import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/models/product.dart';
import 'package:fushipment/pages/user/store_page.dart';
import 'package:fushipment/widgets/user/store/body_widget/total_item_widget.dart';

//ignore: must_be_immutable
class ProductItemWidget extends StatefulWidget {
  Product product;
  String? storeActionType;

  ProductItemWidget({super.key, required this.product, this.storeActionType});

  @override
  State<ProductItemWidget> createState() => _ProductItemWidgetState();
}

class _ProductItemWidgetState extends State<ProductItemWidget> {
    int _quantity = 1;
    void handleModalBottom(BuildContext context, Product product) {
    String? image = product.image;
    String? name = product.name;
    String? des = product.des;
    String price = Helpers.formatNumber(product.price as num);

    showModalBottomSheet(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      context: context,
      builder: (BuildContext context) {
        return BottomSheet(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          onClosing: () {},
          builder: (BuildContext context) {
            return StatefulBuilder(
              builder: (BuildContext context, setState) => Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Colors.white),
                height: MediaQuery.of(context).size.height * 0.7,
                padding: const EdgeInsets.only(
                    top: 20, left: 12, right: 12, bottom: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: ListView(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.network(
                              image!,
                              filterQuality: FilterQuality.high,
                              fit: BoxFit.fill,
                              width: MediaQuery.of(context).size.width * 0.94,
                              height: MediaQuery.of(context).size.height * 0.28,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(
                            name!,
                            style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            price,
                            style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: Colors.orangeAccent,
                                letterSpacing: 1),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            des!,
                            style: const TextStyle(fontWeight: FontWeight.w400),
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    color:  _quantity > 1 ? Colors.orangeAccent : const Color(0xffcccccc),
                                  ),
                                  child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          if (_quantity > 1) {
                                            _quantity--;
                                          }
                                        });
                                      },
                                      child: const Icon(
                                        Icons.remove,
                                        size: 28,
                                        color: Colors.white,
                                      )),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Text(
                                  '$_quantity',
                                  style: const TextStyle(fontSize: 16),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    color: Colors.orangeAccent,
                                  ),
                                  child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          _quantity++;
                                        });
                                      },
                                      child: const Icon(
                                        Icons.add,
                                        size: 28,
                                        color: Colors.white,
                                      )),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    TotalItemStore(
                      storeActionType: widget.storeActionType,
                      showIcon: false,
                      total: (_quantity * (product.price ?? 0))
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    String? image = widget.product.image;
    String? name = widget.product.name;
    String? des = widget.product.des;
    String price = Helpers.formatNumber(widget.product.price as num);

    return InkWell(
      onTap: () {
        widget.storeActionType != null
            ? handleModalBottom(context, widget.product)
            : Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => StorePage(
                          storeActionType: "order",
                        )));
      },
      child: Container(
        margin: const EdgeInsets.only(top: 8, bottom: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.network(
                image!,
                height: 160.0,
                width: 160.0,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              name!,
              style: const TextStyle(fontWeight: FontWeight.w600),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              des!,
              style: const TextStyle(
                  color: Color(0xffcccccc),
                  fontSize: 12,
                  fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              price,
              style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.orangeAccent,
                  fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
