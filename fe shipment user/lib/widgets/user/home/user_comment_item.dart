import 'package:flutter/material.dart';
import 'package:fushipment/models/user_comment.dart';
import 'package:line_icons/line_icons.dart';

//ignore: must_be_immutable
class UserCommentItem extends StatelessWidget {
  UserComment? userComment;

  UserCommentItem({super.key, this.userComment});

  @override
  Widget build(BuildContext context) {
    String? userName = userComment?.userName;
    // double? userRate = userComment?.userRate;
    String? userRole = userComment?.role;
    String? publicDate = userComment?.publicDate;
    String? commentDetail = userComment?.commentDetail;
    String? userImage = userComment?.userImage;
    List<int> stars = List.filled(5, 0);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Divider(
          color: Color(0xffcccccc),
        ),
        // header
        Padding(
          padding: const EdgeInsets.only(left: 4, right: 4, top: 4, bottom: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(40)),
                child: Image.network(
                  userImage!,
                  width: 44,
                  height: 44,
                ),
              ),
              const SizedBox(
                width: 12,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    userName!,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  Row(
                    children: [
                      Text(
                        "$publicDate | ",
                        style: const TextStyle(
                            color: Color(0xffcccccc),
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                      Container(color: const Color(0xffcccccc)),
                      Text(userRole!,
                          style: const TextStyle(
                              color: Colors.green,
                              fontSize: 12,
                              fontWeight: FontWeight.w400))
                    ],
                  ),
                  Visibility(
                    visible: true,
                    child: Container(
                      margin: const EdgeInsets.only(top: 4),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ...stars.map((star) {
                            return (const Icon(
                              Icons.star,
                              color: Colors.orangeAccent,
                              size: 12,
                            ));
                          }).toList(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.28,
              ),
              InkWell(
                onTap: () {},
                child: const Icon(LineIcons.horizontalEllipsis),
              ),
            ],
          ),
        ),

        //body
        Padding(
          padding: const EdgeInsets.only(left: 4, right: 4, top: 4, bottom: 16),
          child: Text(commentDetail!),
        )
      ],
    );
  }
}
