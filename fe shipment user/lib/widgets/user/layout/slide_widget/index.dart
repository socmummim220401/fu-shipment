import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fushipment/models/banner_collection.dart';
import 'package:fushipment/utils/routes.dart';
import 'package:line_icons/line_icons.dart';

class SlideWidget extends StatefulWidget {
  final List<BannerCollection> bannerCollection;
  final double border;
  final double width;
  final double height;
  final double topPosition;
  final double leftPosition;
  final double paddingParent;
  final bool actionButton;

  const SlideWidget({
    Key? key,
    required this.bannerCollection,
    required this.border,
    required this.width,
    required this.height,
    required this.topPosition,
    required this.leftPosition,
    required this.paddingParent,
    this.actionButton = false,
  }) : super(key: key);

  @override
  State<SlideWidget> createState() => _SlideWidgetState();
}

class _SlideWidgetState extends State<SlideWidget> {
  final PageController controller = PageController();
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _currentIndex = 0;
    controller.addListener(() {
      setState(() {
        _currentIndex = controller.page!.toInt();
      });
    });
    _startAutoAdvance();
  }

  void _startAutoAdvance() {
    Timer.periodic(const Duration(seconds: 3), (timer) {
      if (controller.page == widget.bannerCollection.length - 1) {
        controller.animateToPage(0, duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
      } else {
        controller.nextPage(duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final int slideLength = widget.bannerCollection.length;

    return Padding(
      padding: EdgeInsets.all(widget.paddingParent),
      child: Stack(
        children: [
          Container(
            width: widget.width,
            height: widget.height,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(widget.border),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 10,
                  offset: const Offset(0, 3),
                )
              ],
            ),
            child: PageView.builder(
              controller: controller,
              onPageChanged: (index) {
                setState(() {
                  _currentIndex = index;
                });
              },
              itemCount: slideLength,
              itemBuilder: (context, index) {
                var item = widget.bannerCollection[index];
                return ClipRRect(
                  borderRadius: BorderRadius.circular(widget.border),
                  child: Image.network(
                    item.bannerImage,
                    fit: BoxFit.fill,
                  ),
                );
              },
            ),
          ),
          Visibility(
            visible: widget.actionButton,
            child: Positioned(
              top: 40,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      child: Container(
                        margin: const EdgeInsets.only(left: 12),
                        width: 32,
                        height: 32,
                        decoration: BoxDecoration(
                            color: const Color(0xff4c4c4c),
                            borderRadius: BorderRadius.circular(40)),
                        child: const Icon(
                          LineIcons.angleLeft,
                          color: Color(0xffcccccc),
                          size: 16,
                        ),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, AppRoutes.homeRoute2);
                      },
                    ),
                    InkWell(
                      child: Container(
                        margin: const EdgeInsets.only(right: 12),
                        width: 32,
                        height: 32,
                        decoration: BoxDecoration(
                            color: const Color(0xff4c4c4c),
                            borderRadius: BorderRadius.circular(40)),
                        child: const Icon(
                          Icons.share,
                          color: Color(0xffcccccc),
                          size: 16,
                        ),
                      ),
                      onTap: () {},
                    ),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: widget.bannerCollection.isNotEmpty,
            child: Positioned(
              top: widget.topPosition,
              left: widget.leftPosition,
              child: SizedBox(
                height: 10,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  itemCount: slideLength,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return buildIndicator(index == _currentIndex);
                  },
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildIndicator(bool isActive) {
    return UnconstrainedBox(
      child: Container(
        margin: const EdgeInsets.only(right: 6),
        width: 8,
        height: 8,
        decoration: BoxDecoration(
          color: isActive ? Colors.orangeAccent : Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(40)),
          boxShadow: const [
            BoxShadow(
              color: Colors.black38,
              offset: Offset(2, 3),
              blurRadius: 3,
            ),
          ],
        ),
      ),
    );
  }
}

