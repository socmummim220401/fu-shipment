import 'package:flutter/material.dart';
import 'package:fushipment/models/my_voucher.dart';

//ignore: must_be_immutable
class MyVoucherItem extends StatelessWidget {
  MyVoucher? voucher;

  MyVoucherItem({super.key, required this.voucher});

  @override
  Widget build(BuildContext context) {
    // int? id = voucher!.id;
    String? image = voucher!.image;
    int? quantity = voucher!.quantity;
    String? expireDate = voucher!.expireDate;
    String? title = voucher!.title;

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 4),
      padding: const EdgeInsets.only(left: 8, top: 12, bottom: 8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8), color: Colors.white),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipOval(
                child: SizedBox.fromSize(
                    size: const Size.fromRadius(28),
                    // Image radius
                    child: Image.network(
                      image!,
                      fit: BoxFit.fill,
                    )),
              ),
              const SizedBox(
                width: 12,
              ),
              Expanded(
                flex: 8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      expireDate!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(color: Color(0xffa5a5a5)),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      color: Colors.orangeAccent,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(9999),
                          bottomLeft: Radius.circular(9999))),
                  child: Text(
                    "x$quantity",
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
          const InkWell(
            onTap: null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 16.0, bottom: 4),
                  child: Text(
                    "Sử dụng",
                    style: TextStyle(
                        color: Colors.orangeAccent,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
