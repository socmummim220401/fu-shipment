import 'package:flutter/material.dart';
import 'package:fushipment/models/notification.dart';

//ignore: must_be_immutable
class NotificationItem extends StatelessWidget {
  NotificationConstructor? notification;

  NotificationItem({super.key, required this.notification});

  @override
  Widget build(BuildContext context) {
    // int? id = notification!.id;
    String? image = notification!.image;
    String? detail = notification!.detail;
    String? publicDate = notification!.publicDate;
    String? title = notification!.title;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      margin: const EdgeInsets.symmetric(vertical: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8), color: Colors.white),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: SizedBox.fromSize(
                size: const Size.fromRadius(24),
                // Image radius
                child: Image.network(
                  image!,
                  fit: BoxFit.fill,
                )),
          ),
          const SizedBox(
            width: 12,
          ),
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title!,
                    style: const TextStyle(fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  Text(
                    detail!,
                    style: const TextStyle(fontSize: 13),
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  Text(
                    publicDate!,
                    style:
                        const TextStyle(color: Color(0xffa5a5a5), fontSize: 12),
                  )
                ],
              )),
          const SizedBox(
            width: 12,
          )
        ],
      ),
    );
  }
}
