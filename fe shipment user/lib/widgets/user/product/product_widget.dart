// import 'package:flutter/material.dart';
// import 'package:fushipment/values/app_assets.dart';
//
// class ProductWidget extends StatelessWidget {
//   const ProductWidget({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       scrollDirection: Axis.vertical,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
//         child: Row(children: [
//           Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 7),
//             child: Container(
//                 width: 170,
//                 height: 225,
//                 decoration: BoxDecoration(
//                     color: Colors.white,
//                     borderRadius: BorderRadius.circular(10),
//                     boxShadow: [
//                       BoxShadow(
//                         color: Colors.grey.withOpacity(0.5),
//                         spreadRadius: 2,
//                         blurRadius: 10,
//                         offset: const Offset(0, 3),
//                       )
//                     ]),
//                 child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 10),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Container(
//                         alignment: Alignment.center,
//                         child: Image.asset(
//                           AppAssets.hamburger,
//                           width: 140,
//                           height: 140,
//                         ),
//                       ),
//                       const Column(
//                         children: [
//                           Text(
//                             "Hot Burger",
//                             style: TextStyle(
//                               fontSize: 20,
//                               fontWeight: FontWeight.bold,
//                             ),
//                           ),
//                           SizedBox(height: 4),
//                           Text(
//                             "Taste Our Hot Burger",
//                             style: TextStyle(
//                               fontSize: 15,
//                             ),
//                           ),
//                           SizedBox(height: 12),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Text(
//                                 "10000 VND",
//                                 style: TextStyle(
//                                   fontSize: 17,
//                                   color: Colors.red,
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               ),
//                               Icon(
//                                 Icons.favorite_border,
//                                 color: Colors.red,
//                                 size: 26,
//                               ),
//                             ],
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 )),
//           ),
//         ]),
//       ),
//     );
//   }
// }
