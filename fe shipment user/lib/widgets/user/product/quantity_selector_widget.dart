// import 'package:flutter/material.dart';
// import 'package:velocity_x/velocity_x.dart';
// import '../../../core/store.dar';
// import '../../../models/product.dart';
//
// class QuantitySelectorWidget extends StatefulWidget {
//   Product product;
//   TextEditingController quantityText;
//
//   QuantitySelectorWidget(this.product, this.quantityText, {super.key});
//
//   @override
//   _QuantitySelectorState createState() => _QuantitySelectorState();
// }
//
// class _QuantitySelectorState extends State<QuantitySelectorWidget> {
//   void decreaseQuantity() {
//     int currentQuantity = int.tryParse(widget.quantityText.text) ?? 1;
//     setState(() {
//       widget.quantityText.text =
//           currentQuantity > 1 ? "${currentQuantity - 1}" : "1";
//     });
//     TotalPrice(currentQuantity - 1 ?? 1, widget.product.price);
//   }
//
//   void increaseQuantity() {
//     int currentQuantity = int.tryParse(widget.quantityText.text) ?? 1;
//     setState(() {
//       widget.quantityText.text = "${currentQuantity + 1}";
//
//     });
//     TotalPrice(currentQuantity + 1, widget.product.price);
//
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     VxState.watch(context, on: [TotalPrice]);
//
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: [
//         Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             InkWell(
//               onTap: decreaseQuantity,
//               child: Container(
//                 // padding: const EdgeInsets.all(1),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(30),
//                   border: Border.all(
//                     color: Colors.grey,
//                   ),
//                 ),
//                 child: const Icon(Icons.remove, color: Color(0xFF222222)),
//               ),
//             ),
//             const SizedBox(width: 8),
//             Container(
//               margin: const EdgeInsets.symmetric(horizontal: 10),
//               width: 32,
//               height: 40,
//               child: TextField(
//                 decoration: const InputDecoration(
//                   counterText: "",
//                 ),
//                 textAlign: TextAlign.center,
//                 keyboardType: TextInputType.number,
//                 controller: widget.quantityText,
//                 onChanged: (text) {
//                   setState(() {
//                     widget.quantityText.text = text;
//                     widget.quantityText.selection = TextSelection.collapsed(
//                         offset: widget.quantityText.text.length);
//                   });
//                   TotalPrice(num.tryParse(text) ?? 1, widget.product.price);
//                 },
//
//                 maxLength: 2,
//               ),
//             ),
//             const SizedBox(width: 8),
//             InkWell(
//               onTap: increaseQuantity,
//               child: Container(
//                 // padding: const EdgeInsets.all(1),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(30),
//                   border: Border.all(
//                     color: Colors.grey,
//                   ),
//                 ),
//                 child: const Icon(Icons.add, color: Color(0xFF222222)),
//               ),
//             ),
//           ],
//         ),
//         const SizedBox(height: 16, width: 30),
//       ],
//     );
//   }
// }
