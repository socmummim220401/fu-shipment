import 'package:flutter/material.dart';
import 'package:fushipment/widgets/user/store/body_widget/outstanding.dart';
import 'package:fushipment/widgets/user/store/body_widget/rate.dart';
import 'package:fushipment/widgets/user/store/body_widget/voucher.dart';

//ignore: must_be_immutable
class StoreBody extends StatelessWidget {
  String? storeActionType; // "default" , "order"
  StoreBody({super.key, this.storeActionType});

  @override
  Widget build(BuildContext context) {
    return (Column(
      children: storeActionType == null
          ? <Widget>[
              Visibility(
                  child: StoreOutStanding(
                      title: "Nổi bật", storeActionType: storeActionType)),
              const Visibility(child: StoreVoucher()),
              const Visibility(child: StoreRate())
            ]
          : <Widget>[
              Column(
                children: [
                  Visibility(
                      child: StoreOutStanding(
                    title: "Món chính",
                    storeActionType: storeActionType,
                  )),
                ],
              )
            ],
    ));
  }
}
