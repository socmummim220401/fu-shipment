import 'package:flutter/material.dart';
import 'package:fushipment/models/product.dart';
import 'package:fushipment/pages/user/store_page.dart';
import 'package:fushipment/widgets/user/home/product_item_widget.dart';
import 'package:line_icons/line_icons.dart';

class StoreOutStanding extends StatelessWidget {
  final String title;
  final String? storeActionType;

   StoreOutStanding({Key? key, required this.title, this.storeActionType})
      : super(key: key);

  final List<Product> productOutstanding = [
    Product(
      id: 1,
      name: "Há cảo thanh cua",
      image:
      "https://xienquegiasi.com/ckeditor_assets/pictures/266/content_ha-c_o-nhan-th_t.jpg",
      price: 85000,
      des: "10 cái. Chỉ áp dụng mang đi",
    ),
    Product(
      id: 2,
      name: "Vịt quay Quảng Đông",
      image: "https://vit29.com/media/news/989_vit_quay_quang_dong.jpg",
      price: 150000,
      des: "1/2 Con",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      margin: const EdgeInsets.symmetric(vertical: 12),
      width: MediaQuery.of(context).size.width * 0.96,
      padding: const EdgeInsets.only(left: 12, right: 12, top: 20, bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: productOutstanding.map((product) {
              return ProductItemWidget(
                product: product,
                storeActionType: storeActionType,
              );
            }).toList(),
          ),
          Visibility(
            visible: storeActionType == null,
            child: Column(
              children: [
                const Divider(color: Color(0xffcccccc)),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    buttonCustom(context, LineIcons.shoppingCart, "ĐẶT MÓN"),
                    buttonCustom(context, LineIcons.shippingFast, "GIAO VỀ"),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buttonCustom(BuildContext context, IconData icon, String title) {
    String status = title == "ĐẶT MÓN" ? "order" : "ship";
    return Container(
      height: 44,
      width: MediaQuery.of(context).size.width * 0.44,
      decoration: BoxDecoration(
        color: Colors.orangeAccent,
        borderRadius: BorderRadius.circular(9999),
      ),
      child: TextButton.icon(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => StorePage(storeActionType: status),
            ),
          );
        },
        icon: Icon(
          icon,
          color: Colors.white,
        ),
        label: Text(
          title,
          style: const TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
