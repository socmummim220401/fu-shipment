import 'package:flutter/material.dart';
import 'package:fushipment/models/user_comment.dart';
import 'package:fushipment/widgets/user/home/user_comment_item.dart';

class StoreRate extends StatelessWidget {
  const StoreRate({super.key});

  @override
  Widget build(BuildContext context) {
    List<UserComment> listUserComment = [
      UserComment(
          id: 1,
          userName: "Nguyễn Tiến Lộc",
          publicDate: "25/09/2023",
          role: "Người mua hàng",
          userImage:
              "https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper-thumbnail.png",
          userRate: 5,
          commentDetail: "Đi với bạn mà cũng thấy okie lắm nha, yêu Đi với bạn mà cũng thấy okie lắm nha, yêu Đi với bạn mà cũng thấy okie lắm nha, yêu Đi với bạn mà cũng thấy okie lắm nha, yêu"),
      UserComment(
          id: 2,
          userName: "Thiều Đức Phong",
          publicDate: "28/08/2023",
          userImage:
              "https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper-thumbnail.png",
          role: "Người mua hàng",
          userRate: 5,
          commentDetail: "Ngon lắm nè <3")
    ];
    int userCommentLength = listUserComment.length;
    List<int> stars = List.filled(5, 0);
    double averageRate = listUserComment
            .map((comment) => comment.userRate)
            .reduce((value, element) => value! + element!)! /
        listUserComment.length;

    return (Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8))),
      margin: const EdgeInsets.symmetric(vertical: 4),
      width: MediaQuery.of(context).size.width * 0.96,
      padding: const EdgeInsets.only(left: 8, right: 8, top: 20, bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 8),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: '',
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          const TextSpan(
                              text: "Đánh giá ",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600)),
                          TextSpan(
                              text: '($userCommentLength đánh giá)',
                              style: const TextStyle(
                                  color: Color(0xffcccccc),
                                  fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                    const Text("Xem thêm >",
                        style: TextStyle(
                            color: Colors.orangeAccent,
                            fontWeight: FontWeight.w600,
                            fontSize: 13))
                  ],
                ),
                Visibility(
                  visible: true,
                  child: Container(
                    margin: const EdgeInsets.only(top: 4),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ...stars.map((star) {
                          return (const Icon(
                            Icons.star,
                            color: Colors.orangeAccent,
                            size: 12,
                          ));
                        }).toList(),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(averageRate.toString())
                      ],
                    ),
                  ),
                ),
                Column(
                  children: listUserComment.map((userComment) {
                    return UserCommentItem(userComment: userComment);
                  }).toList(),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
