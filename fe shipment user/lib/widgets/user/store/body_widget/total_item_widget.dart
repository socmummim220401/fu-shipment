import 'package:flutter/material.dart';
import 'package:fushipment/helpers/index.dart';
import 'package:fushipment/pages/user/cart_page2.dart';

//ignore: must_be_immutable
class TotalItemStore extends StatelessWidget {
  String? storeActionType;
  bool? showIcon;
  num total;

  TotalItemStore({super.key, this.storeActionType, this.showIcon = true, required this.total});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: storeActionType != null,
      child: Container(
        padding: const EdgeInsets.all(12),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Visibility(
              visible: showIcon ?? false,
              child: const Icon(
                Icons.shopping_cart,
                color: Colors.orangeAccent,
                size: 40,
              ),
            ),
            Text(
              Helpers.formatNumber(total),
              style:  TextStyle(
                  color: total != 0 ? Colors.orange : const Color(0xffcccccc),
                  fontSize: 18,
                  fontWeight: FontWeight.w700),
            ),
            ElevatedButton(
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.symmetric(
                        vertical: 12,
                        horizontal: MediaQuery.of(context).size.width * 0.16)),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(9999))),
                    backgroundColor:
                        MaterialStateProperty.all((total != 0 ? Colors.orangeAccent : const Color(0xffcccccc)))),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>  const CartPage2()));
                },
                child: Text(
                  showIcon == true ? "Xem giỏ hàng" : "Thêm vào giỏ",
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ))
          ],
        ),
      ),
    );
  }
}
