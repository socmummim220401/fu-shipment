import 'package:flutter/material.dart';
import 'package:fushipment/models/voucher.dart';
import 'package:fushipment/widgets/user/home/deal_voucher_item_widget.dart';

class StoreVoucher extends StatelessWidget {
  const StoreVoucher({super.key});

  @override
  Widget build(BuildContext context) {
    List<Voucher> listVoucher = [
      Voucher(
          id: 1,
          name: "Voucher tiền mặt tại Gong Cha",
          des: "Mang các loại trà tốt nhất đến với thực khách",
          score: 0,
          price: 30000,
          image:
              "https://nmgprod.s3.amazonaws.com/media/files/b0/16/b016b26f08bfe0bbea99da02a236110f/cover_image_1646926354.jpg.760x400_q85_crop_upscale.jpg"),
      Voucher(
          id: 2,
          name: "Voucher Buffet 50 Món 229.000VNĐ tại Gong Cha",
          des: "Bậc nhất giữa lòng thủ đô",
          score: 0,
          price: 229000,
          image:
              "https://cdn.tgdd.vn/Files/2022/05/11/1431711/buffet-la-gi-cac-loai-hinh-buffet-pho-bien-va-luu-y-khi-an-buffet-202205110944353015.jpg"),
      Voucher(
          id: 3,
          name: "Voucher Buffet 50 Món 229.000VNĐ tại Gong Cha",
          des: "Bậc nhất giữa lòng thủ đô",
          score: 5.0,
          price: 229000,
          image:
          "https://cdn.tgdd.vn/Files/2022/05/11/1431711/buffet-la-gi-cac-loai-hinh-buffet-pho-bien-va-luu-y-khi-an-buffet-202205110944353015.jpg")
    ];
    return (Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8))),
      margin: const EdgeInsets.symmetric(vertical: 4),
      width: MediaQuery.of(context).size.width * 0.96,
      padding: const EdgeInsets.only(left: 8, right: 8, top: 20, bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 8),
            child: const Text(
              "Deal/Voucher",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: listVoucher.asMap().entries.map((voucher) {
              bool checkLastItem = voucher.key == (listVoucher.length - 1);
              return StoreVoucherItem(voucher: voucher.value, visibleDivider: !checkLastItem,);
            }).toList(),
          ),
        ],
      ),
    ));
  }
}
