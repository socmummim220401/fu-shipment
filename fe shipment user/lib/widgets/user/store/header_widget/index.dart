import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:line_icons/line_icons.dart';

//ignore: must_be_immutable
class StoreHeader extends StatefulWidget {
  String? storeActionType;

  StoreHeader({super.key, this.storeActionType});

  @override
  State<StoreHeader> createState() => _StoreHeaderState();
}

class _StoreHeaderState extends State<StoreHeader> {
  String? status;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.storeActionType != null ? status = widget.storeActionType! : null;
  }

  @override
  Widget build(BuildContext context) {
    List<int> stars = List.filled(5, 0);

    String timeOpen = "07:00:00";
    String timeClose = "22:00:00";

    DateTime now = DateTime.now();

    DateTime parsedTimeOpen = DateFormat('H:m:s').parse(timeOpen);
    DateTime dateTimeOpen = DateTime(
      now.year,
      now.month,
      now.day,
      parsedTimeOpen.hour,
      parsedTimeOpen.minute,
      parsedTimeOpen.second,
    );

    DateTime parsedTimeClose = DateFormat('H:m:s').parse(timeClose);
    DateTime dateTimeClose = DateTime(
      now.year,
      now.month,
      now.day,
      parsedTimeClose.hour,
      parsedTimeClose.minute,
      parsedTimeClose.second,
    );
    bool checkStoreOpen =
        dateTimeOpen.isBefore(now) && dateTimeClose.isAfter(now);

    void handleActionSelected(String title) {
      setState(() {
        if (title == "GIAO VỀ" && status == "order") {
          status = "ship";
        } else if (title == "ĐẶT MÓN" && status == "ship") {
          status = "order";
        }
      });
    }
    return Column(
      children: [
        //title
        Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(12),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                margin: const EdgeInsets.only(top: 12),
                child: const Text(
                  "Gong Cha - Hàng Khay",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                ),
              ),
              Visibility(
                visible: true,
                child: Container(
                  margin: const EdgeInsets.only(top: 12),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      ...stars.map((star) {
                        return (const Icon(
                          Icons.star,
                          color: Colors.orangeAccent,
                          size: 12,
                        ));
                      }).toList(),
                      const SizedBox(
                        width: 4,
                      ),
                      const Text("5.0")
                    ],
                  ),
                ),
              ),
              checkStoreOpen
                  ? Container(
                      margin: const EdgeInsets.only(top: 8),
                      child: const Row(
                        children: [
                          Icon(
                            Icons.watch_later,
                            color: Colors.orangeAccent,
                            size: 14,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Đang mở cửa",
                            style: TextStyle(color: Colors.orange),
                          )
                        ],
                      ),
                    )
                  : Container(
                      margin: const EdgeInsets.only(top: 12),
                      child: RichText(
                        text: TextSpan(
                          text: '',
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            const TextSpan(
                                text: 'Đã đóng cửa ',
                                style:
                                    TextStyle(color: Colors.red, fontSize: 13)),
                            const TextSpan(
                                text: '|',
                                style: TextStyle(
                                    color: Color(0xffcccccc), fontSize: 13)),
                            const TextSpan(
                                text: ' Quay lại lúc ',
                                style: TextStyle(fontSize: 13)),
                            TextSpan(
                                text: '${timeOpen.substring(0, 5)} ngày mai',
                                style: const TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 13)),
                          ],
                        ),
                      ),
                    ),
            ]),
          ),
        ),

        //address
        Container(
          color: Colors.white60,
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
          child: Container(
            child: (Row(
              children: [
                const Expanded(
                  flex: 2,
                  child: (Text(
                    "Tầng 1 Số nhà 25 Phố Hàng Khay, Quận Hoàn Kiếm, Hà Nội",
                    style: TextStyle(
                        letterSpacing: 1, fontWeight: FontWeight.w500),
                  )),
                ),
                Expanded(
                  flex: 1,
                  child: (Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        width: 24,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 10,
                              offset: const Offset(0, 3),
                            )
                          ],
                        ),
                        child: InkWell(
                          onTap: () {},
                          child: const SizedBox(
                              width: 40,
                              height: 40,
                              child: Icon(Icons.place,
                                  color: Colors.orangeAccent)),
                        ),
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 10,
                              offset: const Offset(0, 3),
                            )
                          ],
                        ),
                        child: InkWell(
                          onTap: () {},
                          child: const SizedBox(
                              width: 40,
                              height: 40,
                              child: Icon(Icons.phone_android,
                                  color: Colors.orangeAccent)),
                        ),
                      )
                    ],
                  )),
                ),
              ],
            )),
          ),
        ),

        //voucher

        Visibility(
          visible: true,
          child: Container(
            padding:
                const EdgeInsets.only(top: 16, left: 4, right: 12, bottom: 16),
            color: Colors.white,
            child: const Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Icon(
                    LineIcons.alternateTicket,
                    color: Colors.orangeAccent,
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Giảm 15% khi mua voucher chuỗi Gong Cha",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 14),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Hết hạn 30/09/2023",
                        style:
                            TextStyle(fontSize: 12, color: Color(0xffcccccc)),
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text("Xem thêm >",
                      style: TextStyle(
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.w600,
                          fontSize: 13)),
                )
              ],
            ),
          ),
        ),

        //action button

        Visibility(
          visible: widget.storeActionType != null,
          child: Container(
            padding: const EdgeInsets.only(left: 12, top: 0, right: 12, bottom: 12),
            color: Colors.white,
            child: Column(
              children: [
                const Divider(color: Color(0xffcccccc)),
                const SizedBox(
                  height: 6,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.orangeAccent, width: 1),
                      borderRadius: const BorderRadius.all(Radius.circular(8))),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      buttonCustom(
                          context,
                          LineIcons.shoppingCart,
                          "ĐẶT MÓN",
                          status == "order",
                          () => handleActionSelected("ĐẶT MÓN")),
                      buttonCustom(
                          context,
                          LineIcons.shippingFast,
                          "GIAO VỀ",
                          status == "ship",
                          () => handleActionSelected("GIAO VỀ")),
                    ],
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

Widget buttonCustom(BuildContext context, IconData icon, String title,
    bool selected, handleSelectedAction) {
  return Container(
      height: 32,
      width: MediaQuery.of(context).size.width * 0.46667,
      decoration: BoxDecoration(
          color: selected ? Colors.orangeAccent : Colors.white,
          borderRadius: title == "ĐẶT MÓN"
              ? const BorderRadius.only(
                  topLeft: Radius.circular(8), bottomLeft: Radius.circular(8))
              : const BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8))),
      child: TextButton.icon(
          onPressed: handleSelectedAction,
          icon: Icon(
            icon,
            color: selected ? Colors.white : Colors.orangeAccent,
            size: 16,
          ),
          label: Text(
            title,
            style: TextStyle(
                color: selected ? Colors.white : Colors.orangeAccent,
                fontSize: 12),
          )));
}
