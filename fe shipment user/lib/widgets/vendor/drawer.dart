import 'package:flutter/material.dart';
import 'package:fushipment/utils/routes.dart';

class VendorDrawer extends StatelessWidget {
  const VendorDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const UserAccountsDrawerHeader(
            margin: EdgeInsets.zero,
            accountName: Text("Socquekute"),
            accountEmail: Text("socmummim220401@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://cdn.icon-icons.com/icons2/3708/PNG/512/man_person_people_avatar_icon_230017.png"),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.feed_outlined)
            , title: const Text("Danh sách đơn hàng"),
              onTap: () {
                Navigator.pushNamed(context, AppRoutes.orderRoute);
              }
          ),
          ListTile(
            leading: const Icon(Icons.fastfood)
            , title: const Text("Danh sách sản phẩm"),
            onTap: () {
              Navigator.pushNamed(context, AppRoutes.yieldRoute);
            },
          ),
          ListTile(
            leading: const Icon(Icons.add_shopping_cart_rounded)
            , title: const Text("Thêm sản phẩm"),
            onTap: () {
              Navigator.pushNamed(context, AppRoutes.addYieldRoute);
            },
          )
        ],
      ),
    );
  }

}